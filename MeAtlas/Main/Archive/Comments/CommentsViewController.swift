//
//  CommentsViewController.swift
//  MeAtlas
//
//  Created by Dominic  Smith on 7/23/18.
//  Copyright © 2018 Dominic Smith. All rights reserved.
//Shortcuts for search - Mark:  tableView, Auto-Complete, Text, Data, button actions,

import UIKit
import Parse
import Kingfisher
import SlackTextViewController
import SKPhotoBrowser

class CommentsViewController: SLKTextViewController {
    var comments = [Comment]()
    var note: Post?
    var noteObjectId: String?
    var editComment: Comment?
    
    let uiElement = UIElement()
    
    //var postId: String!
    var mentionedPeopleInNote = [String]()
    var mentionsInTextView = [MEAContacts]()
    var hasSetUpTableView = false
    
    var currentUsername: String?
    var currentUserDisplayName: String?
    var currentUserImageURL: String?
    
    var name: String? 
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = "Comments"
        
        setUpTextView()
        setUpTableView()
        setUpAutoComplete()
        setUpNote()
        setUpCurrentUserInfo()
    }
    
    func setUpCurrentUserInfo() {
        if let name = self.uiElement.getCurrentDefaultUserDisplayName() {
            self.name = name
            
        } else {
            self.loadCurrentUserInfo()
        }
    }
    
    func setUpNote() {
        if let note = self.note {
            if let commentCount = note.commentObjectId?.count {
                for i in 0..<commentCount {
                    let newComment = Comment(objectId: note.commentObjectId![i], text: note.commentText![i], date: note.commentDate![i], userId: note.commentUserId![i], username: nil, name: nil, userImageURL: nil, postId: note.objectId!)
                    self.comments.append(newComment)
                }
            }
            
            if note.username == nil {
                self.loadPostUserInfo()
            }
            
            if !mentionedPeopleInNote.contains(note.userId) {
                self.mentionedPeopleInNote.append(note.userId)
            }
            
            self.loadPeopleMentionedInNote()
            
        } else if let objectId = self.noteObjectId {
            self.loadComments(objectId)
            self.loadNote(objectId)
        }
    }
    
    func setUpTextView() {
        self.tableView?.keyboardDismissMode = .onDrag
        
        self.isInverted = false
        self.textView.placeholder = "Add a comment"
        self.rightButton.setTitle("Send", for: .normal)
        self.rightButton.setTitleColor(uiElement.currentTheme().textColor, for: .normal)
        
        self.shouldScrollToBottomAfterKeyboardShows = true
        self.registerPrefixes(forAutoCompletion: ["@"])
        
        self.textInputbar.backgroundColor = uiElement.currentTheme().backgroundColor
        self.textInputbar.editorTitle.textColor = uiElement.currentTheme().textColor
        self.textInputbar.editorTitle.text = "Edit Comment"
        self.textInputbar.editorLeftButton.tintColor = uiElement.currentTheme().textColor
        self.textInputbar.editorRightButton.tintColor = uiElement.currentTheme().textColor
    }
    
    //mark: Tableview
    let commentsReuse = "commentsReuse"
    let reuse = "reuse"
    func setUpTableView() {
        self.tableView?.register(CommentsTableViewCell.self, forCellReuseIdentifier: commentsReuse)
        self.tableView?.backgroundColor = uiElement.currentTheme().backgroundColor
        self.tableView?.separatorStyle = .none
        
        hasSetUpTableView = true
        self.tableView?.reloadData()
    }
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView == self.autoCompletionView {
            return self.filteredPeople.count
            
        } else if section == 0 {
            return 1
        }
        
        return comments.count 
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if tableView == self.autoCompletionView {
            var cell: MainTableViewCell
            cell = peopleAutoCompleteCell(indexPath)
            return cell
        }
        
        var cell: CommentsTableViewCell!
        cell = commentsCell(indexPath)
        
        if indexPath.section == 0 {
             tableView.separatorStyle = .singleLine
            
        } else {
            tableView.separatorStyle = .none
        }
        
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if tableView == self.autoCompletionView {
            self.tableView?.reloadData()
            var contact: MEAContacts!
            
            if indexPath.section == 0 {
                contact = self.filteredPeople[indexPath.row]
                var userExistsInTextViewArray = false
                let peopleInTextViewIds = self.mentionsInTextView.map {$0.userId}
                
                for user in peopleInTextViewIds {
                    if user == contact.userId {
                        userExistsInTextViewArray = true
                        break
                    }
                }
                
                if !userExistsInTextViewArray {
                    //needs to be space so mention auto-complete dissapears
                    self.acceptAutoCompletion(with: "\(self.filteredPeople[indexPath.row].username!) ")
                    self.mentionsInTextView.append(contact)
                    
                } else {
                    self.showAutoCompletionView(false)
                    self.tableView?.reloadData()
                }
            }
        }
    }
    
    func commentsCell(_ indexPath: IndexPath) -> CommentsTableViewCell {
        var cell: CommentsTableViewCell!
        
        cell = self.tableView?.dequeueReusableCell(withIdentifier: commentsReuse) as? CommentsTableViewCell
        
        var currentComment: Comment!
        if indexPath.section == 0 {
            if let note = self.note {
                let postUserText = Comment(objectId: nil, text: note.text, date: CustomDate().formatDate(date: note.date), userId: note.userId, username: note.username, name: note.name, userImageURL: note.userImageURL, postId: note.objectId)
                currentComment = postUserText
                
            } else {
                currentComment = Comment(objectId: nil, text: "Loading", date: "Loading", userId: "", username: "Loading", name: "Loading", userImageURL: "Loading", postId: "")
            }
            
        } else {
            currentComment = comments[indexPath.row]
        }
        
        if let username = currentComment.username {
            cell.username.text = "@\(username)"
            let personImage = uiElement.currentTheme().personImage
            
            if let userImageURL = currentComment.userImageURL {
                cell.userImage.kf.setImage(with: URL(string: userImageURL ), placeholder: personImage, options: nil, progressBlock: nil, completionHandler: nil)
                
            } else {
                cell.userImage.image = personImage
            }
            
            if let displayName = currentComment.name {
                cell.name.text = displayName
                
            } else {
                cell.name.text = ""
            }
            
        } else {
            if indexPath.section == 1 {
                self.loadUserInfoFromCloud(indexPath.row)
            }
        }
        
        cell.bodyLabel.text = currentComment.text
        cell.date.text = " ∙ \(currentComment.date!)"
        
        cell.replyButton.addTarget(self, action: #selector(self.didPressReplyButton(_:)), for: .touchUpInside)
        
        cell.menuButton.addTarget(self, action: #selector(self.didPressMenuButton(_:)), for: .touchUpInside)
        cell.menuButton.tag = indexPath.row
        
        cell.selectionStyle = .none
        cell.backgroundColor = uiElement.currentTheme().backgroundColor
        
        //different color shows behind cell .. getting superview color to match cell
        cell.superview?.backgroundColor = uiElement.currentTheme().backgroundColor
        
        return cell
    }

    //Mark: auto-complete
    var people = [MEAContacts]()
    var filteredPeople = [MEAContacts]()
    let contactsReuse = "contactsReuse"
    
    func setUpAutoComplete() {
        self.autoCompletionView.register(MainTableViewCell.classForCoder(), forCellReuseIdentifier: contactsReuse)
    }
    
    func peopleAutoCompleteCell(_ indexPath: IndexPath) -> MainTableViewCell {
        var cell: MainTableViewCell!
        
        cell = self.autoCompletionView.dequeueReusableCell(withIdentifier: contactsReuse) as? MainTableViewCell
        
        let contact = self.filteredPeople[indexPath.row]
        cell.actionButton.isHidden = true
        
        let personImage = uiElement.currentTheme().personImage
        
        if let userImageURL = contact.userImageURL {
            cell.userImage.kf.setImage(with: URL(string: userImageURL ), placeholder: personImage, options: nil, progressBlock: nil, completionHandler: nil)
            
        } else {
            cell.userImage.image = personImage
        }
        
        if let username = contact.username {
            cell.username.text = "@\(username)"
            
        } else {
            cell.username.text = contact.phoneNumber
        }
        
        if let name = contact.displayName {
            cell.name.text = name
            
        } else {
            cell.name.text = ""
        }
        
        cell.selectionStyle = .default
        cell.backgroundColor = uiElement.currentTheme().backgroundColor
        
        //different color shows behind cell .. getting superview color to match cell
        cell.superview?.backgroundColor = uiElement.currentTheme().backgroundColor
        cell.hashtag.textColor = uiElement.currentTheme().hashTagColor
        
        return cell
    }
    
    @objc func didPressReplyButton(_ sender: UIButton) {
        self.textView.text = "@\(self.comments[sender.tag].username!) "
        self.textView.becomeFirstResponder()
        
        appendUserToMentions()
    }
    
    override func shouldProcessTextForAutoCompletion() -> Bool {
        return true
    }
    
    override func didChangeAutoCompletionPrefix(_ prefix: String, andWord word: String) {
        if prefix == "@" {
            let wordPredicate = NSPredicate(format: "self BEGINSWITH[c] %@", word)
            
            if word.count == 0 {
                self.filteredPeople = self.people
                
            } else {
                //filter users on MeArchive that are in current user's phone
                var filteredPeople = [MEAContacts]()
                filteredPeople = self.people.filter {
                    wordPredicate.evaluate(with: $0.username) ||
                        wordPredicate.evaluate(with: $0.displayName)
                }
                
                filteredPeople.sort(by: {$0.displayName! < $1.displayName!})
                self.filteredPeople = filteredPeople
            }
            self.showAutoCompletionView(true)
        }
    }
    
    override func heightForAutoCompletionView() -> CGFloat {
        return 50 * CGFloat(filteredPeople.count)
    }
    
    //mark: text
    override func textViewDidChange(_ textView: UITextView) {
        if textView.text.hasSuffix(" ") && !self.textView.text.isEmpty {
            self.appendUserToMentions()
        }
    }
    
    func appendUserToMentions() {
        let mentionsInTextView = Text().getElementInTextview(self.textView.text, element: "@")
        var newMentions = [MEAContacts]()
        for i in 0..<self.people.count {
            for mention in mentionsInTextView {
                if mention == "@\(self.people[i].username!)" {
                    newMentions.append(self.people[i])
                    break
                }
            }
        }
        
        self.mentionsInTextView = newMentions
    }
    
    //this func won't work if sender isn't declared as Any
    override func didCommitTextEditing(_ sender: Any) {
        self.editComment?.text = self.textView.text
        
        self.textView.text = ""
        
        //update tableview with updated data.. object place could have changed because of #s so have to make sure it matches before updating comments
        var didFindMatchingPost = false
        if self.comments.count != 0 {
            for i in 0..<self.comments.count {
                if self.comments[i].objectId == self.editComment?.objectId {
                    self.comments[i] = self.editComment!
                    didFindMatchingPost = true
                }
            }
            
        } else {
            self.comments.insert(self.editComment!, at: 0)
        }
        
        if !didFindMatchingPost {
            self.comments.insert(self.editComment!, at: 0)
        }
        
        self.updateComment(self.editComment!)
        
        self.tableView?.reloadData()
        
        super.didCommitTextEditing(sender)
    }
    
    func setUpEditText(_ row: Int) {
        self.textView.text = self.comments[row].text
        editComment = self.comments[row]
        self.textInputbar.beginTextEditing()
        self.textView.becomeFirstResponder()
    }
    
    //this func won't work if sender isn't declared as Any
    override func didCancelTextEditing(_ sender: Any) {
        self.textView.text = "#"
        super.didCancelTextEditing(sender)
    }
    
    //mark: button actions
    override func didPressRightButton(_ sender: Any?) {
        let newComment = Comment(objectId: nil, text: self.textView.text, date: CustomDate().formatDate(date: Date()), userId: PFUser.current()!.objectId!, username: currentUsername, name: currentUserDisplayName, userImageURL: currentUserImageURL, postId: self.note!.objectId!)
        self.comments.append(newComment)
        
        self.postNewComment(newComment, note: self.note!)
        self.textView.text = ""
        self.tableView?.reloadData()
    }
    
    @objc func didPressMentionsButton(_ sender: UIButton) {
        //isShowingPeopleCompleted = false
        //self.selectedIndex = sender.tag
        //self.performSegue(withIdentifier: showCompleted, sender: self)
    }
    
    @objc func didPressMenuButton(_ sender: UIButton) {
        let row = sender.tag
        let currentPost = self.comments[row]
        let menuAlert = UIAlertController(title: nil , message: nil , preferredStyle: .actionSheet)
        menuAlert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
        if currentPost.userId == PFUser.current()!.objectId! {
            menuAlert.addAction(UIAlertAction(title: "Delete", style: .default, handler: { action in
                let deleteAlert = UIAlertController(title: "Delete Comment?", message: nil, preferredStyle: .alert)
                deleteAlert.addAction(UIAlertAction(title: "NO", style: .cancel, handler: nil))
                deleteAlert.addAction(UIAlertAction(title: "YES", style: .default, handler: { action in
                    self.deletePost(currentPost.objectId!, row: row)
                    self.comments.remove(at: row)
                    self.tableView?.reloadData()
                }))
                self.present(deleteAlert, animated: true, completion: nil)
            }))
            
            menuAlert.addAction(UIAlertAction(title: "Edit", style: .default, handler: { action in
                self.setUpEditText(row)
            }))
        }
        
        self.present(menuAlert, animated: true, completion: nil)
    }
    
    //mark: data
    func postNewComment(_ comment: Comment, note: Post) {
        let newComment = PFObject(className: "Comment")
        newComment["userId"] = comment.userId
        newComment["text"] = comment.text
        newComment["postId"] = note.objectId
        let mentionUserIds = mentionsInTextView.map {$0.userId}
        newComment["mentions"] = mentionUserIds
        newComment["isRemoved"] = false
        newComment["postUserId"] = note.userId
        newComment.saveEventually {
            (success: Bool, error: Error?) -> Void in
            if (success) {
                self.comments[0].objectId = newComment.objectId
                
                if let name = self.name {
                    if comment.userId == note.userId {
                        if let noteMentionUsers = note.mentions {
                            for user in noteMentionUsers {
                                self.uiElement.sendAlert("\(name): '\(comment.text!)'", toUserId: user)
                            }
                        }
                        
                    } else {
                        self.uiElement.sendAlert("\(name): '\(comment.text!)'", toUserId: note.userId)
                        
                        for userId in mentionUserIds {
                            self.uiElement.sendAlert("\(name): '\(comment.text!)'", toUserId: userId!)
                        }
                    }
                }
            }
        }
    }
    
    func updateComment(_ comment: Comment) {
        let query = PFQuery(className: "Comment")
        query.getObjectInBackground(withId: comment.objectId!) {
            (object: PFObject?, error: Error?) -> Void in
            if error != nil {
                //print(error)
                
            } else if let object = object {
                object["text"] = comment.text
                
                object["mentions"] = self.mentionsInTextView
                object.saveEventually()
            }
        }
    }
    
    func loadComments(_ objectId: String) {
        let query = PFQuery(className: "Comment")
        query.whereKey("postId", equalTo: objectId)
        query.whereKey("isRemoved", equalTo: false)
        query.addDescendingOrder("createdAt")
        query.findObjectsInBackground {
            (objects: [PFObject]?, error: Error?) -> Void in
            if error == nil {
                if let objects = objects {
                    for object in objects {
                        let text = object["text"] as! String
                        let date = CustomDate().formatDate(date: object.createdAt!)
                        let userId = object["userId"] as! String
                        
                        let newComment = Comment(objectId: object.objectId, text: text, date: date, userId: userId, username: nil, name: nil, userImageURL: nil, postId: objectId)
                        
                        self.comments.append(newComment)
                    }
                    
                    self.tableView?.reloadData()
                }
                
            } else {
                print("Error: \(error!)")
            }
        }
    }
    
    func loadUserInfoFromCloud(_ row: Int) {
        let comment = comments[row]
        
        let query = PFQuery(className:"_User")
        query.getObjectInBackground(withId: comment.userId) {
            (user: PFObject?, error: Error?) -> Void in
            if let error = error {
                print(error)
                
            } else if let user = user {
                if let nameObject = user["name"] as? String {
                    comment.name = nameObject
                    
                } else {
                    comment.name = "Name"
                }
                
                if let usernameObject = user["username"] as? String {
                    comment.username = usernameObject
                    
                } else {
                    comment.username = "Username"
                }
                
                if let userImageFile = user["userImage"] as? PFFile {
                    comment.userImageURL = userImageFile.url
                }
                
                self.comments[row] = comment
                
                self.tableView?.reloadData()
            }
        }
    }
    
    func loadPeopleMentionedInNote() {
        let query = PFQuery(className: "_User")
        query.whereKey("objectId", containedIn: mentionedPeopleInNote)
        query.findObjectsInBackground {
            (objects: [PFObject]?, error: Error?) -> Void in
            if error == nil {
                if let objects = objects {
                    for user in objects {
                        let newUser = MEAContacts(userId: user.objectId, displayName: nil, username: nil, userImageURL: nil, phoneNumber: nil, isFriendshipConfirmed: nil, postObjectId: nil)
                        
                        if let nameObject = user["name"] as? String {
                            newUser.displayName = nameObject
                            
                        } else {
                            newUser.displayName = "Name"
                        }
                        
                        if let usernameObject = user["username"] as? String {
                            newUser.username = usernameObject
                            
                        } else {
                            newUser.username = "Username"
                        }
                        
                        if let userImageFile = user["userImage"] as? PFFile {
                            newUser.userImageURL = userImageFile.url
                        }
                        
                        self.people.append(newUser)
                    }
                    self.tableView?.reloadData()
                }
                
            } else {
                print("Error: \(error!)")
            }
        }
    }
    
    func deletePost(_ objectId: String, row: Int) {
        let query = PFQuery(className: "Comment")
        query.whereKey("objectId", equalTo: objectId)
        query.whereKey("userId", equalTo: PFUser.current()!.objectId!)
        query.getFirstObjectInBackground {
            (object: PFObject?, error: Error?) -> Void in
            if error != nil || object == nil {
                print("The getFirstObject request failed.")
                
            } else {
                object!["isRemoved"] = true
                object?.saveEventually()
            }
        }
    }
    
    func loadCurrentUserInfo() {
        let query = PFQuery(className:"_User")
        query.getObjectInBackground(withId: PFUser.current()!.objectId!) {
            (user: PFObject?, error: Error?) -> Void in
            if let error = error {
                print(error)
                
            } else if let user = user {
                var name = ""
                
                if let displayName = user["name"] as? String {
                    name = displayName
                    
                } else if let username = user["username"] as? String  {
                    name = username 
                }
                
                self.name = name
                self.uiElement.setDefaultUserDisplayName(name )
            }
        }
    }
    
    func loadPostUserInfo() {
        let query = PFQuery(className:"_User")
        query.getObjectInBackground(withId: self.note!.userId) {
            (user: PFObject?, error: Error?) -> Void in
            if let error = error {
                print(error)
                
            } else if let user = user {
                if let nameObject = user["name"] as? String {
                    self.note?.name = nameObject
                    
                } else {
                    self.note?.name = "Name"
                }
                
                if let usernameObject = user["username"] as? String {
                    self.note?.username = usernameObject
                    
                } else {
                    self.note?.username = "Username"
                }
                
                if let userImageFile = user["userImage"] as? PFFile {
                    self.note?.userImageURL = userImageFile.url
                }
                
                self.tableView?.reloadData()
            }
        }
    }
    
    func loadNote(_ objectId: String) {
        let query = PFQuery(className:"Post")
        query.getObjectInBackground(withId: objectId) {
            (object: PFObject?, error: Error?) -> Void in
            if let error = error {
                print(error)
                
            } else if let object = object {
                self.note = PostObject().createNewPostObject(object)
                if !self.mentionedPeopleInNote.contains(self.note!.userId) {
                    self.mentionedPeopleInNote.append(self.note!.userId)
                }
                
                self.loadPeopleMentionedInNote()
                self.loadPostUserInfo()
            }
        }
    }
}
