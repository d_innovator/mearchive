//
//  PostObject.swift
//  MeAtlas
//
//  Created by Dominic  Smith on 8/21/18.
//  Copyright © 2018 Dominic Smith. All rights reserved.
//

import Foundation
import Parse

class PostObject {
    func createNewPostObject(_ object: PFObject) -> Post {
        let post = Post(objectId: object.objectId!, text: "place holder", date: object.createdAt!, reminder: nil, todo: nil, media: nil, userId: object["userId"] as! String, username: nil, name: nil, userImageURL: nil, completed: nil, mentions: nil, commentText: nil, commentUserId: nil, commentObjectId: nil, commentDate: nil, audio: nil)
        
        //there may be some text still saved in plain text, so need to check
        if let text = object["text"] as? String {
            post.text = text
            
        } else if let textData = object["encryptedText"] as? Data {
            let decryptedTextData = Secure().decrypt(textData, password: Secure().getKey())
            let text = String(data: decryptedTextData, encoding: String.Encoding.utf8) ?? "Encryption Key Mismatch"
            post.text = text
        }
        
        if let todo = object["todo"] as? Bool {
            post.todo = todo
        }
        
        if let media = object["media"] as? PFFile {
            media.getDataInBackground(block: {
                (imageData: Data!, error: Error!) -> Void in
                if (error == nil) {
                    post.media = UIImage(data: imageData)
                }
            })
            
        } else if let mediaData = object["encryptedMedia"] as? Data {
            let decryptedMediaData = Secure().decrypt(mediaData, password: Secure().getKey())
            post.media = UIImage(data: decryptedMediaData, scale: 1.0)
        }
        
        if let audio = object["audio"] as? PFFile {
            audio.getDataInBackground(block: {
                (audioData: Data!, error: Error!) -> Void in
                if (error == nil) {
                    post.audio = audioData
                }
            })
            
        } else if let audioData = object["encryptedAudio"] as? Data {
            let decryptedAudioData = Secure().decrypt(audioData, password: Secure().getKey())
            post.audio = decryptedAudioData
        }
        
        if let completed = object["completed"] as? Array<String> {
            post.completed = completed
            
        } else {
            post.completed = []
        }
        
        if let mentions = object["mentions"] as? Array<String> {
            post.mentions = mentions
            
        } else {
            post.mentions = []
        }
        
        Reminder().reminderCenter.getPendingNotificationRequests(completionHandler: { reminders in
            for reminder in reminders {
                if reminder.identifier == post.objectId! {
                    post.reminder = reminder
                    break
                }
            }
        })
        
        return post
    }
}
