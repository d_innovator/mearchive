//
//  Secure.swift
//  MeAtlas
//
//  Created by Dominic  Smith on 4/30/18.
//  Copyright © 2018 Dominic Smith. All rights reserved.
//

import Foundation
import RNCryptor
import KeychainAccess
import Parse

class Secure {
    func encrypt(_ data: Data, password: String) -> Data {
        let ciphertext = RNCryptor.encrypt(data: data, withPassword: password)
        return ciphertext
    }
    
    func decrypt(_ cipherText: Data, password: String) -> Data {
        do {
            let originalData = try RNCryptor.decrypt(data: cipherText, withPassword: password)
            return originalData
            
        } catch {
            print(error)
        }
        return cipherText
    }
    
    func generateKey() -> String {
        if let userId = PFUser.current()?.objectId {
            let password = Keychain.generatePassword()
            let keychain = Keychain(service: "com.mearchive.\(userId)")
            do {
                try keychain
                    .synchronizable(true)
                    .set(password, key: "MeArchive")
                return password
                
            } catch let error {
                return "error: \(error)"
            }
            
        } else {
            return "error: User not signed in."
        }
    }
    
    func getKey() -> String {
        if let userId = PFUser.current()?.objectId {
            let keychain = Keychain(service: "com.mearchive.\(userId)")
            let token = keychain["MeArchive"]
            return token ?? "nil"
            
        } else {
            return "error: User not signed in."
        }
    }
    
    func setKey(_ key: String) -> Bool {
        if let userId = PFUser.current()?.objectId {
            let keychain = Keychain(service: "com.mearchive.\(userId)")
            do {
                try keychain
                    .synchronizable(true)
                    .set(key, key: "MeArchive")
                return true
                
            } catch _ {
                return false
            }
            
        } else {
            return false
        }
    }
    
    func removeKey() {
        let keychain = Keychain(service: "com.meArchive.meArchive")
        do {
            try keychain.remove("MeArchive")
        } catch let error {
            print("error: \(error)")
        }
    }
    
    func getPasswordForEncryption() -> String {
        var password = getKey()
        if password == "nil" {
            password = generateKey()
            if !password.hasPrefix("error:") {
                return password
                
            } else {
                return "error"
            }
            
        } else {
            return password
        }
    }
    
}
