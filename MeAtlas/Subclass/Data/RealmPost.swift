//
//  RealmPost.swift
//  MeAtlas
//
//  Created by Dominic  Smith on 6/4/18.
//  Copyright © 2018 Dominic Smith. All rights reserved.
//

import RealmSwift
import UserNotifications

class RealmPost: Object {
    @objc dynamic var userId: String! = ""
    @objc dynamic var objectId: String? = nil
    @objc dynamic var isSavedToCloud = false //needed so that app knows if object is saved to cloud database
    @objc dynamic var text: String! = ""
    @objc dynamic var date = Date(timeIntervalSince1970: 1)
    @objc dynamic var todo = false 
    @objc dynamic var media: Data? = nil
    @objc dynamic var audio: Data? = nil
}
