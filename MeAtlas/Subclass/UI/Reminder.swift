//
//  Reminder.swift
//  MeAtlas
//
//  Created by Dominic  Smith on 6/27/18.
//  Copyright © 2018 Dominic Smith. All rights reserved.
//

import Foundation
import UserNotifications
import UIKit

class Reminder {
    let reminderCenter = UNUserNotificationCenter.current()
    let reminderReuse = "reminderReuse"
    let reminderDateFormat = "MMMM dd, yyyy 'at' hh:mm a"
    
    func set(_ objectId: String, text: String, date: Date?, target: UIViewController) -> UNNotificationRequest? {
        
        let content = UNMutableNotificationContent()
        content.body = text
        content.sound = UNNotificationSound.default
        
        var request: UNNotificationRequest!
        
        if let date = date {
            let trigger = UNCalendarNotificationTrigger(dateMatching: Calendar.current.dateComponents([.minute, .hour, .day], from: date), repeats: false)
            
            request = UNNotificationRequest(identifier: objectId,
                                            content: content, trigger: trigger)
        }
        
        self.reminderCenter.add(request, withCompletionHandler: { (error) in
            if let error = error {
                print("ERROR WITH ADDING REMINDER: \(error)")
            }
        })
        return request
    }
}
