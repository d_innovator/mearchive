//
//  SettingsViewController.swift
//  MeAtlas
//
//  Created by Dominic  Smith on 4/23/18.
//  Copyright © 2018 Dominic Smith. All rights reserved.
//

import UIKit
import Parse
import LocalAuthentication
import RealmSwift
import Kingfisher

class SettingsViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    let tableView = UITableView()
    let uiElement = UIElement()
    let color = Color()
    
    let settingsLabels = ["Edit Profile", "Add Friends", "Show Encryption Key", "Local Data"]
    let settingsImages = [UIElement().currentTheme().personImage, #imageLiteral(resourceName: "plus_green"), UIElement().currentTheme().shieldImage, UIElement().currentTheme().dataImage]
    let switchLabels = ["Hide Completed Posts", UIElement().currentTheme().switchText, "Require Touch/Face ID"]
    let appURL = URL(string: "https://itunes.apple.com/us/app/mearchive/id1382532452?mt=8")!
    
    var userImageURL: String?
    
    lazy var userImage: UIImageView = {
        let image = UIImageView()
        image.image = #imageLiteral(resourceName: "person_black")
        image.layer.cornerRadius = 25
        image.clipsToBounds = true
        image.contentMode = .scaleAspectFill
        image.image = uiElement.currentTheme().personImage
        return image
    }()
    
    lazy var name: UILabel = {
        let label = UILabel()
        label.font = UIFont(name: uiElement.mainFont, size: 20)
        label.textColor = uiElement.currentTheme().textColor
        label.text = "Name"
        return label
    }()
    
    lazy var username: UILabel = {
        let label = UILabel()
        label.font = UIFont(name: uiElement.mainFont, size: 17)
        label.textColor = UIColor.darkGray
        label.text = "@username"
        return label
    }()
    
    lazy var friendsView: UIButton = {
        let button = UIButton()
        return button
    }()
    
    lazy var friendsCount: UILabel = {
        let label = UILabel()
        label.font = UIFont(name: uiElement.mainFont, size: 17)
        label.textColor = uiElement.currentTheme().textColor
        label.text = "Loading"
        return label
    }()
    
    lazy var friendsLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont(name: uiElement.mainFont, size: 17)
        label.textColor = UIColor.darkGray
        label.text = " Friends"
        return label
    }()

    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = UIElement().currentTheme().backgroundColor
        navigationController?.navigationBar.barTintColor = UIElement().currentTheme().backgroundColor
        navigationController?.navigationBar.tintColor = UIElement().currentTheme().textColor
        navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIElement().currentTheme().textColor]
        
        let menuButton = UIBarButtonItem(title: "...", style: .plain, target: self, action: #selector(self.didPressMenuButton(_:)))
        self.navigationItem.rightBarButtonItem = menuButton
        
        setUpViews()
        loadUserInfoFromCloud(PFUser.current()!.objectId!)
        self.loadFriends()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        self.resignFirstResponder()
    }
    
    let reuse = "reuse"
    let uiSwitchReuse = "uiSwitchReuse"
    
    func setUpViews() {
        self.title = "Profile"
        
        tableView.delegate = self
        tableView.dataSource = self
        tableView.register(SettingsTableViewCell.self, forCellReuseIdentifier: reuse)
        tableView.register(SettingsTableViewCell.self, forCellReuseIdentifier: uiSwitchReuse)
        tableView.backgroundColor = UIElement().currentTheme().backgroundColor
        tableView.separatorColor = .darkGray
        self.tableView.separatorStyle = .none
        self.view.addSubview(tableView)
        
        self.view.addSubview(userImage)
        self.view.addSubview(name)
        self.view.addSubview(username)
        self.view.addSubview(friendsView)
        friendsView.addTarget(self, action: #selector(self.didPressFriendsButton(_:)), for: .touchUpInside)
        self.friendsView.addSubview(friendsCount)
        self.friendsView.addSubview(friendsLabel)
        
        userImage.snp.makeConstraints { (make) -> Void in
            make.height.width.equalTo(50)
            make.top.equalTo(self.view).offset(uiElement.uiViewTopOffset(self))
            make.left.equalTo(self.view).offset(uiElement.leftOffset)
        }
        
        name.snp.makeConstraints { (make) -> Void in
            make.top.equalTo(userImage.snp.bottom).offset(uiElement.elementOffset)
            make.left.equalTo(self.view).offset(uiElement.leftOffset)
            make.right.equalTo(self.view).offset(uiElement.rightOffset)
        }
        
        username.snp.makeConstraints { (make) -> Void in
            make.top.equalTo(self.name.snp.bottom).offset(1)
            make.left.equalTo(name)
            make.right.equalTo(self.view).offset(uiElement.rightOffset)
        }
        
        friendsView.snp.makeConstraints { (make) -> Void in
            make.top.equalTo(self.username.snp.bottom).offset(1)
            make.left.equalTo(name)
            make.right.equalTo(self.view).offset(uiElement.rightOffset)
        }
        
        friendsCount.snp.makeConstraints { (make) -> Void in
            make.top.equalTo(friendsView)
            make.left.equalTo(friendsView)
            make.bottom.equalTo(friendsView)
        }
        
        friendsLabel.snp.makeConstraints { (make) -> Void in
            make.top.equalTo(friendsView)
            make.left.equalTo(friendsCount.snp.right)
            make.right.equalTo(friendsView)
            make.bottom.equalTo(friendsView)
        }
        
        tableView.snp.makeConstraints { (make) -> Void in
            make.top.equalTo(friendsView.snp.bottom).offset(uiElement.topOffset)
            make.left.equalTo(self.view)
            make.right.equalTo(self.view)
            make.bottom.equalTo(self.view)
        }
    }
    
    //MARK: TableView
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0 {
            return settingsLabels.count
            
        } else {
            return switchLabels.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var cell: SettingsTableViewCell!
         if indexPath.section == 0 {
            cell = tableView.dequeueReusableCell(withIdentifier: reuse, for: indexPath) as? SettingsTableViewCell
            tableView.separatorStyle = .none
            cell.settingsLabel.text = settingsLabels[indexPath.row]
            if indexPath.row == 1 {
                cell.settingsLabel.textColor = Color().green()
                
            } else {
                cell.settingsLabel.textColor = UIElement().currentTheme().textColor
            }
            
            cell.settingsImage.image = settingsImages[indexPath.row]
            
        } else {
            cell = tableView.dequeueReusableCell(withIdentifier: uiSwitchReuse, for: indexPath) as? SettingsTableViewCell
            tableView.separatorStyle = .none
            
            cell.uiSwitch.addTarget(self, action: #selector(self.didPressSwitch(_:)), for: .valueChanged)
            cell.uiSwitch.tag = indexPath.row
            cell.settingsLabel.text = switchLabels[indexPath.row]
            cell.settingsLabel.textColor = UIElement().currentTheme().textColor
            
            switch indexPath.row{
            case 0:
                if UserDefaults.standard.bool(forKey: "shouldHideCompletes") {
                    cell.uiSwitch.isOn = true
                }
                break
                
            case 1:
                if UIElement().currentTheme() == .darkTheme {
                    cell.uiSwitch.isOn = true
                }
                break
                
            case 2:
                if UserDefaults.standard.bool(forKey: "requireTouchID") {
                    cell.uiSwitch.isOn = true
                }
                break
                
            default:
                break
            }
        }
        
        cell.backgroundColor = UIElement().currentTheme().backgroundColor
        cell.selectionStyle = .none
    
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.section == 0 {
            switch indexPath.row {
            case 0:
                self.uiElement.segueToView("Main", withIdentifier: "editProfile", target: self)
                break
                
            case 1:
                self.uiElement.segueToView("Main", withIdentifier: "addFriends", target: self)
                break
                
            case 2:
                self.uiElement.segueToView("Main", withIdentifier: "encryption", target: self)
                break
                
            case 3:
                didPressDataButton()
                break
                
            default:
                break
            }
        }
    }
    
    //MARK: Segment
    @objc func didPressSwitch(_ sender: UISwitch) {
        switch sender.tag {
        case 0:
            UserDefaults.standard.set(sender.isOn, forKey: "shouldHideCompletes")
            self.uiElement.segueToView("Main", withIdentifier: "main", target: self)
            break
            
        case 1:
            if sender.isOn {
                UIElement().applyTheme(.darkTheme)
                
            } else {
                UIElement().applyTheme(.lightTheme)
            }
            
            self.uiElement.segueToView("Main", withIdentifier: "main", target: self)
            break
            
        case 2:
            UserDefaults.standard.set(sender.isOn, forKey: "requireTouchID")
            UserDefaults.standard.synchronize()
            break
            
        default:
            break
        }
    }
    
    func loadUserInfoFromCloud(_ userId: String) {
        let query = PFQuery(className:"_User")
        query.getObjectInBackground(withId: userId) {
            (user: PFObject?, error: Error?) -> Void in
            if let error = error {
                print(error)
                
            } else if let user = user {
                var name: String!
                var username: String!
                
                if let nameObject = user["name"] as? String {
                    name = nameObject
                    
                } else {
                    name = "Name"
                }
                
                if let usernameObject = user["username"] as? String {
                    username = usernameObject
                    
                } else {
                    username = "Username"
                }
                
                self.name.text = name
                self.username.text = "@\(username!)"
                let personImage = self.uiElement.currentTheme().personImage
                if let userImageFile = user["userImage"] as? PFFile {
                    self.userImage.kf.setImage(with: URL(string: userImageFile.url!), placeholder: personImage , options: nil, progressBlock: nil, completionHandler: nil)
                }
            }
        }
    }
    
    func saveUserDataToPhone(name: String?, username: String?, userImageURL: String?, userId: String!) {
        if let userId = PFUser.current()?.objectId {
            let realmObject = RealmUser()
            realmObject.userId = userId
            
            if let name = name {
                realmObject.name = name
            }
            
            if let username = username {
                realmObject.username = username
            }
            
            if let userImageURL = userImageURL {
                realmObject.userImageURL = userImageURL
            }
            
            let realm = try! Realm()
            try! realm.write {
                realm.add(realmObject)
            }
        }
    }
    
    func loadFriends() {
        let fromCurrentUserQuery = PFQuery(className: "Friends")
        fromCurrentUserQuery.whereKey("fromUserId", equalTo:  PFUser.current()!.objectId!)
        
        let toCurrentUserQuery = PFQuery(className: "Friends")
        toCurrentUserQuery.whereKey("toUserId", equalTo:  PFUser.current()!.objectId!)
        
        let query = PFQuery.orQuery(withSubqueries: [fromCurrentUserQuery, toCurrentUserQuery])
        query.whereKey("isRemoved", equalTo: false)
        query.whereKey("isConfirmed", equalTo: true)
        query.findObjectsInBackground {
            (objects: [PFObject]?, error: Error?) -> Void in
            if error == nil {
                if let objects = objects {
                    self.friendsCount.text = "\(objects.count)"
                }
                
            } else {
                print("Error: \(error!)")
            }
        }
    }
    
    @objc func didPressFriendsButton(_ sender: UIButton) {
        self.uiElement.segueToView("Main", withIdentifier: "friends", target: self)
    }
    
    @objc func didPressMenuButton(_ sender: UIButton) {
        let menuAlert = UIAlertController(title: nil , message: nil , preferredStyle: .actionSheet)
        menuAlert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
        menuAlert.addAction(UIAlertAction(title: "Sign Out", style: .default, handler: { action in
            PFUser.logOut()
            let storyboard = UIStoryboard(name: "Login", bundle: nil)
            let controller = storyboard.instantiateViewController(withIdentifier: "welcome")
            let appDelegate = UIApplication.shared.delegate as! AppDelegate
            //show window
            appDelegate.window?.rootViewController = controller
        }))
        
        self.present(menuAlert, animated: true, completion: nil)
    }
    
    func didPressDataButton() {
        let menuAlert = UIAlertController(title: "Local Data", message: "MeArchive stores all your posts locally and in the cloud. You can delete all your local posts if you're running low on phone space. Posts saved to the cloud will still load to your archive.", preferredStyle: .actionSheet)
        menuAlert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
        menuAlert.addAction(UIAlertAction(title: "Delete all local posts", style: .default, handler: { action in
            self.areYouSureYouWantToDeleteAllLocalDataAlert()

        }))
        
        self.present(menuAlert, animated: true, completion: nil)
    }
    
    func areYouSureYouWantToDeleteAllLocalDataAlert() {
        let menuAlert = UIAlertController(title: "Are you sure?", message: nil, preferredStyle: .alert)
        menuAlert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
        menuAlert.addAction(UIAlertAction(title: "Yes, delete all local posts", style: .default, handler: { action in
            self.deleteRealmFromDisk()
        }))
        
        self.present(menuAlert, animated: true, completion: nil)
    }
    
    func deleteRealmFromDisk() {
        let realmURL = Realm.Configuration.defaultConfiguration.fileURL!
        let realmURLs = [
            realmURL,
            realmURL.appendingPathExtension("lock"),
            realmURL.appendingPathExtension("note"),
            realmURL.appendingPathExtension("management")
        ]
        for URL in realmURLs {
            do {
                try FileManager.default.removeItem(at: URL)
                
            } catch {
                // handle error
            }
        }
        
        self.uiElement.segueToView("Main", withIdentifier: "main", target: self)
    }
}
