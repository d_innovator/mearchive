//
//  FriendsViewController.swift
//  MeAtlas
//
//  Created by Dominic  Smith on 8/1/18.
//  Copyright © 2018 Dominic Smith. All rights reserved.
//

import UIKit
import Parse
import Kingfisher

class FriendsViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    var friends = [MEAContacts]()
    var tableView = UITableView()
    
    var noResultsReuse = "noResultsReuse"
    var contactsReuse = "contactsReuse"
    
    var name: String?

    override func viewDidLoad() {
        super.viewDidLoad()
        loadFriends()
        setUpViews()
        setUpTabBar()
        
        if let name = UIElement().getCurrentDefaultUserDisplayName() {
            self.name = name
            
        } else {
            self.loadCurrentUserInfo()
        }
    }
    
    func setUpViews() {
        tableView.delegate = self
        tableView.dataSource = self
        tableView.register(MainTableViewCell.self, forCellReuseIdentifier: contactsReuse)
        tableView.register(MainTableViewCell.self, forCellReuseIdentifier: noResultsReuse)
        tableView.backgroundColor = UIElement().currentTheme().backgroundColor
        tableView.separatorColor = .darkGray
        tableView.keyboardDismissMode = .onDrag
        tableView.separatorStyle = .none
        tableView.frame = view.bounds
        view.addSubview(tableView)
    }
    
    func setUpTabBar() {
        self.view.backgroundColor = UIElement().currentTheme().backgroundColor
        navigationController?.navigationBar.barTintColor = UIElement().currentTheme().backgroundColor
        navigationController?.navigationBar.tintColor = UIElement().currentTheme().textColor
        navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIElement().currentTheme().textColor]
        
        let doneButton = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(didPressDoneButton(_:)))
        self.navigationItem.rightBarButtonItem = doneButton
    }
    
    //mark: tableview
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return "Friends"
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if self.friends.count != 0 {
            return friends.count
        }
        
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var cell: MainTableViewCell
        
        if self.friends.count != 0 {
            cell = self.tableView.dequeueReusableCell(withIdentifier: contactsReuse) as! MainTableViewCell
            let friend = self.friends[indexPath.row]
            
            cell.actionLabel.text = "Friends"
            cell.actionButton.addTarget(self, action: #selector(self.didPressFriendsButton(_:)), for: .touchUpInside)
            cell.actionButton.tag = indexPath.row
            if let isConfirmed = friend.isFriendshipConfirmed {
                if isConfirmed {
                    cell.actionButton.layer.borderWidth = 1
                    cell.actionButton.layer.borderColor = UIElement().currentTheme().textColor.cgColor
                    cell.actionButton.backgroundColor = .clear
                    cell.actionLabel.text = "Friends"
                    cell.actionLabel.textColor = UIElement().currentTheme().textColor
                    
                } else {
                    cell.actionButton.layer.borderWidth = 0
                    cell.actionButton.backgroundColor = .darkGray
                    cell.actionLabel.text = "Pending"
                    cell.actionLabel.textColor = .white
                }

            } else {
                cell.actionButton.layer.borderWidth = 0
                cell.actionButton.backgroundColor = Color().blue()
                cell.actionLabel.text = "Add"
                cell.actionLabel.textColor = .white 
            }
            
            let personImage = UIElement().currentTheme().personImage
            if let userImageURL = friend.userImageURL {
                cell.userImage.kf.setImage(with: URL(string: userImageURL ), placeholder: personImage, options: nil, progressBlock: nil, completionHandler: nil)
                
            } else {
                cell.userImage.image = personImage
            }
            
            if let username = friend.username {
                cell.username.text = "@\(username)"
                
            } else {
                cell.username.text = friend.phoneNumber
            }
            
            if let name = friend.displayName {
                cell.name.text = name
                
            } else {
                cell.name.text = ""
            }
            
        } else {
            cell = self.tableView.dequeueReusableCell(withIdentifier: noResultsReuse) as! MainTableViewCell
            cell.bodyLabel.text = "No friends yet."
        }
        
        cell.selectionStyle = .none
        cell.backgroundColor = UIElement().currentTheme().backgroundColor
        
        //different color shows behind cell .. getting superview color to match cell
        cell.superview?.backgroundColor = UIElement().currentTheme().backgroundColor
        cell.hashtag.textColor = UIElement().currentTheme().hashTagColor
        
        return cell
    }
    
    //mark: button actions
    @objc func didPressDoneButton(_ sender: UIBarButtonItem) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let controller = storyboard.instantiateViewController(withIdentifier: "main")
        self.present(controller, animated: true, completion: nil)
    }
    
    @objc func didPressFriendsButton(_ sender: UIButton) {
        if self.friends[sender.tag].isFriendshipConfirmed != nil{
            //removing friend regardless of whether or not it's confirmed
            self.removeFriend(row: sender.tag)
            
        } else {
            self.addFriend(row: sender.tag)
        }
    }
    
    //mark: data
    func loadFriends() {
        var userIds = [String]()
        
        let fromCurrentUserQuery = PFQuery(className: "Friends")
        fromCurrentUserQuery.whereKey("fromUserId", equalTo:  PFUser.current()!.objectId!)
        
        let toCurrentUserQuery = PFQuery(className: "Friends")
        toCurrentUserQuery.whereKey("toUserId", equalTo:  PFUser.current()!.objectId!)
        
        let query = PFQuery.orQuery(withSubqueries: [fromCurrentUserQuery, toCurrentUserQuery])
        query.whereKey("isRemoved", equalTo: false)
        query.whereKey("isConfirmed", equalTo: true)
        query.findObjectsInBackground {
            (objects: [PFObject]?, error: Error?) -> Void in
            if error == nil {
                if let objects = objects {
                    for object in objects {
                        let fromUserId = object["fromUserId"] as! String
                        let toUserId = object["toUserId"] as! String
                        
                        if fromUserId == PFUser.current()!.objectId! {
                            userIds.append(toUserId)
                            
                        } else {
                            userIds.append(fromUserId)
                        }
                    }
                }
                
                self.loadFriendInfo(friendUserIds: userIds)
            } else {
                print("Error: \(error!)")
            }
        }
    }
    
    func loadFriendInfo(friendUserIds: Array<String>) {
        let query = PFQuery(className: "_User")
        query.whereKey("objectId", containedIn: friendUserIds)
        query.findObjectsInBackground {
            (objects: [PFObject]?, error: Error?) -> Void in
            if error == nil {
                if let objects = objects {
                    for object in objects {
                        let userObject = MEAContacts(userId: object.objectId, displayName: nil, username: nil, userImageURL: nil, phoneNumber: nil, isFriendshipConfirmed: true, postObjectId: nil)
                        
                        if let name = object["name"] as? String {
                            userObject.displayName = name
                        }
                        
                        if let username = object["username"] as? String {
                            userObject.username = username
                        }
                        
                        if let userImageFile = object["userImage"] as? PFFile {
                            userObject.userImageURL = userImageFile.url!
                        }
                        
                        self.friends.append(userObject)
                    }
                }
                self.tableView.reloadData()
                
            } else {
                print("Error: \(error!)")
            }
        }
    }
    
    func addFriend(row: Int) {
        self.friends[row].isFriendshipConfirmed = false
        self.tableView.reloadData()
        
        let newFriend = PFObject(className: "Friends")
        newFriend["fromUserId"] = PFUser.current()!.objectId!
        newFriend["toUserId"] = self.friends[row].userId!
        newFriend["isConfirmed"] = false
        newFriend["isRemoved"] = false
        newFriend.saveEventually {
            (success: Bool, error: Error?) -> Void in
            if (success) {
                if let name = self.name {
                    UIElement().sendAlert("\(name) sent you a friend request.", toUserId: self.friends[row].userId!)
                }
            }
        }
    }
    
    func removeFriend(row: Int) {
        let friendUserId = self.friends[row].userId
        self.friends[row].isFriendshipConfirmed = nil
        self.tableView.reloadData()
        
        let fromCurrentUserQuery = PFQuery(className: "Friends")
        fromCurrentUserQuery.whereKey("fromUserId", equalTo:  PFUser.current()!.objectId!)
        fromCurrentUserQuery.whereKey("toUserId", equalTo: friendUserId!)
        
        let toCurrentUserQuery = PFQuery(className: "Friends")
        toCurrentUserQuery.whereKey("toUserId", equalTo:  PFUser.current()!.objectId!)
        toCurrentUserQuery.whereKey("fromUserId", equalTo: friendUserId!)
        
        let query = PFQuery.orQuery(withSubqueries: [fromCurrentUserQuery, toCurrentUserQuery])
        query.whereKey("isRemoved", equalTo: false)
        query.getFirstObjectInBackground {
            (object: PFObject?, error: Error?) -> Void in
            if error != nil || object == nil {
                print("The getFirstObject request failed.")
                
            } else {
                object!["isRemoved"] = true
                object?.saveEventually()
            }
        }
    }
    
    func loadCurrentUserInfo() {
        let query = PFQuery(className:"_User")
        query.getObjectInBackground(withId: PFUser.current()!.objectId!) {
            (user: PFObject?, error: Error?) -> Void in
            if let error = error {
                print(error)
                
            } else if let user = user {
                var name = ""
                if let displayName = user["name"] as? String {
                    name = displayName
                    
                } else if let username = user["username"] as? String {
                    name = username
                }
                
                self.name = name
                UIElement().setDefaultUserDisplayName(name)
                
            }
        }
    }
}
