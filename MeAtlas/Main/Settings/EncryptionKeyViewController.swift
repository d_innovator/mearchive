//
//  EncryptionKeyViewController.swift
//  MeAtlas
//
//  Created by Dominic  Smith on 5/17/18.
//  Copyright © 2018 Dominic Smith. All rights reserved.
//

import UIKit
import SnapKit

class EncryptionKeyViewController: UIViewController {
    
    lazy var titleLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont(name: UIElement().mainFont, size: 17)
        label.textColor = UIElement().currentTheme().textColor
        label.text = "Below is the encryption key used to encrypt your personal posts. We recommend that you store this key elsewhere in case MeArchive can't retrieve it."
        label.numberOfLines = 0
        return label
    }()
    
    lazy var encryptionKeyLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont(name: UIElement().mainFont, size: 35)
        label.textColor = UIElement().currentTheme().textColor
        label.textAlignment = .center
        label.numberOfLines = 0
        return label
    }()
    
    lazy var copyTextButton: UIButton = {
        let button = UIButton()
        button.titleLabel?.font = UIFont(name: UIElement().mainFont, size: 20)
        button.setTitle("Copy Encryption Key", for: .normal)
        button.backgroundColor = Color().meArchiveYellow()
        button.setTitleColor(Color().black(), for: .normal)
        button.layer.cornerRadius = 3
        button.clipsToBounds = true
        return button
    }()
    
    lazy var changeEncryptionKeyButton: UIButton = {
        let button = UIButton()
        button.titleLabel?.font = UIFont(name: UIElement().mainFont, size: 17)
        button.setTitle("Set Encryption Key", for: .normal)
        button.setTitleColor(UIElement().currentTheme().textColor, for: .normal)
        return button
    }()

    override func viewDidLoad() {
        super.viewDidLoad()
        
        setUpTabBar()
        setUpUI()
        getEncryptionKey()
    }
    
    func setUpTabBar() {
        self.view.backgroundColor = UIElement().currentTheme().backgroundColor
        navigationController?.navigationBar.barTintColor = UIElement().currentTheme().backgroundColor
        navigationController?.navigationBar.tintColor = UIElement().currentTheme().textColor
        navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIElement().currentTheme().textColor]
        
        let menuButton = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(self.doneAction(_:)))
        self.navigationItem.rightBarButtonItem = menuButton
    }
    
    func setUpUI() {
        self.view.addSubview(titleLabel)
        self.view.addSubview(encryptionKeyLabel)
        self.view.addSubview(changeEncryptionKeyButton)
        self.view.addSubview(copyTextButton)
        
        titleLabel.snp.makeConstraints { (make) -> Void in
            make.top.equalTo(view).offset(UIElement().uiViewTopOffset(self))
            make.left.equalTo(view).offset(UIElement().leftOffset)
            make.right.equalTo(view).offset(UIElement().rightOffset)
        }
        
        encryptionKeyLabel.snp.makeConstraints { (make) -> Void in
            make.top.equalTo(titleLabel.snp.bottom).offset(20)
            make.left.equalTo(view).offset(UIElement().leftOffset)
            make.right.equalTo(view).offset(UIElement().rightOffset)
        }
        
        changeEncryptionKeyButton.addTarget(self, action: #selector(self.didPressChangeEncryptionKey(_:)), for: .touchUpInside)
        changeEncryptionKeyButton.snp.makeConstraints { (make) -> Void in
            make.top.equalTo(encryptionKeyLabel.snp.bottom).offset(10)
            make.left.equalTo(view).offset(UIElement().leftOffset)
            make.right.equalTo(view).offset(UIElement().rightOffset)
        }
        
        copyTextButton.tag = 0
        copyTextButton.addTarget(self, action: #selector(self.didPressCopyTextButton(_:)), for: .touchUpInside)
        copyTextButton.snp.makeConstraints { (make) -> Void in
            make.height.width.equalTo(50)
            make.left.equalTo(view).offset(UIElement().leftOffset)
            make.right.equalTo(view).offset(UIElement().rightOffset)
            make.bottom.equalTo(view).offset(UIElement().bottomOffset)
        }
    }
    
    func getEncryptionKey() {
        let key = Secure().getKey()
        if key == "nil" || key == "" {
            let newKey = Secure().generateKey()
            if !newKey.hasPrefix("error:") {
                encryptionKeyLabel.text = newKey
                
            } else {
                titleLabel.text = "There was an issue generating your encryption key"
                //this should show the error
                encryptionKeyLabel.text = newKey
            }
            
        } else {
            encryptionKeyLabel.text = key
        }
    }
    
    @objc func didPressCopyTextButton(_ sender: UIButton) {
        UIPasteboard.general.string = "\(encryptionKeyLabel.text!)"
        sender.setTitle("✓ Copied", for: .normal)
    }
    
    @objc func didPressChangeEncryptionKey(_ sender: UIButton) {
        self.performSegue(withIdentifier: "showSetEncryptionKey", sender: self)
    }
    
    @objc func doneAction(_ sender: UIBarButtonItem) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let initialViewController = storyboard.instantiateViewController(withIdentifier: "main")
        self.present(initialViewController, animated: true, completion: nil)
    }
}
