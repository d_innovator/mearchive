//
//  File.swift
//  MeAtlas
//
//  Created by Dominic  Smith on 6/27/18.
//  Copyright © 2018 Dominic Smith. All rights reserved.
//

import Foundation

class Text {
    func getElementInTextview(_ text: String, element: String) -> Array<String> {
        let textArray = text.split{$0 == " "}.map(String.init)
        var elementsInString = [String]()
        for text in textArray {
            if text.hasPrefix(element) && text != element {
                elementsInString.append(text)
            }
        }
        return elementsInString
    }
}
