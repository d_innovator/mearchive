//
//  MainViewController.swift
//  MeArchive
//  Created by Dominic Smith on 12/18/17.
//  Copyright © 2018 Dominic Smith. All rights reserved.
//Shortcuts for search - Mark: UI, TableView, Button Actions, Auto-Complete, Text, Reminder, todo, media, Data, Date, friends, Mich, Audio

import UIKit
import SlackTextViewController
import SnapKit
import Parse
import UserNotifications
import DateTimePicker
import SKPhotoBrowser
import RealmSwift
import Contacts
import Kingfisher
import Firebase
import SAConfettiView
import AVFoundation

class MainViewController: SLKTextViewController, UNUserNotificationCenterDelegate, DateTimePickerDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate, UITabBarControllerDelegate, AVAudioRecorderDelegate, AVAudioPlayerDelegate {
    
    let uiElement = UIElement()
    
    let refreshControl = UIRefreshControl()
    
    var userHashtags = [String]()
    var filteredPosts = [Post]()
    var posts = [Post]()
    var editPost: Post!
    
    var currentUser: PFUser!
    var name: String?
    var newMedia: PFFile?
    
    var selectedIndex: Int!
    var isShowingPeopleCompleted = false
    
    var showComments = "showComments"
    
    var shouldHideCompletes = UserDefaults.standard.bool(forKey: "shouldHideCompletes")
    
    var confettiView: SAConfettiView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
                
        self.view.backgroundColor = uiElement.currentTheme().backgroundColor
        
        incrementBadgeOnActivityView()
        
        if let currentUser = PFUser.current() {
            self.currentUser = currentUser
            
            getLocalData()
            
            loadFriends()
            
            setUpConfetti()
            setUpTabBar()
            setUpTableView()
            setUpTextView()
            setUpAutoComplete()
            setUpWidgetUI()
            //setUpAudioUI()
            
            if let name = self.uiElement.getCurrentDefaultUserDisplayName() {
                self.name = name
                
            } else {
                self.loadCurrentUserInfo()
            }
            
        } else {
            let storyboard = UIStoryboard(name: "Login", bundle: nil)
            let controller = storyboard.instantiateViewController(withIdentifier: "welcome")
            let appDelegate = UIApplication.shared.delegate as! AppDelegate
            //show window
            appDelegate.window?.rootViewController = controller
        }
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
        if confettiView.isActive() {
            self.stopConfetti()
        }
    }
    
    func incrementBadgeOnActivityView() {
        if let tabItems = self.tabBarController?.tabBar.items as NSArray? {
            let installation = PFInstallation.current()
            // In this case we want to modify the badge number of the third tab:
            if installation?.badge != 0 {
                let tabItem = tabItems[1] as! UITabBarItem
                tabItem.badgeValue = "\(installation!.badge)"
            }
        }
    }
    
    override func keyForTextCaching() -> String? {
        return Bundle.main.bundleIdentifier
    }
    
    @objc func keyboardWillAppear() {
        self.widgetUI.isHidden = false
        self.noResultsLabel.isHidden = true
    }
    
    @objc func keyboardWillDisappear() {
        self.widgetUI.isHidden = true
        if self.filteredPosts.count == 0 && hashtagsInTextview.count == 0 {
            self.noResultsLabel.isHidden = false
        }
    }
     
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == showCompleted {
            if let completedViewController = segue.destination as? CompletedViewController {
                completedViewController.noteObjectId = self.filteredPosts[selectedIndex].objectId
                completedViewController.isShowingCompleted = self.isShowingPeopleCompleted
            }
            
        } else {
            if let commentsViewController = segue.destination as? CommentsViewController {
                let post = self.filteredPosts[selectedIndex]
                commentsViewController.note = post
                
                if let mentions = post.mentions {
                    commentsViewController.mentionedPeopleInNote = mentions
                }
            }
        }
     }
    
    func tabBarController(_ tabBarController: UITabBarController, didSelect viewController: UIViewController) {
        let tabBarIndex = tabBarController.selectedIndex
        if tabBarIndex == 0 {
            scrollUp()
        }
    }
    
    //MARK: UI
    lazy var noResultsLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont(name: "HelveticaNeue", size: 17)
        label.numberOfLines = 0
        label.textColor = uiElement.currentTheme().textColor
        return label
    }()
    
    lazy var loadingSpinner: UIActivityIndicatorView = {
        let spinner  = UIActivityIndicatorView()
        spinner.color = uiElement.currentTheme().textColor
        return spinner
    }()
    
    func setUpSpinner() {
        self.view.addSubview(loadingSpinner)
        loadingSpinner.snp.makeConstraints { (make) -> Void in
            make.height.width.equalTo(100)
            make.top.equalTo(self.view).offset(self.view.frame.height / 2 - 100)
            make.left.equalTo(self.view).offset(self.view.frame.width / 2 - 55)
        }
        loadingSpinner.startAnimating()
    }
    
    func setUpNoResultsUI() {
        self.view.addSubview(noResultsLabel)
        noResultsLabel.snp.makeConstraints { (make) -> Void in
            make.top.equalTo(self.view).offset(uiElement.uiViewTopOffset(self))
            make.left.equalTo(self.view).offset(uiElement.leftOffset)
            make.right.equalTo(self.view).offset(uiElement.rightOffset)
        }
    }
    
    lazy var line: UIView = {
        let view = UIView()
        view.layer.borderWidth = 1
        view.layer.borderColor = UIColor.lightGray.cgColor
        view.backgroundColor = uiElement.currentTheme().backgroundColor
        return view
    }()
    
    lazy var widgetUI: UIView = {
        let view = UIView()
        view.backgroundColor = uiElement.currentTheme().backgroundColor
        view.isHidden = true
        return view
    }()
    
    func setUpWidgetUI() {
        self.view.addSubview(self.widgetUI)
        self.widgetUI.addSubview(line)
        self.widgetUI.addSubview(mediaButton)
        //self.widgetUI.addSubview(audioButton)
        
        let widgetUIHeight = 50
        let widgetItemHeight = 50
        
        widgetUI.snp.makeConstraints { (make) -> Void in
            make.height.equalTo(widgetUIHeight)
            make.bottom.equalTo(self.textInputbar.snp.top)
            make.left.equalTo(self.view)
            make.right.equalTo(self.view)
        }
        
        line.snp.makeConstraints { (make) -> Void in
            make.height.equalTo(0.5)
            make.top.equalTo(self.widgetUI)
            make.left.equalTo(self.widgetUI.snp.left)
            make.right.equalTo(self.widgetUI.snp.right)
        }
        
        mediaButton.snp.makeConstraints { (make) -> Void in
            make.width.height.equalTo(widgetItemHeight)
            make.top.equalTo(self.line.snp.bottom)
            make.right.equalTo(self.widgetUI.snp.right).offset((uiElement.rightOffset) + 10)
        }
        
        /*audioButton.snp.makeConstraints { (make) -> Void in
            make.width.height.equalTo(widgetItemHeight)
            make.top.equalTo(self.mediaButton)
            make.right.equalTo(self.mediaButton.snp.left).offset(uiElement.rightOffset)
        }*/
    }
    
    var clearButton: UIBarButtonItem!
    var settingButton: UIBarButtonItem!
    
    func setUpTabBar() {
        self.title = "MeArchive"
        self.tabBarController?.delegate = self
        navigationController?.tabBarController?.tabBar.barTintColor = uiElement.currentTheme().backgroundColor
        navigationController?.tabBarController?.tabBar.tintColor = uiElement.currentTheme().textColor
        navigationController?.navigationBar.barTintColor = uiElement.currentTheme().backgroundColor
        navigationController?.navigationBar.tintColor = uiElement.currentTheme().textColor
        navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: uiElement.currentTheme().textColor]
    }
    
    //MARK: Tableview
    let reuse = "reuse"
    let urlReuse = "urlReuse"
    let spinnerReuse = "spinnerReuse"
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView == self.tableView {
            return self.filteredPosts.count
            
        } else if showFriends {
            return filteredFriends.count
            
        }  else if let hashtagSearchResult = self.autoCompleteResults {
            return hashtagSearchResult.count
        }
        
        return 0
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if tableView == self.tableView {
            var cell: MainTableViewCell!
            tableView.separatorStyle = .none 
            
            if self.filteredPosts.indices.contains(indexPath.row) {
                if self.filteredPosts[indexPath.row].media != nil {
                    cell = self.mediaCell(indexPath)
                    
                } else if self.filteredPosts[indexPath.row].audio != nil {
                    cell = self.audioCell(indexPath)
                    
                } else {
                    if let mentions = self.filteredPosts[indexPath.row].mentions {
                        if mentions.count == 0 {
                            cell = tableView.dequeueReusableCell(withIdentifier: reuse, for: indexPath) as? MainTableViewCell
                            
                        } else {
                            cell = tableView.dequeueReusableCell(withIdentifier: userReuse, for: indexPath) as? MainTableViewCell
                        }

                    } else {
                        cell = tableView.dequeueReusableCell(withIdentifier: reuse, for: indexPath) as? MainTableViewCell
                    }
                }
            }
            
            universalCells(cell, indexPath: indexPath)
            
            return cell
            
        } else if showFriends {
            return self.friendsAutoCompleteCell(indexPath)
        }
        
        return self.autoCompleteCell(indexPath)
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
         if tableView == self.autoCompletionView {
            if showFriends {
                self.showFriends = false
                self.tableView?.reloadData()
                var contact: MEAContacts!
                
                if indexPath.section == 0 {
                    contact = self.filteredFriends[indexPath.row]
                    var userExistsInTextViewArray = false
                    let contactsInTextViewIds = self.mentionsInTextView.map {$0.userId}
                    
                    for user in contactsInTextViewIds {
                        if user == contact.userId {
                            userExistsInTextViewArray = true
                            break
                        }
                    }
                    
                    if !userExistsInTextViewArray {
                        //needs to be space so mention auto-complete dissapears
                        self.acceptAutoCompletion(with: "\(self.filteredFriends[indexPath.row].username!) ")
                        self.mentionsInTextView.append(contact)
                        
                    } else {
                        self.showAutoCompletionView(false)
                        self.tableView?.reloadData()
                    }
                }
                
            } else {
                guard let searchResult = self.autoCompleteResults else {
                    return
                }
                
                var selectedHashtag = searchResult[indexPath.row]
                
                var hashtagExistsInTextviewArray = false
                
                for hashtag in self.hashtagsInTextview {
                    if hashtag == selectedHashtag {
                        hashtagExistsInTextviewArray = true
                    }
                }
                
                if !hashtagExistsInTextviewArray {
                    //autocomplete appends # for you, so have to remove
                    selectedHashtag.removeFirst()
                    selectedHashtag += " "
                    self.acceptAutoCompletion(with: selectedHashtag)
                    
                    self.hashtagsInTextview = self.text.getElementInTextview(self.textView.text, element: "#")
                    self.resetDataCounting()
                    if self.hashtagsInTextview.count == 0 {
                        self.filteredPosts = self.posts
                        self.checkStatusOfPosts(nil)
                        
                    } else {
                        self.filterLocalObjects(hashtagsInTextview)
                    }
                    
                } else {
                    self.showAutoCompletionView(false)
                    self.tableView?.reloadData()
                }                
            }
            
            self.widgetUI.isHidden = false
        }
    }
    
    override open func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        
        if indexPath.row == self.filteredPosts.count - 1 && !self.isUpdatingData  && !self.thereIsNoMoreData && didLoadLocalData && !self.showFriends {
            self.isUpdatingData = true
            if self.hashtagsInTextview.count == 0 {
                self.loadData(nil)
                
            } else {
                self.loadData(self.hashtagsInTextview)
            }
        }
    }
    
    override func tableView(_ tableView: UITableView, editActionsForRowAt: IndexPath) -> [UITableViewRowAction]? {
        
        let row = editActionsForRowAt.row
        
        if tableView == self.autoCompletionView && !self.showFriends {
            let clearAction = UITableViewRowAction(style: .normal, title: "Clear") { action, index in
                self.clearAction(self.autoCompleteResults![row], row: row)
            }
            clearAction.backgroundColor = .red
            return [clearAction]
        }
        
        return []
    }
    
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    func setUpTableView() {
        self.tableView?.register(MainTableViewCell.self, forCellReuseIdentifier: reuse)
        self.tableView?.register(MainTableViewCell.self, forCellReuseIdentifier: mediaReuse)
        self.tableView?.register(MainTableViewCell.self, forCellReuseIdentifier: friendsReuse)
        self.tableView?.register(MainTableViewCell.self, forCellReuseIdentifier: mediaUserReuse)
        self.tableView?.register(MainTableViewCell.self, forCellReuseIdentifier: userReuse)
        self.tableView?.register(MainTableViewCell.self, forCellReuseIdentifier: audioReuse)
        self.tableView?.backgroundColor = uiElement.currentTheme().backgroundColor
        self.tableView?.separatorStyle = .none
        
        refreshControl.addTarget(self, action: #selector(didPullToRefresh(_:)), for: UIControl.Event.valueChanged)
        self.tableView?.addSubview(refreshControl)
    }
    
    func universalCells(_ cell: MainTableViewCell, indexPath: IndexPath) {
        cell.backgroundColor = uiElement.currentTheme().backgroundColor
        cell.selectionStyle = .none
        let currentPost = self.filteredPosts[indexPath.row]
        
        if cell.reuseIdentifier == userReuse || cell.reuseIdentifier == mediaUserReuse {
            if let username = currentPost.username {
                cell.username.text = "@\(username)"
                
                if let name = currentPost.name {
                    cell.name.text = name
                }
                
                let personImage = uiElement.currentTheme().personImage
                
                if let userImageURL = currentPost.userImageURL {
                    cell.userImage.kf.setImage(with: URL(string: userImageURL), placeholder: personImage, options: nil, progressBlock: nil, completionHandler: nil)
                    
                } else {
                    cell.userImage.image = personImage
                }
                
            } else {
                self.loadUserInfoFromCloud(currentPost.userId, row: indexPath.row)
            }
        }
        
        let dateString = customDate.formatDate(date: currentPost.date)
        cell.date.text = "\(dateString)"
        
        if let todo = currentPost.todo {
            if todo {
                let attributeString: NSMutableAttributedString =  NSMutableAttributedString(string: currentPost.text)
                attributeString.addAttribute(.strikethroughStyle, value: 2, range: NSMakeRange(0, attributeString.length))
                
                cell.bodyLabel.attributedText = attributeString
                
            } else {
                cell.bodyLabel.text = currentPost.text
            }
            
        } else {
            cell.bodyLabel.text = currentPost.text
        }
        
        if let _ = currentPost.reminder {
            cell.bodyLabel.textColor = Color().red()
            
        } else {
            cell.bodyLabel.textColor = uiElement.currentTheme().textColor
        }
    
        
        cell.bodyLabel.hashtagColor = uiElement.currentTheme().hashTagColor
        cell.bodyLabel.URLColor = uiElement.currentTheme().hashTagColor
        cell.hashtag.textColor = uiElement.currentTheme().hashTagColor
        
        if let _ = currentPost.mentions {
            //mentions
            /*if mentions.count != 0 {
                cell.mentionsButton.setTitle(" \(mentions.count)", for: .normal)
                cell.mentionsButton.isHidden = false
                cell.spaceLabel.isHidden = false
                
            } else {
                cell.mentionsButton.isHidden = true
                cell.spaceLabel.isHidden = true
            }
            
            cell.mentionsButton.addTarget(self, action: #selector(self.didPressMentionsButton(_:)), for: .touchUpInside)
            cell.mentionsButton.tag = indexPath.row*/
            
            //completed
            if let completed = currentPost.completed {
                var completedTitle = ""
                completedTitle = " \(completed.count)"
                cell.completedButton.setTitle(completedTitle, for: .normal)
                cell.completedButton.addTarget(self, action: #selector(self.didPressCompletedButton(_:)), for: .touchUpInside)
            }
            cell.completedButton.tag = indexPath.row
            
            //comments
            if let commentCount = currentPost.commentObjectId?.count {
                var commentTitle = ""
                if commentCount != 0 {
                    commentTitle = " \(commentCount)"
                }
                
                cell.commentButton.setTitle(commentTitle, for: .normal)
                cell.commentButton.addTarget(self, action: #selector(self.didPressCommentButton(_:)), for: .touchUpInside)
                
            } else {
                self.loadComments(indexPath.row)
            }
            
            cell.commentButton.tag = indexPath.row
        }
        
        cell.menuButton.addTarget(self, action: #selector(self.didPressMenuButton(_:)), for: .touchUpInside)
        cell.menuButton.tag = indexPath.row
    }
    
    //MARK: button actions
    @objc func didPullToRefresh(_ sender: AnyObject) {
        self.textView.text = ""
        self.textView.resignFirstResponder()
        self.resetEverything()
    }
    
    @objc func didPressCommentsButton(_ sender: UIButton) {
        self.selectedIndex = sender.tag
        self.performSegue(withIdentifier: showComments, sender: self)
    }
    
    var showCompleted = "showCompleted"
    @objc func didPressCompletedButton(_ sender: UIButton) {
        isShowingPeopleCompleted = true
        self.selectedIndex = sender.tag 
        self.performSegue(withIdentifier: showCompleted, sender: self)
    }
    
    /*@objc func didPressMentionsButton(_ sender: UIButton) {
        isShowingPeopleCompleted = false
        self.selectedIndex = sender.tag
        self.performSegue(withIdentifier: showCompleted, sender: self)
    }*/
    
    @objc func didPressMenuButton(_ sender: UIButton) {
        var reminderTitle = "Set Reminder"
        let row = sender.tag
        
        if let reminder = self.filteredPosts[row].reminder {
            if let trigger = reminder.trigger as? UNCalendarNotificationTrigger,
                let triggerDate = trigger.nextTriggerDate(){
                reminderTitle = "⏰ \(self.customDate.formatDate(date: triggerDate))"
            }
        }
        
        var completeTitle = " ✔︎ Mark Complete"
        if let todo = self.filteredPosts[row].todo {
            if todo {
                completeTitle = "✖︎ Mark Incomplete"
            }
        }
        
        let dateString = customDate.formatDate(date:  self.filteredPosts[row].date)
        let menuAlert = UIAlertController(title: dateString, message: nil , preferredStyle: .actionSheet)
        menuAlert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
        menuAlert.addAction(UIAlertAction(title: reminderTitle, style: .default, handler: { action in
            self.didPressReminder(row)
        }))
        
        menuAlert.addAction(UIAlertAction(title: completeTitle, style: .default, handler: { action in
            self.didPressTODO(row)
        }))
        
        menuAlert.addAction(UIAlertAction(title: "More", style: .default, handler: { action in
            self.didPressMore(row)
        }))
        
        self.present(menuAlert, animated: true, completion: nil)
    }
    
    func didPressMore(_ row: Int) {
        let currentPost = self.filteredPosts[row]

        if currentPost.userId == self.currentUser.objectId! {
            self.noteOwnerMenuActions(currentPost, row: row)
            
        } else {
            self.mentionedUserMenuActions(currentPost, row: row)
        }
    }
    
    func mentionedUserMenuActions(_ currentPost: Post, row: Int) {
        let menuAlert = UIAlertController(title: nil , message: nil , preferredStyle: .actionSheet)
        menuAlert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
        menuAlert.addAction(UIAlertAction(title: "Remove", style: .default, handler: { action in
            let removeAlert = UIAlertController(title: "Remove Post", message: "Doing so will remove this post from your archive.", preferredStyle: .alert)
            removeAlert.addAction(UIAlertAction(title: "NO", style: .cancel, handler: nil))
            removeAlert.addAction(UIAlertAction(title: "YES", style: .default, handler: { action in
                if let mentions = currentPost.mentions {
                    for i in 0..<mentions.count {
                        if mentions[i] == self.currentUser.objectId! {
                            currentPost.mentions?.remove(at: i)
                        }
                    }
                    
                    self.updatePost(currentPost)
                    
                    for i in 0..<self.posts.count {
                        if self.posts[i].objectId == self.filteredPosts[row].objectId {
                            self.posts.remove(at: i)
                            break
                        }
                    }
                    self.filteredPosts.remove(at: row)
                    self.reminder.reminderCenter.removePendingNotificationRequests(withIdentifiers: [currentPost.objectId!])
                    self.tableView?.reloadData()
                }
            }))
            
            self.present(removeAlert, animated: true, completion: nil)
        }))
        
        self.present(menuAlert, animated: true, completion: nil)
    }
    
    func noteOwnerMenuActions(_ currentPost: Post, row: Int) {
        let menuAlert = UIAlertController(title: nil , message: nil , preferredStyle: .actionSheet)
        menuAlert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
        menuAlert.addAction(UIAlertAction(title: "Delete", style: .default, handler: { action in
            let deleteAlert = UIAlertController(title: "Delete Post?", message: nil, preferredStyle: .alert)
            deleteAlert.addAction(UIAlertAction(title: "NO", style: .cancel, handler: nil))
            deleteAlert.addAction(UIAlertAction(title: "YES", style: .default, handler: { action in
                self.deletePost(currentPost.objectId!, row: row)
                Local().deleteObject(currentPost.objectId!)
                for i in 0..<self.posts.count {
                    if self.posts[i].objectId == self.filteredPosts[row].objectId {
                        self.posts.remove(at: i)
                        break
                    }
                }
                self.filteredPosts.remove(at: row)
                self.reminder.reminderCenter.removePendingNotificationRequests(withIdentifiers: [currentPost.objectId!])
                self.tableView?.reloadData()
                self.checkStatusOfPosts(self.hashtagsInTextview)
            }))
            self.present(deleteAlert, animated: true, completion: nil)
        }))
        
        menuAlert.addAction(UIAlertAction(title: "Edit", style: .default, handler: { action in
            self.setUpEditText(row)
        }))
        
        self.present(menuAlert, animated: true, completion: nil)
    }
        
    override func didPressRightButton(_ sender: Any?) {
        let newObjectId = generateObjectId()
        var media: UIImage?
        var audioFile: Data?
        
        if let audio = self.audioFile {
            audioFile = audio
            self.audioFile = nil
        }
        
        if didChooseMedia {
            media = self.mediaButton.imageView?.image
            self.mediaButton.setImage(uiElement.currentTheme().pictureImage, for: .normal)
            self.didChooseMedia = false
        }
        
        let mentionUserIds = self.mentionsInTextView.map {$0.userId} + self.mentions.map {$0.userId}
        self.mentionsInTextView.removeAll()
        self.mentions.removeAll()
        
        let newPost = Post(objectId: newObjectId, text: self.textView.text, date: Date(), reminder: nil, todo: false, media: media, userId: self.currentUser.objectId!, username: nil, name: nil, userImageURL: nil, completed: [], mentions: mentionUserIds as? Array<String>, commentText: nil, commentUserId: nil, commentObjectId: nil, commentDate: nil, audio: audioFile)
        
        if mentionUserIds.count == 0 {
            newPost.mentions = nil
        }
        
        self.filteredPosts.insert(newPost, at: 0)
        self.posts.insert(newPost, at: 0)
        self.postLocalData(newPost)
        
        self.appendhashtagsToTextview()
        
        self.noResultsLabel.removeFromSuperview()
        self.tableView?.isHidden = false
        self.tableView?.reloadData()
        
        Analytics.logEvent("new_post", parameters: [
            "name": "New Post",
            "full_text": "New Post"
            ])
    }
    
    @objc func didPressCommentButton(_ sender: UIButton) {
        self.selectedIndex = sender.tag
        self.performSegue(withIdentifier: showComments, sender: self)
    }
    
    //MARK: auto-complete
    var autoCompleteResults: [String]?
    var isLink = [Bool]()
    var hashtagsInTextview = [String]()
    
    let autoCompleteReuse = "autoReuse"
    
    func setUpAutoComplete() {
        self.autoCompletionView.register(MainTableViewCell.classForCoder(), forCellReuseIdentifier: autoCompleteReuse)
        self.autoCompletionView.register(MainTableViewCell.classForCoder(), forCellReuseIdentifier: friendsReuse)
    }
    
    func autoCompleteCell(_ indexPath: IndexPath) -> MainTableViewCell {
        var cell: MainTableViewCell!
        
        cell = self.autoCompletionView.dequeueReusableCell(withIdentifier: autoCompleteReuse) as? MainTableViewCell
        guard let searchResult = self.autoCompleteResults else {
            return cell
        }
        cell.hashtag.text = searchResult[indexPath.row]
        
        cell.selectionStyle = .default
        cell.backgroundColor = uiElement.currentTheme().backgroundColor
        
        //different color shows behind cell .. getting superview color to match cell
        cell.superview?.backgroundColor = uiElement.currentTheme().backgroundColor
        cell.hashtag.textColor = uiElement.currentTheme().hashTagColor
        
        return cell
    }
    
    override func shouldProcessTextForAutoCompletion() -> Bool {
        return true
    }
    
    override func didChangeAutoCompletionPrefix(_ prefix: String, andWord word: String) {
        //predict # based off of what user is typing
       
        if prefix == "#" {
            let textArray = textView.text.split{$0 == " "}.map(String.init)
            let wordPredicate = NSPredicate(format: "self BEGINSWITH[c] %@", textArray[textArray.count - 1])
            var filteredHashtags = [String]()
            if word.count == 0 {
                let defaultHashtags = ["#grocery", "#idea", "#place", "#todo"]
                if self.userHashtags.count == 0 {
                    for defaultHashtag in defaultHashtags {
                        filteredHashtags.append(defaultHashtag)
                    }
                    
                } else {
                    filteredHashtags = self.userHashtags
                }
                
            } else if self.autoCompleteResults != nil {
                filteredHashtags = self.autoCompleteResults!.filter { wordPredicate.evaluate(with: $0) }
            }
            
            let sortedFilteredPost = filteredHashtags.sorted()
            self.autoCompleteResults = sortedFilteredPost
            
            self.showFriends = false 
            self.showAutoCompletionView(true)
            
        } else if prefix == "@" {
            let wordPredicate = NSPredicate(format: "self BEGINSWITH[c] %@", word)
            
            if word.count == 0 {
                self.filteredFriends = self.friends
                
            } else {
                //filter users on MeArchive that are in current user's phone
                var filteredFriends = [MEAContacts]()
                filteredFriends = self.friends.filter {
                    wordPredicate.evaluate(with: $0.username) ||
                    wordPredicate.evaluate(with: $0.displayName)
                }
                
                filteredFriends.sort(by: {$0.displayName! < $1.displayName!})
                self.filteredFriends = filteredFriends
            }
            self.showFriends = true
            self.showAutoCompletionView(true)
        }
        
        self.widgetUI.isHidden = true
    }
    
    override func heightForAutoCompletionView() -> CGFloat {
        guard let searchResult = self.autoCompleteResults else {
            return 0
        }
        
        return 50 * CGFloat(searchResult.count)
    }
    
    func clearAction(_ hashtagToClear: String, row: Int) {
        let deleteAlert = UIAlertController(title: "Clear \(hashtagToClear)?", message: "This will delete all archives associated with \(hashtagToClear)", preferredStyle: .alert)
        deleteAlert.addAction(UIAlertAction(title: "NO", style: .cancel, handler: nil))
        deleteAlert.addAction(UIAlertAction(title: "YES", style: .default, handler: { action in
            self.clearCurrentHashtag(hashtagToClear)
            self.autoCompleteResults?.remove(at: row)
        }))
        self.present(deleteAlert, animated: true, completion: nil)
    }
    
    //MARK: text
    let text = Text()
    
    override func textViewDidChange(_ textView: UITextView) {
        //only want to check for +1 or -1 hashtags if user pressed space or selected hastag from auto-complete
        if textView.text.hasSuffix(" ") && !self.textView.text.isEmpty {
            self.resetDataCounting()
            self.isUpdatingData = false
            
            //means there is +1 or -1 hashtag in textview if != & need to update archive(UI)
            let hashtagsInTextview = self.text.getElementInTextview(self.textView.text, element: "#")
            if self.hashtagsInTextview != hashtagsInTextview {
                self.filterLocalObjects(hashtagsInTextview)
            }
            self.hashtagsInTextview = hashtagsInTextview
            
            let mentionsInTextView = self.text.getElementInTextview(self.textView.text, element: "@")
            var newMentions = [MEAContacts]()
            for i in 0..<self.friends.count {
                for mention in mentionsInTextView {
                    if mention == "@\(self.friends[i].username!)" {
                        newMentions.append(self.friends[i])
                        break
                    }
                }
            }
            
            self.mentionsInTextView = newMentions
            self.widgetUI.isHidden = false
            self.tableView?.reloadData()
            
        } else if self.textView.text.isEmpty {
            self.resetEverything()
        }
    }
    
    func resetEverything() {
        self.resetDataCounting()
        self.showFriends = false
        self.isUpdatingData = false
        self.hashtagsInTextview.removeAll()
        self.mentionsInTextView.removeAll()
        self.dataCount = 0
        filterPostsThatWereNotCompleted()
    }
    
    func filterPostsThatWereNotCompleted() {
        if self.shouldHideCompletes {
            var postsThatHaveNotBeenCompleted = [Post]()
            for post in self.posts {
                if let todo = post.todo {
                    if !todo {
                        postsThatHaveNotBeenCompleted.append(post)
                    }
                }
            }
            
            self.filteredPosts = postsThatHaveNotBeenCompleted
            
        } else {
            self.filteredPosts = self.posts
        }
        
        self.tableView?.reloadData()
        self.refreshControl.endRefreshing()
        self.checkStatusOfPosts(nil)
    }
    
    override func textViewDidBeginEditing(_ textView: UITextView) {
        scrollUp()
    }
    
    //this func won't work if sender isn't declared as Any
    override func didCommitTextEditing(_ sender: Any) {

        self.editPost.text = self.textView.text
        
        //update UI with new info
        self.textView.text = ""
        
        //update tableview with updated data.. object place could have changed because of #s so have to make sure it matches before updating filteredposts
        var didFindMatchingPost = false
        if self.filteredPosts.count != 0 {
            for i in 0...self.filteredPosts.count - 1 {
                if self.filteredPosts[i].objectId == self.editPost.objectId {
                    self.filteredPosts[i] = self.editPost
                    didFindMatchingPost = true
                }
            }
            
        } else {
            self.filteredPosts.insert(self.editPost, at: 0)
        }
        
        if !didFindMatchingPost {
            self.filteredPosts.insert(self.editPost, at: 0)
        }
        
        self.tableView?.reloadData()
        
        self.updatePost(self.editPost)
        Local().update(self.editPost.objectId!, newObjectId: nil, post: self.editPost)
        
        super.didCommitTextEditing(sender)
    }
    
    //this func won't work if sender isn't declared as Any
    override func didCancelTextEditing(_ sender: Any) {
        self.textView.text = ""
        super.didCancelTextEditing(sender)
    }
    
    func setUpTextView() {
        self.tableView?.keyboardDismissMode = .onDrag
        
        self.isInverted = false
        self.textView.placeholder = "Add something you want to remember"
        self.rightButton.setTitle("Post", for: .normal)
        self.rightButton.setTitleColor(uiElement.currentTheme().textColor, for: .normal)
        
        self.shouldScrollToBottomAfterKeyboardShows = false
        self.registerPrefixes(forAutoCompletion: ["#", "@"])
        
        self.textInputbar.backgroundColor = uiElement.currentTheme().backgroundColor
        self.textInputbar.editorTitle.textColor = uiElement.currentTheme().textColor
        self.textInputbar.editorTitle.text = "Edit Message"
        self.textInputbar.editorLeftButton.tintColor = uiElement.currentTheme().textColor
        self.textInputbar.editorRightButton.tintColor = uiElement.currentTheme().textColor
        
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillDisappear), name: UIResponder.keyboardWillHideNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillAppear), name: UIResponder.keyboardWillShowNotification, object: nil)
    }
    
    func setUpEditText(_ row: Int) {
        self.textView.text = self.filteredPosts[row].text
        editPost = self.filteredPosts[row]
        
        self.textInputbar.beginTextEditing()
        self.textView.becomeFirstResponder()
    }
    
    func checkStatusOfPosts(_ hashtags: Array<String>?) {
        self.loadingSpinner.removeFromSuperview()
        self.tableView?.isHidden = true
        self.refreshControl.endRefreshing()
        
        if self.dataCount == self.filteredPosts.count {
            self.thereIsNoMoreData = true
            
        } else {
            self.dataCount = self.filteredPosts.count
            self.isUpdatingData = false
        }
        
        if self.filteredPosts.count != 0 {
            self.noResultsLabel.removeFromSuperview()
            self.tableView?.isHidden = false
            self.tableView?.reloadData()
            
        } else if hashtags == nil || hashtags!.isEmpty {
            self.noResultsLabel.text =
            "Welcome to MeArchive! \n\n - Use #hashtags to organize your posts. \n\n - Use @mentions to include friends in your posts. \n\n Tap the text input box below to get started."
            self.setUpNoResultsUI()
            
        } else if hashtags![0] != "#" {
            self.noResultsLabel.text = "No posts were found that contain: "
            for i in 0...hashtags!.count - 1 {
                if hashtags!.count - 1 != i {
                    self.noResultsLabel.text = "\(String(describing: self.noResultsLabel.text!)) \(hashtags![i]), "
                    
                } else if hashtags!.count > 1 {
                    self.noResultsLabel.text = "\(String(describing: self.noResultsLabel.text!))& \(hashtags![i])"
                    
                } else {
                    self.noResultsLabel.text = "\(String(describing: self.noResultsLabel.text!))\(hashtags![i])"
                }
            }
            var lastWordOfNoResultsLabel = "this hashtag"
            if hashtags!.count > 1 {
                lastWordOfNoResultsLabel = "these hashtags"
            }
            self.noResultsLabel.text = "\(String(describing: self.noResultsLabel.text!)). Pressing post will save \(lastWordOfNoResultsLabel)."
            self.noResultsLabel.isHidden = false
            self.setUpNoResultsUI()
        }
    }
    
    func appendhashtagsToTextview() {
        self.textView.text = ""
        let oldUserHashtagCount = self.userHashtags.count
        
        for hashtag in self.hashtagsInTextview {
            self.textView.text = "\(self.textView.text!)\(hashtag) "
            if !self.userHashtags.contains(hashtag) {
                self.userHashtags.append(hashtag)
            }
        }
        
        if oldUserHashtagCount != self.userHashtags.count {
            self.userHashtags = self.userHashtags.sorted()
        }
    }
    
    func generateObjectId() -> String {
        //see https://stackoverflow.com/questions/36508470/how-do-i-generate-a-mongo-db-objectid-in-swift
        let time = String(Int(NSDate().timeIntervalSince1970), radix: 16, uppercase: false)
        let machine = String(arc4random_uniform(900000) + 100000)
        let pid = String(arc4random_uniform(9000) + 1000)
        let counter = String(arc4random_uniform(900000) + 100000)
        return time + machine + pid + counter
    }
    
    //MARK: Reminder
    let reminder = Reminder()

    func didPressReminder(_ row: Int) {
        let options: UNAuthorizationOptions = [.alert, .sound]
        self.reminder.reminderCenter.requestAuthorization(options: options) {
            (granted, error) in
            if granted {
                let oneMinute = 60.0
                let oneHour = 60.0
                let oneDay = 24.0
                
                let objectId = self.filteredPosts[row].objectId
                let text = self.filteredPosts[row].text
                
                let alertController = UIAlertController (title: "Set Reminder", message: nil, preferredStyle: .actionSheet)
                
                let oneHourAction = UIAlertAction(title: "1 Hour", style: .default) { (_) -> Void in
                    let reminderDate = Date().addingTimeInterval(oneMinute * oneHour)
                    self.setNewReminder(row: row, date: reminderDate, objectId: objectId!, text: text)
                    
                    self.logReminderEvent()
                }
                alertController.addAction(oneHourAction)
                
                let twentyFourHourAction = UIAlertAction(title: "24 Hours", style: .default) { (_) -> Void in
                    let reminderDate = Date().addingTimeInterval(oneMinute * oneHour * oneDay)
                    self.setNewReminder(row: row, date: reminderDate, objectId: objectId!, text: text)
                    
                    self.logReminderEvent()
                }
                alertController.addAction(twentyFourHourAction)
                
                let customDateTimeAction = UIAlertAction(title: "Custom Date & Time", style: .default) { (_) -> Void in
                    if let reminder = self.filteredPosts[row].reminder {
                        if let trigger = reminder.trigger as? UNCalendarNotificationTrigger,
                            let triggerDate = trigger.nextTriggerDate(){
                            self.showDateTimePicker(triggerDate, row: row)
                        }
                        
                    } else if self.filteredPosts[row].objectId != nil {
                        self.showDateTimePicker(Date(), row: row)
                    }
                }
                alertController.addAction(customDateTimeAction)
                
                if self.filteredPosts[row].reminder != nil {
                    let removeReminderAction = UIAlertAction(title: "Remove Reminder", style: .default) { (_) -> Void in
                        self.reminder.reminderCenter.removePendingNotificationRequests(withIdentifiers: [objectId!])
                        self.filteredPosts[row].reminder = nil
                         let indexPath = IndexPath(item: row, section: 0)
                        self.tableView?.reloadRows(at: [indexPath], with: .none)
                        
                    }
                    alertController.addAction(removeReminderAction)
                }
                
                let cancelAction = UIAlertAction(title: "Cancel", style: .cancel) { (_) -> Void in
                }
                alertController.addAction(cancelAction)
                
                self.present(alertController, animated: true, completion: nil)
                
            } else {
                self.uiElement.permissionDenied("Oops", message: "Notifications are off.", target: self)
            }
        }
    }
    
    func setNewReminder(row: Int, date: Date, objectId: String, text: String) {
        self.reminder.reminderCenter.removePendingNotificationRequests(withIdentifiers: [objectId])
        if let newReminder = self.reminder.set(objectId, text: text, date: date, target: self) {
            self.filteredPosts[row].reminder = newReminder
            DispatchQueue.main.async {
                //self.tableView?.reloadData()
                let indexPath = IndexPath(item: row, section: 0)
                self.tableView?.reloadRows(at: [indexPath], with: .none)
            }
            
        } else {
            self.uiElement.showAlert("Oops", message: "There was an issue creating your reminder.", target: self)
        }
    }
    
    //MARK: todo
    func didPressTODO(_ row: Int) {
        func postIsMarkedAsComplete() {
            self.filteredPosts[row].todo = true
            self.startConfetti()
            self.logTodoEvent()
        }
        
        if let todo = self.filteredPosts[row].todo {
            if todo {
                self.filteredPosts[row].todo = false
                
            } else {
                postIsMarkedAsComplete()
            }
            
        } else {
            postIsMarkedAsComplete()
        }
        
        if let complete = self.filteredPosts[row].completed {
            if self.filteredPosts[row].todo! {
                self.filteredPosts[row].completed?.append(self.currentUser.objectId!)
                
            } else {
                for i in 0..<complete.count {
                    if complete[i] == self.currentUser.objectId! {
                        self.filteredPosts[row].completed?.remove(at: i)
                    }
                }
            }
            
        } else {
            self.filteredPosts[row].completed? = [self.currentUser.objectId!]
        }
        
        self.updatePost(self.filteredPosts[row])
        Local().update(self.filteredPosts[row].objectId!, newObjectId: nil, post: self.filteredPosts[row])
        
        let indexPath = IndexPath(item: row, section: 0)
        self.tableView?.reloadRows(at: [indexPath], with: .none)
        
    }
    
    //MARK: media
    let mediaReuse = "mediaReuse"
    let mediaUserReuse = "mediaUserReuse"
    
    var didChooseMedia = false
    
    lazy var mediaButton: UIButton = {
        let button = UIButton()
        button.setImage(uiElement.currentTheme().cameraImage, for: .normal)
        button.imageView?.contentMode = .scaleAspectFill
        button.addTarget(self, action: #selector(self.didPressMediaButton(_:)), for: .touchUpInside)
        return button
    }()
    
    func mediaCell(_ indexPath: IndexPath) -> MainTableViewCell {
        var cell: MainTableViewCell!
        
        if let mentions = self.filteredPosts[indexPath.row].mentions {
            if mentions.count == 0 {
                cell = tableView?.dequeueReusableCell(withIdentifier: mediaReuse, for: indexPath) as? MainTableViewCell
                
            } else {
                cell = tableView?.dequeueReusableCell(withIdentifier: mediaUserReuse, for: indexPath) as? MainTableViewCell
            }
            
        } else {
            cell = tableView?.dequeueReusableCell(withIdentifier: mediaReuse, for: indexPath) as? MainTableViewCell
        }
        
        if let media = self.filteredPosts[indexPath.row].media {
            cell.mediaButton.setImage(media, for: .normal)
            cell.mediaButton.addTarget(self, action: #selector(self.expandMedia(_:)), for: .touchUpInside)
            cell.mediaButton.isEnabled = true
            
        } else {
            cell.mediaButton.removeTarget(self, action: #selector(self.expandMedia(_:)), for: .touchUpInside)
            cell.mediaButton.setImage(#imageLiteral(resourceName: "Image"), for: .normal)
            cell.mediaButton.isEnabled = false
        }

        return cell
    }
    
    @objc func didPressMediaButton(_ sender: UIButton){
        let imagePicker = UIImagePickerController()
        imagePicker.delegate = self
        imagePicker.allowsEditing = false
        
        let alertController = UIAlertController (title: nil, message: nil, preferredStyle: .actionSheet)
        
        let cameraAction = UIAlertAction(title: "Camera", style: .default) { (_) -> Void in
            imagePicker.sourceType = .camera
            self.present(imagePicker, animated: true, completion: nil)
        }
        alertController.addAction(cameraAction)
        
        let photolibraryAction = UIAlertAction(title: "Photo Library", style: .default) { (_) -> Void in
            imagePicker.sourceType = .photoLibrary
            self.present(imagePicker, animated: true, completion: nil)
        }
        alertController.addAction(photolibraryAction)
        
        if self.didChooseMedia {
            let cancelAction = UIAlertAction(title: "Remove Media", style: .default) { (_) -> Void in
                self.mediaButton.setImage(self.uiElement.currentTheme().pictureImage, for: .normal)
                self.didChooseMedia = false
            }
            alertController.addAction(cancelAction)
        }
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel) { (_) -> Void in
        }
        alertController.addAction(cancelAction)
        
        present(alertController, animated: true, completion: nil)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        // Local variable inserted by Swift 4.2 migrator.
        let info = convertFromUIImagePickerControllerInfoKeyDictionary(info)

        let image = info[convertFromUIImagePickerControllerInfoKey(UIImagePickerController.InfoKey.originalImage)] as! UIImage
        
        self.mediaButton.setImage(image, for: .normal)
        let proPic = image.jpegData(compressionQuality: 0.5)
        newMedia = PFFile(name: "media.jpeg", data: proPic!)
        newMedia?.saveInBackground()
        self.didChooseMedia = true
        
        dismiss(animated: true, completion: {
        self.textView.becomeFirstResponder()
        })
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        dismiss(animated: true, completion: nil)
    }
    
    @objc func expandMedia(_ sender: UIButton) {
        let photo = SKPhoto.photoWithImage((sender.imageView?.image)!)
        let browser = SKPhotoBrowser(photos: [photo])
        browser.initializePageIndex(0)
        present(browser, animated: true, completion: {})
    }
    
    //MARK: Audio
    let audioReuse = "audioReuse"
    let deniedMicrophonePermissionMessage = "You denied permission to your microphone. Access to your microphone is required to record audio"
    var counter = 0.0
    var timer = Timer()
    var isPlaying = false
    
    var soundRecorder: AVAudioRecorder!
    var soundPlayer: AVAudioPlayer!
    
    let fileName = "audio.caf"
    
    var audioFile: Data?
    var parseAudioFile: PFFile?
    
    func audioCell(_ indexPath: IndexPath) -> MainTableViewCell {
        var cell: MainTableViewCell!
        
        cell = tableView?.dequeueReusableCell(withIdentifier: audioReuse, for: indexPath) as? MainTableViewCell
        
        cell.playBackButton.addTarget(self, action: #selector(self.didPressPlayBackAudio(_:)), for: .touchUpInside)
        cell.playBackButton.tag = indexPath.row
        
        return cell
    }
    
    //for tableview
    @objc func didPressPlayBackAudio(_ sender: UIButton) {
        if let audioData = self.filteredPosts[sender.tag].audio {
            do {
                soundPlayer = try AVAudioPlayer(data: audioData)
                self.setUpAudioUI()
                audioUI.isHidden = false
                recordButton.isHidden = true
                self.playAudio(soundPlayer)
                
            } catch let error {
                print(error)
                soundPlayer = nil
            }
        }
    }
    
    lazy var audioButton: UIButton = {
        let button = UIButton()
        button.setImage(uiElement.currentTheme().microphoneImage, for: .normal)
        button.imageView?.contentMode = .scaleAspectFill
        button.addTarget(self, action: #selector(self.didPressAudioButton(_:)), for: .touchUpInside)
        return button
    }()
    
    lazy var audioLine: UIView = {
        let view = UIView()
        view.layer.borderWidth = 1
        view.layer.borderColor = UIColor.lightGray.cgColor
        view.backgroundColor = uiElement.currentTheme().backgroundColor
        return view
    }()
    
    lazy var audioUI: UIView = {
        let view = UIView()
        view.backgroundColor = uiElement.currentTheme().backgroundColor
        view.isHidden = true
        return view
    }()
    
    lazy var audioTime: UILabel = {
        let label = UILabel()
        label.font = UIFont(name: uiElement.mainFont, size: 30)
        label.text = "0.0"
        label.numberOfLines = 0
        label.textColor = uiElement.currentTheme().textColor
        label.textAlignment = .center
        return label
    }()
    
    @objc func UpdateTimer() {
        counter = counter + 0.1
        audioTime.text = String(format: "%.1f", counter)
    }
    
    lazy var recordButton: UIButton = {
        let button = UIButton()
        button.setImage(#imageLiteral(resourceName: "record"), for: .normal)
        button.imageView?.contentMode = .scaleAspectFill
        button.addTarget(self, action: #selector(self.didPressRecordAudio(_:)), for: .touchUpInside)
        button.tag = 0
        return button
    }()
    
    lazy var playButton: UIButton = {
        let button = UIButton()
        button.setImage(uiElement.currentTheme().playImage, for: .normal)
        button.imageView?.contentMode = .scaleAspectFill
        button.addTarget(self, action: #selector(self.didPressPlayAudio(_:)), for: .touchUpInside)
        button.tag = 0
        return button
    }()
    
    lazy var doneWithAudioButton: UIButton = {
        let button = UIButton()
        button.setTitle("Done", for: .normal)
        button.titleLabel?.font = UIFont(name: uiElement.mainFont, size: 20)
        button.setTitleColor(uiElement.currentTheme().textColor, for: .normal)
        button.addTarget(self, action: #selector(self.didPressDoneWithAudioButton(_:)), for: .touchUpInside)
        return button
    }()
    
    func setUpAudioUI() {
        self.view.addSubview(self.audioUI)
        self.audioUI.addSubview(self.audioTime)
        self.audioUI.addSubview(self.audioLine)
        self.audioUI.addSubview(self.recordButton)
        self.audioUI.addSubview(self.playButton)
        self.audioUI.addSubview(self.doneWithAudioButton)
        
        let widgetItemHeight = 65
        
        audioUI.snp.makeConstraints { (make) -> Void in
            make.height.equalTo(125)
            make.bottom.equalTo(self.textInputbar.snp.top)
            make.left.equalTo(self.view)
            make.right.equalTo(self.view)
        }
        
        audioLine.snp.makeConstraints { (make) -> Void in
            make.height.equalTo(0.5)
            make.top.equalTo(self.audioUI)
            make.left.equalTo(self.audioUI.snp.left)
            make.right.equalTo(self.audioUI.snp.right)
        }
        
        audioTime.snp.makeConstraints { (make) -> Void in
            make.top.equalTo(self.audioLine.snp.bottom).offset(uiElement.topOffset)
            make.right.equalTo(self.audioUI).offset(uiElement.rightOffset)
            make.left.equalTo(self.audioUI).offset(uiElement.leftOffset)
        }
        
        doneWithAudioButton.snp.makeConstraints { (make) -> Void in
            make.height.width.equalTo(widgetItemHeight)
            make.top.equalTo(self.audioTime.snp.bottom).offset(uiElement.topOffset)
            make.right.equalTo(self.audioUI.snp.right).offset((uiElement.rightOffset) - 10)
        }
        
        recordButton.snp.makeConstraints { (make) -> Void in
            make.height.width.equalTo(widgetItemHeight)
            make.top.equalTo(doneWithAudioButton)
            make.right.equalTo(self.doneWithAudioButton.snp.left).offset(uiElement.rightOffset - 75)
        }
        
        playButton.snp.makeConstraints { (make) -> Void in
            make.height.width.equalTo(widgetItemHeight)
            make.top.equalTo(doneWithAudioButton)
            make.left.equalTo(self.audioUI.snp.left).offset(uiElement.leftOffset)
        }
    }
    
    @objc func didPressAudioButton(_ sender: UIButton) {
        switch AVAudioSession.sharedInstance().recordPermission {
        case AVAudioSession.RecordPermission.granted:
            DispatchQueue.main.async {
                self.setupRecorder()
                self.widgetUI.isHidden = true
                self.audioUI.isHidden = false
            }
            
        case AVAudioSession.RecordPermission.denied:
            uiElement.permissionDenied("Oops", message: deniedMicrophonePermissionMessage, target: self)
            
        case AVAudioSession.RecordPermission.undetermined:
            AVAudioSession.sharedInstance().requestRecordPermission({ (granted) in
                if granted {
                    DispatchQueue.main.async {
                        self.setupRecorder()
                        self.widgetUI.isHidden = true
                        self.audioUI.isHidden = false
                    }
                    
                } else {
                    self.uiElement.permissionDenied("Oops", message: self.deniedMicrophonePermissionMessage, target: self)
                }
            })
        }
    }
    
    @objc func didPressRecordAudio(_ sender: UIButton) {
        if sender.tag == 0 {
            sender.tag = 1
            soundRecorder.record()
            timer = Timer.scheduledTimer(timeInterval: 0.1, target: self, selector: #selector(UpdateTimer), userInfo: nil, repeats: true)
            sender.setImage(#imageLiteral(resourceName: "stop"), for: .normal)
            playButton.isEnabled = false
            doneWithAudioButton.isEnabled = false
            
        } else {
            sender.tag = 0
            soundRecorder.stop()
            sender.setImage(#imageLiteral(resourceName: "record"), for: .normal)
            playButton.isEnabled = true
            doneWithAudioButton.isEnabled = true
            timer.invalidate()
        }
    }
    
    @objc func didPressPlayAudio(_ sender: UIButton) {
        if sender.tag == 0 {
            sender.tag = 1
            recordButton.isEnabled = false
            sender.setImage(uiElement.currentTheme().pauseImage, for: .normal)
            //preparePlayerAndPlay()
            
            do {
                soundPlayer = try AVAudioPlayer(contentsOf: getFileURL())
                self.playAudio(soundPlayer)
                
            } catch let error {
                print(error ?? "nil")
                soundPlayer = nil
            }
            
        } else {
            sender.tag = 0
            recordButton.isEnabled = true
            sender.setImage(uiElement.currentTheme().playImage, for: .normal)
        }
    }
    
    @objc func didPressDoneWithAudioButton(_ sender: UIButton) {
        do {
            let audioData = try Data(contentsOf: getFileURL())
            audioFile = audioData
            parseAudioFile = PFFile(name: "audio.caf", data: audioData)
            parseAudioFile?.saveInBackground()
            
        } catch {
            self.uiElement.showAlert("Oops", message: "There was an issue processing your audio recording.", target: self)
        }
        
        self.audioUI.isHidden = true
        self.widgetUI.isHidden = false
        self.textView.becomeFirstResponder()
    }
    
    func setupRecorder() {
        //set the settings for recorder
        let recordSettings = [AVSampleRateKey : NSNumber(value: Float(44100.0)),
                              AVFormatIDKey : NSNumber(value: Int32(kAudioFormatAppleLossless)),
                              AVNumberOfChannelsKey : NSNumber(value: 2),
                              AVEncoderAudioQualityKey : NSNumber(value: Int32(AVAudioQuality.max.rawValue))];
        
        var error: Error?
        
        do {
            soundRecorder =  try AVAudioRecorder(url: getFileURL(), settings: recordSettings)
            
        } catch let error1 {
            error = error1
            soundRecorder = nil
        }
        
        if let err = error {
            print("AVAudioRecorder error: \(err.localizedDescription)")
            
        } else {
            soundRecorder.delegate = self
            soundRecorder.prepareToRecord()
        }
    }
    
    func playAudio(_ soundPlayer: AVAudioPlayer) {
        soundPlayer.delegate = self
        soundPlayer.prepareToPlay()
        soundPlayer.play()
    }
    
    func getCacheDirectory() -> String {
        let paths = NSSearchPathForDirectoriesInDomains(.cachesDirectory, .userDomainMask, true)
        return paths[0]
    }
    
    func getFileURL() -> URL {
        let path = "\(getCacheDirectory())\(fileName)"
        let filePath = URL(fileURLWithPath: path)
        return filePath
    }
    
    func audioPlayerDidFinishPlaying(_ player: AVAudioPlayer, successfully flag: Bool) {
        recordButton.isEnabled = true
        playButton.setImage(uiElement.currentTheme().playImage, for: .normal)
    }
    
    func audioPlayerDecodeErrorDidOccur(_ player: AVAudioPlayer, error: Error?) {
        print("Error while playing audio \(error!.localizedDescription)")
    }
    
    func audioRecorderDidFinishRecording(_ recorder: AVAudioRecorder, successfully flag: Bool) {
        playButton.isEnabled = true
        recordButton.setImage(#imageLiteral(resourceName: "record"), for: .normal)
    }
    
    func audioRecorderEncodeErrorDidOccur(_ recorder: AVAudioRecorder, error: Error?) {
        print("Error while recording audio \(error!.localizedDescription)")
    }
    
    //MARK: data
    let postObject = PostObject()
    var isUpdatingData = false
    var dataCount = 0
    var thereIsNoMoreData = false
    var didLoadLocalData = false
    
    func resetDataCounting() {
        dataCount = 0
        thereIsNoMoreData = false
    }
    
    func loadMentionsAndCompletes(_ noteIds: Array<String>) {
        let query = PFQuery(className: "Post")
        query.whereKey("objectId", containedIn: noteIds)
        query.whereKey("isRemoved", equalTo: false)
        query.findObjectsInBackground {
            (objects: [PFObject]?, error: Error?) -> Void in
            if error == nil {
                if let objects = objects {
                    for object in objects {
                        for i in 0..<self.posts.count {
                            if object.objectId == self.posts[i].objectId {
                                if let mentions = object["mentions"] as? Array<String> {
                                    self.posts[i].mentions = mentions
                                    
                                } else {
                                    self.posts[i].mentions = []
                                }
                                
                                if let completes = object["completes"] as? Array<String> {
                                    self.posts[i].completed = completes
                                    
                                } else {
                                    self.posts[i].completed = []
                                }
                                
                                continue
                            }
                        }
                    }
                }
                
                self.tableView?.reloadData()
                
            } else {
                print("Error: \(error!)")
            }
        }
    }
    
    func loadComments(_ row: Int) {
        let query = PFQuery(className: "Comment")
        query.whereKey("postId", equalTo: self.filteredPosts[row].objectId!)
        query.whereKey("isRemoved", equalTo: false)
        query.findObjectsInBackground {
            (objects: [PFObject]?, error: Error?) -> Void in
            if error == nil {
                if self.filteredPosts.indices.contains(row) {
                    if let objects = objects {
                        var commentText = [String]()
                        var commentDate = [String]()
                        var commentObjectId = [String]()
                        var commentUserId = [String]()
                        
                        for object in objects {
                            let newCommentText = object["text"] as! String
                            commentText.append(newCommentText)
                            
                            let newCommentDate = self.customDate.formatDate(date: object.createdAt!)
                            commentDate.append(newCommentDate)
                            
                            let newCommentObjectId = object.objectId
                            commentObjectId.append(newCommentObjectId!)
                            
                            let newCommentUserId = object["userId"] as! String
                            commentUserId.append(newCommentUserId)
                        }
                        
                        self.filteredPosts[row].commentText = commentText
                        self.filteredPosts[row].commentDate = commentDate
                        self.filteredPosts[row].commentObjectId = commentObjectId
                        self.filteredPosts[row].commentUserId = commentUserId
                    }
                }                
                
            } else {
                print("Error: \(error!)")
            }
        }
    }
    
    func loadUserInfoFromCloud(_ userId: String, row: Int?) {
        let query = PFQuery(className: "_User")
        query.getObjectInBackground(withId: userId) {
            (user: PFObject?, error: Error?) -> Void in
            if let error = error {
                print(error)
                
            } else if let user = user {
                var name: String!
                var username: String!
                
                if let nameObject = user["name"] as? String {
                    name = nameObject
                    
                } else {
                    name = "Name"
                }
                
                if let usernameObject = user["username"] as? String {
                    username = usernameObject
                    
                } else {
                    username = "Username"
                }
                
                if let row = row {
                    if self.filteredPosts.indices.contains(row) {
                        self.filteredPosts[row].name = name
                        self.filteredPosts[row].username = username
                        
                        if let userImageFile = user["userImage"] as? PFFile {
                            self.filteredPosts[row].userImageURL = userImageFile.url
                        }
                        
                        let indexPath = IndexPath(item: row, section: 0)
                        self.tableView?.reloadRows(at: [indexPath], with: .none)
                    }
                }
            }
        }
    }
    
    func loadCurrentUserInfo() {
        let query = PFQuery(className:"_User")
        query.getObjectInBackground(withId: PFUser.current()!.objectId!) {
            (user: PFObject?, error: Error?) -> Void in
            if let error = error {
                print(error)
                
            } else if let user = user {
                var name = ""
                if let displayName = user["name"] as? String {
                    name = displayName
                    
                } else if let username = user["username"] as? String {
                    name = username
                }
                
                self.name = name
                self.uiElement.setDefaultUserDisplayName(name)
            }
        }
    }
    
    func loadHashtagsFromCloud() {
        let query = PFQuery(className: "Post")
        query.whereKey("userId", equalTo: self.currentUser.objectId!)
        query.whereKeyExists("hashtags")
        query.whereKey("isRemoved", equalTo: false)
        query.addDescendingOrder("createdAt")
        query.findObjectsInBackground {
            (objects: [PFObject]?, error: Error?) -> Void in
            if error == nil {
                if let objects = objects {
                    for object in objects {
                        if let hashtags = object["hashtags"] as? Array<String> {
                            for hashtag in hashtags {
                                if !self.userHashtags.contains(hashtag) {
                                    self.userHashtags.append(hashtag)
                                }
                            }
                        }
                    }
                    self.tableView?.reloadData()
                }
                
            } else {
                print("Error: \(error!)")
            }
        }
    }
    
    func loadData(_ hashtags: Array<String>?) {
        let objectIds: Array<String> = self.posts.map {$0.objectId!}
        let userId = self.currentUser.objectId!
        
        let currentUserPostsQuery = PFQuery(className: "Post")
        currentUserPostsQuery.whereKey("userId", equalTo: userId)
        
        let mentionPostQuery = PFQuery(className: "Post")
        mentionPostQuery.whereKey("mentions", containedIn: [userId])
        
        let query = PFQuery.orQuery(withSubqueries: [currentUserPostsQuery, mentionPostQuery])
        query.limit = 25
        query.whereKey("objectId", notContainedIn: objectIds)
        query.whereKey("isRemoved", equalTo: false)
        query.addDescendingOrder("createdAt")
        if self.shouldHideCompletes {
           query.whereKey("todo", equalTo: false)
        }
        if let hashtags = hashtags {
            query.whereKey("hashtags", containsAllObjectsIn: hashtags)
        }
        query.findObjectsInBackground {
            (objects: [PFObject]?, error: Error?) -> Void in
            if error == nil {
                if let objects = objects {
                    for object in objects {
                        let post = self.postObject.createNewPostObject(object)
                        
                        let filteredObjectIds: Array<String> = self.filteredPosts.map {$0.objectId!}
                        if !filteredObjectIds.contains(post.objectId!) {
                            self.filteredPosts.append(post)
                        }
                        
                        if hashtags == nil {
                            let postObjectIds: Array<String> = self.posts.map {$0.objectId!}
                            if !postObjectIds.contains(post.objectId!) {
                                self.posts.append(post)
                            }
                        }
                    }
                    
                    self.checkStatusOfPosts(hashtags)
                }
                
            } else {
                print("Error: \(error!)")
            }
        }
    }
    
    func loadNotesCurrentUserIsMentionedIn() {
        let objectIds: Array<String> = self.filteredPosts.map {$0.objectId!}
        
        let query = PFQuery(className: "Post")
        query.whereKey("mentions", containedIn: [currentUser.objectId!])
        query.whereKey("objectId", notContainedIn: objectIds)
        if self.shouldHideCompletes {
            query.whereKey("completed", notContainedIn: [self.currentUser.objectId!])
        }
        query.whereKey("isRemoved", equalTo: false)
        query.addDescendingOrder("createdAt")
        query.findObjectsInBackground {
            (objects: [PFObject]?, error: Error?) -> Void in
            if error == nil {
                if let objects = objects {
                    var currentPosts = self.posts
                    
                    for object in objects {
                        let post = self.postObject.createNewPostObject(object)
                        currentPosts.append(post)
                    }
                    
                    self.posts = currentPosts.sorted(by: {$0.date > $1.date})
                    
                    self.filteredPosts = self.posts
                    
                    self.checkStatusOfPosts(nil)
                }
                
            } else {
                // Log details of the failure
                print("Error: \(error!)")
                self.checkStatusOfPosts(nil)
            }
        }
    }
    
    func postToCloud(_ post: Post) {
        let password = Secure().getPasswordForEncryption()
        
        if password != "error" {
            let newPost = PFObject(className: "Post")
            newPost["userId"] = self.currentUser.objectId!
            
            let hashtags = self.text.getElementInTextview(post.text, element: "#")
            if hashtags.count != 0 {
                newPost["hashtags"] = hashtags
            }
            
            if let mentions = post.mentions {
                newPost["mentions"] = mentions
                newPost["text"] = post.text
                if let newMedia = self.newMedia {
                    newPost["media"] = newMedia
                }
                
                if let newAudio = self.parseAudioFile {
                    newPost["audio"] = newAudio
                    self.parseAudioFile = nil
                }
                
            } else {
                let textData = post.text.data(using: .utf8)
                newPost["encryptedText"] = Secure().encrypt(textData!, password: password)
                
                if let media = post.media {
                    let imageData = media.jpegData(compressionQuality: 0.5)
                    newPost["encryptedMedia"] = Secure().encrypt(imageData!, password: password)
                }
                
                if let audio = post.audio {
                    newPost["encryptedAudio"] = Secure().encrypt(audio, password: password)
                }
            }
            
            newPost["todo"] = false

            newPost["isRemoved"] = false
            newPost.saveInBackground {
                (success: Bool, error: Error?) -> Void in
                if (success) {
                    Local().update(post.objectId!, newObjectId: newPost.objectId, post: nil)
                    self.filteredPosts[0].objectId = newPost.objectId
                    if let mentionUserIds = post.mentions {
                        if let name = self.name {
                            for toUserId in mentionUserIds {
                                self.uiElement.sendAlert("\(name) mentioned you in their post.", toUserId: toUserId )
                            }
                        }
                    }
                }
            }
            
        } else {
            uiElement.showAlert("Oops", message: "There was an error retrieving your key for post encryption", target: self)
        }
    }
    
    func deletePost(_ objectId: String, row: Int) {
        let query = PFQuery(className: "Post")
        query.whereKey("objectId", equalTo: objectId)
        query.whereKey("userId", equalTo: self.currentUser.objectId!)
        query.getFirstObjectInBackground {
            (object: PFObject?, error: Error?) -> Void in
            if error != nil || object == nil {
                print("The getFirstObject request failed.")
                
            } else {
                object!["isRemoved"] = true
                object?.saveEventually()
                Local().deleteObject(object!.objectId!)
            }
        }
    }
    
    func clearCurrentHashtag(_ hashtagToClear: String) {
        //TODO: do something where person can delete without internet connection and will be updated later.
        let query = PFQuery(className:"Post")
        query.whereKey("userId", equalTo: self.currentUser.objectId!)
        query.whereKey("hashtags", contains: hashtagToClear)
        query.whereKey("isRemoved", equalTo: false)
        query.findObjectsInBackground {
            (objects: [PFObject]?, error: Error?) -> Void in
            if error == nil {
                if let objects = objects {
                    for object in objects {
                        object["isRemoved"] = true
                        object.saveInBackground()
                        Local().deleteObject(object.objectId!)
                    }
                    self.filteredPosts = self.posts
                }
                
            } else {
                // Log details of the failure
                print("Error: \(error!)")
            }
        }
    }
    
    func updatePost(_ post: Post) {
        var currentTODOStatus: Bool?
        
        let query = PFQuery(className: "Post")
        query.getObjectInBackground(withId: post.objectId!) {
            (object: PFObject?, error: Error?) -> Void in
            if error != nil {
                //print(error)
                
            } else if let object = object {
                if post.mentions?.count == 0 || post.mentions == nil {
                    let textData = post.text.data(using: .utf8)
                    let password = Secure().getPasswordForEncryption()
                    object["encryptedText"] = Secure().encrypt(textData!, password: password)
                    
                } else {
                    object["text"] = post.text
                }
                
                if let todo = post.todo {
                    if let currentTodoStatus = object["todo"] as? Bool {
                        currentTODOStatus = currentTodoStatus
                    }
                    
                    object["todo"] = todo
                }
                
                if let completed = post.completed {
                    object["completed"] = completed
                }
                
                if let mentions = post.mentions {
                    object["mentions"] = mentions
                }
                
                object["hashtags"] = self.text.getElementInTextview(post.text, element: "#")
                
                object.saveEventually {
                    (success: Bool, error: Error?) -> Void in
                    if (success) {
                        if let todo = post.todo {
                            //avoid re-sending notifications when data is updated
                            if todo && currentTODOStatus != todo {
                                if let name = self.name {
                                    if post.userId != self.currentUser.objectId! {
                                        self.uiElement.sendAlert("\(name) completed your post.", toUserId: post.userId)
                                    }
                                    
                                    if let mentionUsers = post.mentions {
                                        for user in mentionUsers {
                                            self.uiElement.sendAlert("\(name) completed a post you were mentioned in.", toUserId: user)
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }
    
    func getLocalData() {
        DispatchQueue(label: "background").async {
            autoreleasepool {
                if let userId = self.currentUser.objectId {
                    let realm = try! Realm()
                    var realmPost: Results<RealmPost>
                    realmPost = realm.objects(RealmPost.self).sorted(byKeyPath: "date", ascending: false).filter("userId == '\(userId)'")
                    
                    for localObject in realmPost {
                        let post = Post(objectId: localObject.objectId, text: localObject.text, date: localObject.date, reminder: nil, todo: localObject.todo, media: nil, userId: localObject.userId, username: nil, name: nil, userImageURL: nil, completed: nil, mentions: nil, commentText: nil, commentUserId: nil, commentObjectId: nil, commentDate: nil, audio: nil)
                        
                        self.reminder.reminderCenter.getPendingNotificationRequests(completionHandler: { reminders in
                            for reminder in reminders {
                                if reminder.identifier == post.objectId! {
                                    post.reminder = reminder
                                    break
                                }
                            }
                        })
                        
                        if let media = localObject.media {
                            post.media = UIImage(data: media, scale: 1.0)
                        }
                        
                        if let audio = localObject.audio {
                            post.audio = audio
                        }
                        
                        let hashtags = self.text.getElementInTextview(localObject.text, element: "#")
                        for hashtag in hashtags {
                            if !self.userHashtags.contains(hashtag) {
                                self.userHashtags.append(hashtag)
                            }
                        }
                        
                        if self.shouldHideCompletes {
                            if let todo = post.todo {
                                if !todo {
                                    self.filteredPosts.append(post)
                                }
                            }
                            
                        } else {
                            self.filteredPosts.append(post)
                        }
                        
                        self.posts.append(post)
                    }
                }
                
                DispatchQueue.main.async {
                    self.loadHashtagsFromCloud()
                    self.didLoadLocalData = true
                    self.loadMentionsAndCompletes(self.posts.map({$0.objectId!}))
                    
                    if self.posts.count == 5 || self.posts.count == 10 || self.posts.count == 15 || self.posts.count > 20 && self.currentUser.objectId != "ac23NsVQ0d" {
                        SKStoreReviewController.requestReview()
                    }
                    
                    if self.filteredPosts.count < 25 {
                        self.loadData(nil)
                        
                    } else {
                        self.loadNotesCurrentUserIsMentionedIn()
                    }
                }
            }
        }
    }
    
    func postLocalData(_ post: Post) {
        let realmObject = RealmPost()
        realmObject.userId = post.userId
        realmObject.objectId = post.objectId
        realmObject.text = post.text
        realmObject.date = Date()
        realmObject.todo = false
        
        if let media = post.media {
            let mediaData = media.jpegData(compressionQuality: 0.5)
            realmObject.media = mediaData
        }
        
        if let audio = post.audio {
            realmObject.audio = audio
        }
        
        let realm = try! Realm()
        try! realm.write {
            realm.add(realmObject)
            self.postToCloud(post)
        }
    }
    
    func filterLocalObjects(_ hashtags: Array<String>) {
        var filteredObjects = [Post]()
        
        for i in 0..<self.posts.count {
            let iPost = self.posts[i]
            var postContainsHashtags = true
            let textArray = self.text.getElementInTextview(iPost.text, element: "#")
            for hashtag in hashtags {
                if !textArray.contains(hashtag) {
                    postContainsHashtags = false
                    break
                }
            }
            
            if postContainsHashtags {
                if shouldHideCompletes {
                    if let todo = iPost.todo {
                        if !todo {
                            filteredObjects.append(iPost)
                        }
                        
                    } else {
                        filteredObjects.append(iPost)
                    }
                    
                } else {
                    filteredObjects.append(iPost)
                }
            }
        }
        
        self.filteredPosts = filteredObjects
        
        if filteredObjects.count == 0 {
            self.loadData(hashtags)
            
        } else {
           self.checkStatusOfPosts(hashtags)
        }
    }

    //MARK: Date
    let customDate = CustomDate()

    //required
    func dateTimePicker(_ picker: DateTimePicker, didSelectDate: Date) {
    }
    
    func showDateTimePicker(_ currentTriggerDate: Date, row: Int?) {
        self.textView.resignFirstResponder()
        
        var objectId: String!
        var text: String!
        var doneButtonTitle: String!
        if let row = row {
            objectId = self.filteredPosts[row].objectId
            text = self.filteredPosts[row].text
            doneButtonTitle = "Set Reminder"
        }
        
        let maximumDate = Date().addingTimeInterval(60 * 60 * 24 * 365)
        var minimumDate: Date
        //selected date has to be => minimum date.
        //if no reminder yet, app crashes because selected date is set to Date() a couples seconds after minimum date is set
        if currentTriggerDate < Date() {
            minimumDate = currentTriggerDate
            
        } else {
            minimumDate = Date()
        }
        
        let picker = DateTimePicker.create(minimumDate: minimumDate, maximumDate: maximumDate)
        picker.highlightColor = Color().meArchiveYellow()
        picker.darkColor = Color().black()
        picker.doneButtonTitle = doneButtonTitle
        picker.doneBackgroundColor = Color().meArchiveYellow()
        picker.is12HourFormat = true
        picker.dateFormat = self.reminder.reminderDateFormat
        picker.delegate = self
        picker.completionHandler = { date in
            self.reminder.reminderCenter.removePendingNotificationRequests(withIdentifiers: [objectId!])
            if let newReminder = self.reminder.set(objectId!, text: text, date: date, target: self) {
                self.filteredPosts[row!].reminder = newReminder
                DispatchQueue.main.async {
                    self.tableView?.reloadData()
                    self.logReminderEvent()
                }
                
            } else {
                self.uiElement.showAlert("Oops", message: "There was an issue creating your reminder.", target: self)
            }
        }
        
        picker.show()
    }
    
    //mark: friends
    var mentionsInTextView = [MEAContacts]()
    var mentions = [MEAContacts]()
    var friends = [MEAContacts]()
    var filteredFriends = [MEAContacts]()
    var showFriends = false
    let friendsReuse = "contactsReuse"
    let userReuse = "userReuse"
    
    func friendsAutoCompleteCell(_ indexPath: IndexPath) -> MainTableViewCell {
        var cell: MainTableViewCell!
        
        cell = self.autoCompletionView.dequeueReusableCell(withIdentifier: friendsReuse) as? MainTableViewCell
        
        let contact = self.filteredFriends[indexPath.row]
        cell.actionButton.isHidden = true
        
        let personImage = uiElement.currentTheme().personImage
        
        if let userImageURL = contact.userImageURL {
            cell.userImage.kf.setImage(with: URL(string: userImageURL ), placeholder: personImage, options: nil, progressBlock: nil, completionHandler: nil)
            
        } else {
            cell.userImage.image = personImage
        }
        
        if let username = contact.username {
            cell.username.text = "@\(username)"
            
        } else {
            cell.username.text = contact.phoneNumber
        }
        
        if let name = contact.displayName {
            cell.name.text = name
            
        } else {
            cell.name.text = ""
        }

        cell.selectionStyle = .default
        cell.backgroundColor = uiElement.currentTheme().backgroundColor
        
        //different color shows behind cell .. getting superview color to match cell
        cell.superview?.backgroundColor = uiElement.currentTheme().backgroundColor
        cell.hashtag.textColor = uiElement.currentTheme().hashTagColor
        
        return cell
    }
    
    func loadFriends() {
        var userIds = [String]()
        
        let fromCurrentUserQuery = PFQuery(className: "Friends")
        fromCurrentUserQuery.whereKey("fromUserId", equalTo:  self.currentUser.objectId!)
        
        let toCurrentUserQuery = PFQuery(className: "Friends")
        toCurrentUserQuery.whereKey("toUserId", equalTo:  self.currentUser.objectId!)
        
        let query = PFQuery.orQuery(withSubqueries: [fromCurrentUserQuery, toCurrentUserQuery])
        query.whereKey("isRemoved", equalTo: false)
        query.whereKey("isConfirmed", equalTo: true)
        query.findObjectsInBackground {
            (objects: [PFObject]?, error: Error?) -> Void in
            if error == nil {
                if let objects = objects {
                    for object in objects {
                        let fromUserId = object["fromUserId"] as! String
                        let toUserId = object["toUserId"] as! String
                        
                        if fromUserId == self.currentUser.objectId! {
                            userIds.append(toUserId)
                            
                        } else {
                            userIds.append(fromUserId)
                        }
                    }
                }
                
                self.loadFriendInfo(friendUserIds: userIds)
            } else {
                print("Error: \(error!)")
            }
        }
    }
    
    func loadFriendInfo(friendUserIds: Array<String>) {
        let query = PFQuery(className: "_User")
        query.whereKey("objectId", containedIn: friendUserIds)
        query.findObjectsInBackground {
            (objects: [PFObject]?, error: Error?) -> Void in
            if error == nil {
                if let objects = objects {
                    for object in objects {
                        let userObject = MEAContacts(userId: object.objectId, displayName: nil, username: nil, userImageURL: nil, phoneNumber: nil, isFriendshipConfirmed: true, postObjectId: nil)
                        
                        if let name = object["name"] as? String {
                            userObject.displayName = name
                        }
                        
                        if let username = object["username"] as? String {
                            userObject.username = username
                        }
                        
                        if let userImageFile = object["userImage"] as? PFFile {
                            userObject.userImageURL = userImageFile.url!
                        }
                        
                        self.friends.append(userObject)
                    }
                }
                self.tableView?.reloadData()
                
            } else {
                print("Error: \(error!)")
            }
        }
    }
    
    //MARK: Log Events
    func logTodoEvent() {
        Analytics.logEvent("new_complete", parameters: [
            "name": "New Complete",
            "full_text": "New Complete"
            ])
    }
    
    func logReminderEvent() {
        Analytics.logEvent("new_reminder", parameters: [
            "name": "New Reminder",
            "full_text": "New Reminder"
            ])
    }
    
    //MARK: mich
    func setUpConfetti() {
        confettiView = SAConfettiView(frame: self.view.bounds)
        confettiView.intensity = 0.75
        confettiView.type = .Star
    }
    
    func startConfetti() {
        self.view.addSubview(confettiView)
        confettiView.startConfetti()
    }
    
    func stopConfetti() {
        self.confettiView.stopConfetti()
        self.confettiView.removeFromSuperview()
    }
    
    func scrollUp() {
        DispatchQueue.main.async {
            if self.filteredPosts.indices.contains(0) {
                let indexPath = IndexPath(row: 0, section: 0)
                self.tableView?.scrollToRow(at: indexPath, at: .top , animated: true)
            }
        }
    }
}

// Helper function inserted by Swift 4.2 migrator.
fileprivate func convertFromUIImagePickerControllerInfoKeyDictionary(_ input: [UIImagePickerController.InfoKey: Any]) -> [String: Any] {
	return Dictionary(uniqueKeysWithValues: input.map {key, value in (key.rawValue, value)})
}

// Helper function inserted by Swift 4.2 migrator.
fileprivate func convertFromUIImagePickerControllerInfoKey(_ input: UIImagePickerController.InfoKey) -> String {
	return input.rawValue
}
