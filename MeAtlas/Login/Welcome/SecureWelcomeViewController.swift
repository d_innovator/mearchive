//
//  SecureWelcomeViewController.swift
//  MeAtlas
//
//  Created by Dominic  Smith on 6/11/18.
//  Copyright © 2018 Dominic Smith. All rights reserved.
//

import UIKit
import SnapKit

class SecureWelcomeViewController: UIViewController {
    
    lazy var mearchiveLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont(name: "HelveticaNeue-Bold", size: 30)
        label.text = "MeArchive"
        label.textAlignment = .center
        label.numberOfLines = 0
        label.textColor = Color().black()
        return label
    }()
    
    lazy var secureArchive: UILabel = {
        let label = UILabel()
        label.font = UIFont(name: "HelveticaNeue-Bold", size: 25)
        label.text = "Protect Your Data"
        label.textAlignment = .left
        label.numberOfLines = 0
        label.textColor = Color().black()
        return label
    }()
    
    lazy var lock: UIImageView = {
        let image = UIImageView()
        image.image = #imageLiteral(resourceName: "lock")
        return image
    }()
    
    lazy var subTitleLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont(name: "HelveticaNeue", size: 20)
        label.text = "By default, your archive is end to end encrypted. Only you can view it's contents."
        label.textAlignment = .left
        label.numberOfLines = 0
        label.textColor = Color().black()
        return label
    }()
    
    lazy var pageIndicator: UIImageView = {
        let image = UIImageView()
        image.image = #imageLiteral(resourceName: "page-1")
        image.contentMode = .scaleAspectFill
        return image
    }()
    
    lazy var nextButton: UIButton = {
        let button = UIButton()
        button.setTitle("Get Started", for: .normal)
        button.titleLabel?.font = UIFont(name: UIElement().mainFont, size: 17)
        button.setTitleColor(Color().black(), for: .normal)
        button.backgroundColor = Color().meArchiveYellow()
        button.layer.cornerRadius = 3
        button.clipsToBounds = true
        return button
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = .white
        self.view.addSubview(mearchiveLabel)
        self.view.addSubview(secureArchive)
        self.view.addSubview(lock)
        self.view.addSubview(subTitleLabel)
        self.view.addSubview(pageIndicator)
        self.view.addSubview(nextButton)
        self.nextButton.addTarget(self, action: #selector(didPressGetStartedButton(_:)), for: .touchUpInside)
        
        mearchiveLabel.snp.makeConstraints { (make) -> Void in
            make.top.equalTo(self.view).offset(50)
            make.left.equalTo(self.view).offset(UIElement().leftOffset)
            make.right.equalTo(self.view).offset(UIElement().rightOffset)
        }
        
        lock.snp.makeConstraints { (make) -> Void in
            make.height.equalTo(self.view.frame.height / 2 - 50)
            make.top.equalTo(self.mearchiveLabel.snp.bottom).offset(UIElement().topOffset)
            make.left.equalTo(self.view).offset(UIElement().leftOffset)
            make.right.equalTo(self.view).offset(UIElement().rightOffset)
        }
        
        secureArchive.snp.makeConstraints { (make) -> Void in
            make.top.equalTo(self.lock.snp.bottom).offset(UIElement().topOffset)
            make.left.equalTo(self.view).offset(UIElement().leftOffset)
            make.right.equalTo(self.view).offset(UIElement().rightOffset)
        }
        
        subTitleLabel.snp.makeConstraints { (make) -> Void in
            make.top.equalTo(self.secureArchive.snp.bottom).offset(UIElement().topOffset)
            make.left.equalTo(self.view).offset(UIElement().leftOffset)
            make.right.equalTo(self.view).offset(UIElement().rightOffset)
        }
        
        nextButton.snp.makeConstraints { (make) -> Void in
            make.height.equalTo(UIElement().buttonHeight)
            make.top.equalTo(self.subTitleLabel.snp.bottom).offset(UIElement().topOffset)
            make.left.equalTo(self.view).offset(UIElement().leftOffset)
            make.right.equalTo(self.view).offset(UIElement().rightOffset)
        }
        
        pageIndicator.snp.makeConstraints { (make) -> Void in
            make.height.equalTo(12)
            make.width.equalTo(24)
            make.top.equalTo(self.nextButton.snp.bottom).offset(UIElement().topOffset)
            make.left.equalTo(self.view.frame.width / 2 - 12)
        }
    }
    
    @objc func didPressGetStartedButton(_ sender: UIButton) {
        let storyboard = UIStoryboard(name: "Login", bundle: nil)
        let initialViewController = storyboard.instantiateViewController(withIdentifier: "welcome")
        self.present(initialViewController, animated: true, completion: nil)
    }
}
