//
//  PhoneNumberViewController.swift
//  MeAtlas
//
//  Created by Dominic  Smith on 7/4/18.
//  Copyright © 2018 Dominic Smith. All rights reserved.
//

import UIKit
import SnapKit
import Firebase
import NVActivityIndicatorView

class PhoneNumberViewController: UIViewController, NVActivityIndicatorViewable {
    
    let uiElement = UIElement()
    
    var fullPhoneNumberString: String!
    
    lazy var phoneNumberLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont(name: "\(uiElement.mainFont)-Bold", size: 25)
        label.text = "What's your number?"
        label.textAlignment = .center
        label.numberOfLines = 0
        label.textColor = uiElement.currentTheme().textColor
        return label
    }()
    
    lazy var phoneNumberDescriptionLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont(name: uiElement.mainFont, size: 20)
        label.text = "We'll text you a code to verify"
        label.textAlignment = .center
        label.numberOfLines = 0
        label.textColor = uiElement.currentTheme().textColor
        return label
    }()
    
    /*lazy var countryCodeButton: UIButton = {
        let button = UIButton()
        button.titleLabel?.font = UIFont(name: uiElement.mainFont, size: 17)
        button.titleLabel?.textAlignment = .center
        button.setTitle("+1", for: .normal)
        button.backgroundColor = Color().meArchiveYellow()
        button.setTitleColor(Color().black(), for: .normal)
        button.layer.cornerRadius = 3
        button.clipsToBounds = true
        return button
    }()*/
    
    lazy var countryCodePlus: UILabel = {
        let label = UILabel()
        label.font = UIFont(name: "\(uiElement.mainFont)-bold" , size: 20)
        label.text = "+"
        label.numberOfLines = 0
        label.textColor = uiElement.currentTheme().textColor
        return label
    }()
    
    lazy var countryCodeText: UITextField = {
        let text = UITextField()
        text.placeholder = "1"
        text.text = "1"
        text.font = UIFont(name: uiElement.mainFont, size: 17)
        text.backgroundColor = .white
        text.borderStyle = .roundedRect
        text.clearButtonMode = .whileEditing
        text.keyboardType = .phonePad
        return text
    }()
    
    lazy var phoneNumberText: UITextField = {
        let text = UITextField()
        text.placeholder = "609 555 0123"
        text.font = UIFont(name: uiElement.mainFont, size: 17)
        text.backgroundColor = .white
        text.borderStyle = .roundedRect
        text.clearButtonMode = .whileEditing
        text.keyboardType = .phonePad
        return text
    }()
    
    lazy var nextButton: UIButton = {
        let button = UIButton()
        button.titleLabel?.font = UIFont(name: uiElement.mainFont, size: 20)
        button.titleLabel?.textAlignment = .right
        button.setTitle("Next", for: .normal)
        button.backgroundColor = Color().meArchiveYellow()
        button.setTitleColor(Color().black(), for: .normal)
        button.layer.cornerRadius = 3
        button.clipsToBounds = true
        return button
    }()

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.view.backgroundColor = UIElement().currentTheme().backgroundColor
        navigationController?.navigationBar.barTintColor = UIElement().currentTheme().backgroundColor
        navigationController?.navigationBar.tintColor = UIElement().currentTheme().textColor
        navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIElement().currentTheme().textColor]

        self.view.addSubview(phoneNumberLabel)
        self.view.addSubview(phoneNumberDescriptionLabel)
        self.view.addSubview(countryCodeText)
        self.view.addSubview(countryCodePlus)
        self.view.addSubview(phoneNumberText)
        self.view.addSubview(nextButton)
        nextButton.addTarget(self, action: #selector(didPressNextButton(_:)), for: .touchUpInside)
        
        phoneNumberLabel.snp.makeConstraints { (make) -> Void in
            make.top.equalTo(self.view).offset(UIElement().uiViewTopOffset(self))
            make.left.equalTo(self.view).offset(uiElement.leftOffset)
            make.right.equalTo(self.view).offset(uiElement.rightOffset)
        }
        
        phoneNumberDescriptionLabel.snp.makeConstraints { (make) -> Void in
            make.top.equalTo(self.phoneNumberLabel.snp.bottom).offset(uiElement.elementOffset)
            make.left.equalTo(self.view).offset(uiElement.leftOffset)
            make.right.equalTo(self.view).offset(uiElement.rightOffset)
        }
        
        countryCodePlus.snp.makeConstraints { (make) -> Void in
            make.top.equalTo(self.phoneNumberDescriptionLabel.snp.bottom).offset(uiElement.elementOffset)
            make.left.equalTo(self.view).offset(uiElement.leftOffset)
        }
        
        countryCodeText.snp.makeConstraints { (make) -> Void in
            make.width.equalTo(75)
            make.top.equalTo(self.countryCodePlus)
            make.left.equalTo(self.countryCodePlus.snp.right).offset(uiElement.elementOffset)
        }
        
        phoneNumberText.snp.makeConstraints { (make) -> Void in
            make.width.equalTo(200)
            make.top.equalTo(countryCodePlus)
            make.left.equalTo(self.countryCodeText.snp.right).offset(uiElement.elementOffset)
            //make.right.equalTo(self.view).offset(uiElement.rightOffset)
        }
        
        nextButton.snp.makeConstraints { (make) -> Void in
            make.height.equalTo(uiElement.buttonHeight)
            make.top.equalTo(phoneNumberText.snp.bottom).offset(uiElement.elementOffset)
            make.left.equalTo(self.view).offset(uiElement.leftOffset)
            make.right.equalTo(self.view).offset(uiElement.rightOffset)
        }
        
        self.phoneNumberText.becomeFirstResponder()
        
        let menuButton = UIBarButtonItem(title: "Cancel", style: .plain, target: self, action: #selector(self.doneAction(_:)))
        self.navigationItem.leftBarButtonItem = menuButton
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let nextView = segue.destination as! PhoneVerificationViewController
        //nextView.phoneNumberString = "\(countryCode)\(self.phoneNumberText.text!)"
        nextView.phoneNumberString = fullPhoneNumberString
    }
    
    @objc func didPressNextButton(_ sender: UIButton) {
        self.startAnimating()
        if phoneNumberIsVerified() {
            fullPhoneNumberString = "+\(countryCodeText.text!)\(phoneNumberText.text!)"
            PhoneAuthProvider.provider().verifyPhoneNumber(fullPhoneNumberString, uiDelegate: nil) { (verificationID, error) in
                self.stopAnimating()
                if let error = error {
                    print(error.localizedDescription)
                    //self.showMessagePrompt(error.localizedDescription)
                    self.uiElement.showAlert("Oops", message: error.localizedDescription, target: self)
                    return
                }
                UserDefaults.standard.set(verificationID, forKey: "authVerificationID")
                self.performSegue(withIdentifier: "showPhoneVerification", sender: self)
            }
            
        } else {
            self.stopAnimating()
        }
    }
    
    func phoneNumberIsVerified() -> Bool {
        if self.phoneNumberText.text!.isEmpty {
            phoneNumberText.attributedPlaceholder = NSAttributedString(string:"609 555 0123",
                                                                 attributes:[NSAttributedString.Key.foregroundColor: UIColor.red])
            self.phoneNumberText.text = ""
            return false

        } else if self.countryCodeText.text!.isEmpty {
            phoneNumberText.attributedPlaceholder = NSAttributedString(string:"1",
                                                                       attributes:[NSAttributedString.Key.foregroundColor: UIColor.red])
            self.countryCodeText.text = ""
        }
        
        return true
    }
    
    @objc func doneAction(_ sender: UIBarButtonItem) {
        let storyboard = UIStoryboard(name: "Login", bundle: nil)
        let initialViewController = storyboard.instantiateViewController(withIdentifier: "welcome")
        self.present(initialViewController, animated: true, completion: nil)
    }
}


