//
//  NewUsernameViewController.swift
//  MeAtlas
//
//  Created by Dominic  Smith on 8/27/18.
//  Copyright © 2018 Dominic Smith. All rights reserved.
//

import UIKit
import Parse
import SnapKit
import NVActivityIndicatorView

class NewUsernameViewController: UIViewController, NVActivityIndicatorViewable {
    let uiElement = UIElement()
    
    var phoneNumber: String! 
    
    lazy var usernameLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont(name: "\(uiElement.mainFont)-bold", size: 30)
        label.text = "Choose a Username"
        label.textAlignment = .center
        label.numberOfLines = 0
        label.textColor = uiElement.currentTheme().textColor
        return label
    }()
    
    lazy var usernameText: UITextField = {
        let label = UITextField()
        label.placeholder = "username"
        label.font = UIFont(name: uiElement.mainFont, size: 17)
        label.backgroundColor = .white
        label.borderStyle = .roundedRect
        label.clearButtonMode = .whileEditing
        return label
    }()
    
    lazy var nextButton: UIButton = {
        let button = UIButton()
        button.titleLabel?.font = UIFont(name: uiElement.mainFont, size: 20)
        button.titleLabel?.textAlignment = .right
        button.setTitle("Next", for: .normal)
        button.backgroundColor = Color().meArchiveYellow()
        button.setTitleColor(Color().black(), for: .normal)
        button.layer.cornerRadius = 3
        button.clipsToBounds = true
        return button
    }()

    override func viewDidLoad() {
        super.viewDidLoad()
        
        if let phoneNumber = UserDefaults.standard.string(forKey: "phoneNumber") {
            self.phoneNumber = phoneNumber
        }
        
        self.view.backgroundColor = UIElement().currentTheme().backgroundColor
        navigationController?.navigationBar.barTintColor = UIElement().currentTheme().backgroundColor
        navigationController?.navigationBar.tintColor = UIElement().currentTheme().textColor
        navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIElement().currentTheme().textColor]
        
        self.view.addSubview(usernameLabel)
        self.view.addSubview(usernameText)
        
        nextButton.addTarget(self, action: #selector(didPressNextButton(_:)), for: .touchUpInside)
        self.view.addSubview(nextButton)
        
        usernameLabel.snp.makeConstraints { (make) -> Void in
            make.top.equalTo(self.view).offset(UIElement().uiViewTopOffset(self))
            make.left.equalTo(self.view).offset(uiElement.leftOffset)
            make.right.equalTo(self.view).offset(uiElement.rightOffset)
        }
        
        usernameText.snp.makeConstraints { (make) -> Void in
            make.top.equalTo(self.usernameLabel.snp.bottom).offset(uiElement.topOffset)
            make.left.equalTo(self.view).offset(uiElement.leftOffset)
            make.right.equalTo(self.view).offset(uiElement.rightOffset)
        }
        
        nextButton.snp.makeConstraints { (make) -> Void in
            make.height.equalTo(uiElement.buttonHeight)
            make.top.equalTo(usernameText.snp.bottom).offset(uiElement.topOffset)
            make.left.equalTo(self.view).offset(uiElement.leftOffset)
            make.right.equalTo(self.view).offset(uiElement.rightOffset)
        }
        
        self.usernameText.becomeFirstResponder()

        // Do any additional setup after loading the view.
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    
    @objc func didPressNextButton(_ sender: UIButton) {
        if self.usernameText.text!.isEmpty {
            self.uiElement.showAlert("Oops", message: "Username required", target: self)
            
        } else {
            let username = self.usernameText.text!.lowercased()
            let trimmedUsername = username.trimmingCharacters(in: .whitespaces)
            let query = PFQuery(className: "_User")
            query.whereKey("username", equalTo: trimmedUsername)
            query.getFirstObjectInBackground {
                (object: PFObject?, error: Error?) -> Void in
                if error != nil || object == nil {
                    self.signUpNewUser(trimmedUsername)
                    
                } else {
                    self.uiElement.showAlert("Oops", message: "Username taken.", target: self)
                }
            }
        }
    }
    
    func signUpNewUser(_ username: String) {
        let user = PFUser()
        user.username = username
        user.password = self.phoneNumber
        user["phoneNumber"] = self.phoneNumber
        user.signUpInBackground{ (succeeded: Bool, error: Error?) -> Void in
            self.stopAnimating()
            if error != nil {
                
            } else {
                let installation = PFInstallation.current()
                installation?["user"] = PFUser.current()
                installation?["userId"] = PFUser.current()?.objectId
                installation?.saveEventually()
                
                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                let initialViewController = storyboard.instantiateViewController(withIdentifier: "notifications")
                self.present(initialViewController, animated: true, completion: nil)
            }
        }
    }
}
