//
//  MainTableViewCell.swift
//  MeAtlas
//
//  Created by Dominic Smith on 12/18/17.
//  Copyright © 2017 Dominic Smith. All rights reserved.
//

import UIKit
import SnapKit
import ActiveLabel

class MainTableViewCell: UITableViewCell {
    
    let color = Color()
    let uiElement = UIElement()
    
    var isCurrentUser = false
    
    let userReuse = "userReuse"
    
    //post actions
    lazy var reminderButton: UIButton = {
        let button = UIButton()
       // button.setImage(uiElement.currentTheme().clockImage, for: .normal)
        button.setTitle("", for: .normal)
        button.titleLabel?.font = UIFont(name: uiElement.mainFont, size: 14)
        button.setTitleColor(uiElement.currentTheme().textColor, for: .normal)
        button.imageView?.contentMode = .scaleAspectFit
        return button
    }()
    
    lazy var commentButton: UIButton = {
        let button = UIButton()
       // button.setImage(uiElement.currentTheme().speechImage, for: .normal)
        button.imageView?.contentMode = .scaleAspectFit
        button.setTitle("", for: .normal)
        button.titleLabel?.font = UIFont(name: uiElement.mainFont, size: 17)
        button.setTitleColor(uiElement.currentTheme().textColor, for: .normal)
        button.setImage(uiElement.currentTheme().speechImage, for: .normal)
        return button
    }()
    
    lazy var todoButton: UIButton = {
        let button = UIButton()
        button.imageView?.contentMode = .scaleAspectFit
        button.setTitle("", for: .normal)
        button.titleLabel?.font = UIFont(name: uiElement.mainFont, size: 17)
        button.setTitleColor(uiElement.currentTheme().textColor, for: .normal)
        //button.setImage(uiElement.currentTheme().checkBoxImage, for: .normal)
        return button
    }()
    
    //regular post
    let reuse = "reuse"
    lazy var bodyLabel: ActiveLabel = {
        let label = ActiveLabel()
        label.font = UIFont(name: uiElement.mainFont, size: 17)
        label.enabledTypes = [.hashtag, .url, .mention]
        label.textColor = uiElement.currentTheme().textColor
        label.numberOfLines = 0
        label.URLColor = uiElement.currentTheme().hashTagColor
        label.mentionColor = uiElement.currentTheme().hashTagColor
        label.hashtagColor = uiElement.currentTheme().hashTagColor
        label.handleURLTap { url in  UIApplication.shared.open(url, options: convertToUIApplicationOpenExternalURLOptionsKeyDictionary([:]), completionHandler: nil) }
        return label
    }()
    
    lazy var hashtag: UILabel = {
        let label = UILabel()
        label.font = UIFont(name: uiElement.mainFont, size: 20)
        label.textColor = uiElement.currentTheme().hashTagColor
        label.numberOfLines = 0
        return label
    }()
    
    lazy var hashtagSubTitle: UILabel = {
        let label = UILabel()
        label.font = UIFont(name: uiElement.mainFont, size: 17)
        label.textColor = uiElement.currentTheme().hashTagColor
        label.numberOfLines = 0
        return label
    }()
    
    lazy var date: UILabel = {
        let label = UILabel()
        label.font = UIFont(name: uiElement.mainFont, size: 12)
        label.textColor = UIColor.darkGray
        return label
    }()
    
    lazy var menuButton: UIButton = {
        let button = UIButton()
        button.setImage(uiElement.currentTheme().menuImage, for: .normal)
        return button
    }()
    
    lazy var spaceLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont(name: uiElement.mainFont, size: 20)
        label.textColor = UIColor.darkGray
        label.text = "|"
        label.isHidden = true
        return label
    }()
    
    lazy var completedButton: UIButton = {
        let button = UIButton()
        button.setTitleColor(UIColor.darkGray, for: .normal)
        button.titleLabel?.font = UIFont(name: uiElement.mainFont, size: 16)
        button.setImage(uiElement.currentTheme().checkImage, for: .normal)
        return button
    }()
    
    lazy var mentionsButton: UIButton = {
        let button = UIButton()
        button.isHidden = true
        button.setTitleColor(UIColor.darkGray, for: .normal)
        button.titleLabel?.font = UIFont(name: uiElement.mainFont, size: 17)
        button.titleLabel?.textAlignment = .left
        button.setImage(uiElement.currentTheme().personImage, for: .normal)
        return button
    }()
    
    lazy var mentionsImage: UIImageView = {
        let image = UIImageView()
        image.image = uiElement.currentTheme().personImage
        image.clipsToBounds = true
        image.contentMode = .scaleAspectFill
        return image
    }()
    
    lazy var mentionsLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont(name: uiElement.mainFont, size: 17)
        label.textColor = uiElement.currentTheme().textColor
        return label
    }()
    
    //contacts
    let contactsReuse = "contactsReuse"
    
    lazy var userImage: UIImageView = {
        let image = UIImageView()
        image.image = #imageLiteral(resourceName: "person_black")
        image.layer.cornerRadius = CGFloat(uiElement.userImageHeightWidth / 2)
        image.clipsToBounds = true
        image.contentMode = .scaleAspectFill
        image.image = uiElement.currentTheme().personImage
        return image
    }()
    
    lazy var name: UILabel = {
        let label = UILabel()
        label.font = UIFont(name: uiElement.mainFont, size: 17)
        label.textColor = UIColor.darkGray
        label.text = "Name"
        label.textAlignment = .left 
        return label
    }()
    
    lazy var username: UILabel = {
        let label = UILabel()
        //label.font = UIFont(name: uiElement.mainFont, size: 14)
        label.font = UIFont(name: uiElement.mainFont, size: 17)
        //label.textColor = uiElement.currentTheme().hashTagColor
        label.textColor = uiElement.currentTheme().textColor
        label.text = "@username"
        return label
    }()
    
    lazy var actionButton: UIButton = {
        let button = UIButton()
        button.layer.cornerRadius = 3
        button.clipsToBounds = true
        button.backgroundColor = color.blue()
        return button
    }()
    
    lazy var actionLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont(name: uiElement.mainFont, size: 14)
        label.textColor = .white
        label.textAlignment = .center
        label.text = "Mention"
        return label
    }()
    
    //media
    let mediaReuse = "mediaReuse"
    let mediaUserReuse = "mediaUserReuse"
    lazy var mediaButton: UIButton = {
        let button = UIButton()
        button.imageView?.contentMode = .scaleAspectFill
        button.layer.cornerRadius = 5
        button.layer.borderWidth = 0.2
        button.layer.borderColor = uiElement.currentTheme().textColor.cgColor
        button.clipsToBounds = true
        return button
    }()
    
    //audio
    let audioReuse = "audioReuse"
    lazy var playBackButton: UIButton = {
        let button = UIButton()
        button.setImage(uiElement.currentTheme().audioFileImage, for: .normal)
        button.imageView?.contentMode = .scaleAspectFill
        return button
    }()
    
    lazy var playBackSlider: UISlider = {
        let slider = UISlider()
        slider.minimumValue = 0
        slider.maximumValue = 100
        slider.tintColor = .darkGray
        return slider
    }()
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        switch reuseIdentifier {
        case reuse, mediaReuse, userReuse, mediaUserReuse, audioReuse:
            self.addSubview(date)
            self.addSubview(bodyLabel)
            self.addSubview(menuButton)
            self.addSubview(mentionsButton)
            self.addSubview(completedButton)
            self.addSubview(commentButton)
            
            menuButton.snp.makeConstraints { (make) -> Void in
                make.height.width.equalTo(25)
                make.right.equalTo(self).offset(uiElement.rightOffset)
            }
            
            bodyLabel.snp.makeConstraints { (make) -> Void in
                make.left.equalTo(self).offset(uiElement.leftOffset)
                make.right.equalTo(self.menuButton.snp.left).offset(uiElement.rightOffset)
            }
            
            if reuseIdentifier == userReuse || reuseIdentifier == mediaUserReuse {
                self.addSubview(userImage)
                self.addSubview(username)
                self.addSubview(name)
                
                userImage.snp.makeConstraints { (make) -> Void in
                    make.width.height.equalTo(uiElement.userImageHeightWidth)
                    make.top.equalTo(self).offset(uiElement.topOffset)
                    make.left.equalTo(self).offset(uiElement.leftOffset)
                }
                
                username.snp.makeConstraints { (make) -> Void in
                    make.top.equalTo(self.userImage).offset(uiElement.elementOffset + 5)
                    make.left.equalTo(self.userImage.snp.right).offset(uiElement.elementOffset)
                }
                
                name.snp.makeConstraints { (make) -> Void in
                    make.top.equalTo(self.username)
                    make.left.equalTo(self.username.snp.right).offset(uiElement.elementOffset)
                }
            }
            
            if reuseIdentifier == userReuse || reuseIdentifier == mediaUserReuse {
                menuButton.snp.makeConstraints { (make) -> Void in
                    make.top.equalTo(self.username)
                }
                
                bodyLabel.snp.makeConstraints { (make) -> Void in
                    make.top.equalTo(self.userImage.snp.bottom).offset(uiElement.elementOffset)
                    make.left.equalTo(self).offset(uiElement.leftOffset)
                }
                
            } else {
                menuButton.snp.makeConstraints { (make) -> Void in
                    make.top.equalTo(self).offset(uiElement.topOffset)
                }
                
                bodyLabel.snp.makeConstraints { (make) -> Void in
                    make.top.equalTo(self.menuButton)
                }
            }
            
            if reuseIdentifier == reuse || reuseIdentifier == userReuse {
                if reuseIdentifier == userReuse {
                    self.setPeopleAndCompleteButtonLayouts(bodyLabel)
                    
                } else {
                    bodyLabel.snp.makeConstraints { (make) -> Void in
                     make.bottom.equalTo(self).offset(uiElement.bottomOffset)
                    }
                }
                
                
            } else if reuseIdentifier == mediaReuse || reuseIdentifier == mediaUserReuse {
                self.addSubview(mediaButton)
                
                mediaButton.snp.makeConstraints { (make) -> Void in
                    make.height.equalTo(200)
                    make.top.equalTo(self.bodyLabel.snp.bottom).offset(1)
                    make.left.equalTo(self.bodyLabel)
                    make.right.equalTo(self).offset(uiElement.rightOffset)
                    make.bottom.equalTo(self).offset(uiElement.bottomOffset)
                }
                
                if reuseIdentifier == mediaUserReuse {
                    self.setPeopleAndCompleteButtonLayouts(mediaButton)
                    
                } else {
                    mediaButton.snp.makeConstraints { (make) -> Void in
                        make.bottom.equalTo(self).offset(uiElement.bottomOffset)
                    }
                }
                
            } else if reuseIdentifier == audioReuse {
                self.addSubview(playBackButton)
                playBackButton.snp.makeConstraints { (make) -> Void in
                    make.width.equalTo(100)
                    make.height.equalTo(50)
                    make.top.equalTo(self.bodyLabel.snp.bottom)
                    make.left.equalTo(self.bodyLabel)
                    make.bottom.equalTo(self).offset(uiElement.bottomOffset)
                }
            }
            
            break
            
        case "autoReuse":
            self.addSubview(hashtag)
            hashtag.snp.makeConstraints { (make) -> Void in
                make.top.equalTo(self).offset(10)
                make.left.equalTo(self).offset(uiElement.leftOffset)
                make.right.equalTo(self).offset(uiElement.rightOffset)
                make.bottom.equalTo(self).offset(-10)
            }
            break
            
        case contactsReuse:
            self.addSubview(userImage)
            self.addSubview(username)
            self.addSubview(name)
            self.addSubview(actionButton)
            self.actionButton.addSubview(actionLabel)
            
            userImage.snp.makeConstraints { (make) -> Void in
                make.height.width.equalTo(uiElement.userImageHeightWidth)
                make.top.equalTo(self).offset(uiElement.topOffset)
                make.left.equalTo(self).offset(uiElement.leftOffset)
                make.bottom.equalTo(self).offset(uiElement.bottomOffset)
            }
            
            actionButton.snp.makeConstraints { (make) -> Void in
                make.height.equalTo(30)
                make.width.equalTo(75)
                make.top.equalTo(userImage).offset(10)
                make.right.equalTo(self).offset(uiElement.rightOffset)
            }
            
            actionLabel.snp.makeConstraints { (make) -> Void in
                make.top.equalTo(actionButton).offset(5)
                make.right.equalTo(actionButton).offset(-5)
                make.left.equalTo(actionButton).offset(5)
                make.bottom.equalTo(actionButton).offset(-5)
            }
            
            name.snp.makeConstraints { (make) -> Void in
                make.top.equalTo(userImage).offset(uiElement.elementOffset)
                make.left.equalTo(userImage.snp.right).offset(uiElement.elementOffset)
                make.right.equalTo(actionButton.snp.left).offset(uiElement.rightOffset)
            }
            
            username.snp.makeConstraints { (make) -> Void in
                make.top.equalTo(name.snp.bottom).offset(uiElement.elementOffset - 2)
                make.left.equalTo(userImage.snp.right).offset(uiElement.elementOffset)
                make.right.equalTo(actionButton.snp.left).offset(uiElement.rightOffset)
            }
            
            break
            
        case "noResultsReuse":
            self.addSubview(bodyLabel)
            bodyLabel.snp.makeConstraints { (make) -> Void in
                make.top.equalTo(self).offset(uiElement.topOffset)
                make.left.equalTo(self).offset(uiElement.leftOffset)
                make.right.equalTo(self).offset(uiElement.rightOffset)
                make.bottom.equalTo(self).offset(uiElement.bottomOffset)
            }
            break
            
        default:
            break
        }
    }
    
    func setActionButtonLayouts(_ topItem: UIView ) {
        todoButton.snp.makeConstraints { (make) -> Void in
            make.top.equalTo(topItem.snp.bottom).offset(uiElement.topOffset)
            make.left.equalTo(topItem)
            make.bottom.equalTo(self).offset(uiElement.bottomOffset)
        }
        
        commentButton.snp.makeConstraints { (make) -> Void in
            make.top.equalTo(todoButton)
            make.left.equalTo(self.todoButton.snp.right).offset(15)
            make.bottom.equalTo(self).offset(uiElement.bottomOffset)
        }
        
        reminderButton.snp.makeConstraints { (make) -> Void in
            make.top.equalTo(commentButton)
            make.left.equalTo(self.commentButton.snp.right).offset(15)
            make.bottom.equalTo(self).offset(uiElement.bottomOffset)
        }
    }
    
    func setPeopleAndCompleteButtonLayouts(_ topItem: UIView) {
        completedButton.snp.makeConstraints { (make) -> Void in
            make.height.equalTo(25)
            make.top.equalTo(topItem.snp.bottom).offset(uiElement.elementOffset)
            make.left.equalTo(topItem)
        }
        
       /*mentionsButton.snp.makeConstraints { (make) -> Void in
            make.height.equalTo(25)
            make.top.equalTo(self.completedButton)
            make.left.equalTo(self.completedButton.snp.right).offset(uiElement.elementOffset)
            make.bottom.equalTo(self).offset(uiElement.bottomOffset)
        }*/
        
        commentButton.snp.makeConstraints { (make) -> Void in
            make.height.equalTo(25)
            make.top.equalTo(self.completedButton)
            make.left.equalTo(self.completedButton.snp.right).offset(uiElement.leftOffset)
            make.bottom.equalTo(self).offset(uiElement.bottomOffset)
        }
    }
    
    ///////////
    // We won’t use this but it’s required for the class to compile
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
}

// Helper function inserted by Swift 4.2 migrator.
fileprivate func convertToUIApplicationOpenExternalURLOptionsKeyDictionary(_ input: [String: Any]) -> [UIApplication.OpenExternalURLOptionsKey: Any] {
	return Dictionary(uniqueKeysWithValues: input.map { key, value in (UIApplication.OpenExternalURLOptionsKey(rawValue: key), value)})
}
