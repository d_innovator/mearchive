//
//  AppDelegate.swift
//  MeAtlas
//
//  Created by Dominic Smith on 12/18/17.
//  Copyright © 2017 Dominic Smith. All rights reserved.
//

import UIKit
import Parse
import CoreMotion
import RealmSwift
import Firebase
import NVActivityIndicatorView
import UserNotifications
import Fabric

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        
        UITabBar.appearance().barTintColor = UIElement().currentTheme().backgroundColor
        UITabBar.appearance().tintColor = UIElement().currentTheme().textColor
        
        FirebaseApp.configure()
        Fabric.sharedSDK().debug = true
        
        let configuration = ParseClientConfiguration {
            $0.applicationId = "E96F386B4D18BEDDE364EA3EBB4FC"
            $0.clientKey = "E517EC63C84AA3B63A2A54DAF3752"
            $0.server = "https://me-archive.herokuapp.com/parse"
        }
        Parse.initialize(with: configuration)
        
        NVActivityIndicatorView.DEFAULT_TYPE = .ballScaleMultiple
        NVActivityIndicatorView.DEFAULT_COLOR = Color().meArchiveYellow()
        NVActivityIndicatorView.DEFAULT_BLOCKER_SIZE = CGSize(width: 60, height: 60)
        NVActivityIndicatorView.DEFAULT_BLOCKER_BACKGROUND_COLOR = UIColor(red: 0, green: 0, blue: 0, alpha: 0.5)
        
        realmConfiguration()
        
        if UserDefaults.standard.bool(forKey: "requireTouchID") && PFUser.current() != nil {
            let storyboard = UIStoryboard(name: "Login", bundle: nil)
            let initialViewController = storyboard.instantiateViewController(withIdentifier: "localAuth")
            self.window?.rootViewController = initialViewController
            
        } else {
            self.goToPage()
        }
        
        return true
    }
    
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        let installation = PFInstallation.current()
        installation?.setDeviceTokenFrom(deviceToken)
        installation?.saveInBackground()
    }
    
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable: Any], fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
        if application.applicationState == UIApplication.State.background {
            PFPush.handle(userInfo)
        }
    }
    
    @available(iOS 10.0, *)
    func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        PFPush.handle(notification.request.content.userInfo)
        completionHandler(.alert)
        
    }
    
    func applicationWillEnterForeground(_ application: UIApplication) {
        if PFUser.current() != nil  {
            if UserDefaults.standard.bool(forKey: "requireTouchID") {
                let storyboard = UIStoryboard(name: "Login", bundle: nil)
                let initialViewController = storyboard.instantiateViewController(withIdentifier: "localAuth")
                self.window?.rootViewController = initialViewController
            }
            
            self.getLocalData()
        }
    }
    
    func goToPage() {
        self.window = UIWindow(frame: UIScreen.main.bounds)
        if PFUser.current() != nil {
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let initialViewController = storyboard.instantiateViewController(withIdentifier: "main")
            self.window?.rootViewController = initialViewController
            
        } else {
            let storyboard = UIStoryboard(name: "Login", bundle: nil)
            let initialViewController = storyboard.instantiateViewController(withIdentifier: "welcome")
            self.window?.rootViewController = initialViewController
        }
    }
    
    func getLocalData() {
        DispatchQueue(label: "background").async {
            autoreleasepool {
                if let userId = PFUser.current()?.objectId {
                    let realm = try! Realm()
                    var realmPost: Results<RealmPost>
                    realmPost = realm.objects(RealmPost.self).sorted(byKeyPath: "date", ascending: false).filter("userId == '\(userId)'")
                    
                    for localObject in realmPost{
                        let post = Post(objectId: localObject.objectId, text: localObject.text, date: localObject.date, reminder: nil, todo: localObject.todo, media: nil, userId: userId, username: nil, name: nil, userImageURL: nil, completed: nil, mentions: nil, commentText: nil, commentUserId: nil, commentObjectId: nil, commentDate: nil, audio: nil)
                        
                        if let media = localObject.media {
                            post.media = UIImage(data: media, scale: 1.0)
                        }
                        
                        if let audio = localObject.audio {
                            post.audio = audio
                        }
                        
                        if !localObject.isSavedToCloud {
                            self.postToCloud(post, localObjectId: localObject.objectId!)
                        }
                    }
                }
            }
        }
    }
    
    func postToCloud(_ post: Post, localObjectId: String ) {
        let password = Secure().getPasswordForEncryption()
        
        if password != "error" {
            let newPost = PFObject(className: "Post")
            newPost["userId"] = PFUser.current()?.objectId
            
            let textData = post.text.data(using: .utf8)
            newPost["encryptedText"] = Secure().encrypt(textData!, password: password)
            
            let hashtags = Text().getElementInTextview(post.text, element: "#")
            if hashtags.count != 0 {
                newPost["hashtags"] = hashtags
            }
            
            if let todo = post.todo {
                newPost["todo"] = todo
            }
            
            if let media = post.media {
                let imageData = media.jpegData(compressionQuality: 0.5)
                newPost["encryptedMedia"] = Secure().encrypt(imageData!, password: password)
            }
            
            if let audio = post.audio {
                newPost["encryptedAudio"] = Secure().encrypt(audio, password: password)
            }
            
            newPost["isRemoved"] = false
            newPost.saveInBackground {
                (success: Bool, error: Error?) -> Void in
                if (success) {
                    Local().update(localObjectId, newObjectId: newPost.objectId, post: nil)
                }
            }
        }
    }
    
    func realmConfiguration() {
        //see link for explanation: https://realm.io/docs/swift/latest#migrations
        let config = Realm.Configuration(
            schemaVersion: 1,
            migrationBlock: { migration, oldSchemaVersion in
                if oldSchemaVersion < 1 {
                }
        })
        Realm.Configuration.defaultConfiguration = config
       let _ = try! Realm()
    }
}

