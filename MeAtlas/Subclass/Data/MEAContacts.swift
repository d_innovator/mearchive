//
//  MEAContacts.swift
//  MeAtlas
//
//  Created by Dominic  Smith on 7/8/18.
//  Copyright © 2018 Dominic Smith. All rights reserved.
//

import UIKit

class MEAContacts {
    var userId: String?
    var displayName: String?
    var username: String?
    var userImageURL: String?
    var phoneNumber: String?
    var isFriendshipConfirmed: Bool?
    var postObjectId: String?
    
    init(userId: String?, displayName: String?, username: String?, userImageURL: String?, phoneNumber: String?, isFriendshipConfirmed: Bool?, postObjectId: String?) {
        self.userId = userId
        self.displayName = displayName
        self.username = username
        self.userImageURL = userImageURL
        self.phoneNumber = phoneNumber
        self.isFriendshipConfirmed = isFriendshipConfirmed
        self.postObjectId = postObjectId
    }
}
