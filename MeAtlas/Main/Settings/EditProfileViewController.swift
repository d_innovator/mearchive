//
//  EditProfileViewController.swift
//  MeAtlas
//
//  Created by Dominic  Smith on 7/30/18.
//  Copyright © 2018 Dominic Smith. All rights reserved.
//

import UIKit
import Parse
import Kingfisher
import RealmSwift
import Contacts

class EditProfileViewController: UIViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    var isNewUser: Bool?
    
    lazy var name: UITextField = {
        let text = UITextField()
        text.placeholder = "Enter Name"
        text.borderStyle = .roundedRect
        text.clearButtonMode = .whileEditing
        return text
    }()
    
    lazy var userImage: UIButton = {
        let button = UIButton()
        button.layer.cornerRadius = 50
        button.clipsToBounds = true
        button.backgroundColor = Color().meArchiveYellow()
        button.setTitle("Add Pic", for: .normal)
        button.titleLabel?.font = UIFont(name: "\(UIElement().mainFont)-bold", size: 17)
        button.setTitleColor(Color().black(), for: .normal)
        button.imageView?.contentMode = .scaleAspectFill
        return button
    }()
    
    var username: String?
    
    var newUserImage: PFFile?

    override func viewDidLoad() {
        super.viewDidLoad()

        getLocalUserData()
        setUpTabBar()
        setUpView()
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    
    func setUpTabBar() {
        self.view.backgroundColor = UIElement().currentTheme().backgroundColor
        navigationController?.navigationBar.barTintColor = UIElement().currentTheme().backgroundColor
        navigationController?.navigationBar.tintColor = UIElement().currentTheme().textColor
        navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIElement().currentTheme().textColor]
        
        let saveButton = UIBarButtonItem(title: "Save", style: .plain, target: self, action: #selector(didPressSaveButton(_:)))
        self.navigationItem.rightBarButtonItem = saveButton
        
        let cancelButton = UIBarButtonItem(title: "Cancel", style: .plain, target: self, action: #selector(didPressCancelButton(_:)))
        self.navigationItem.leftBarButtonItem = cancelButton
    }
    
    func setUpView() {
        self.view.addSubview(name)
        self.view.addSubview(userImage)
        self.userImage.addTarget(self, action: #selector(self.didPressEditImagebutton(_:)), for: .touchUpInside)
        userImage.snp.makeConstraints { (make) -> Void in
            make.height.width.equalTo(100)
            make.top.equalTo(self.view).offset(UIElement().uiViewTopOffset(self))
            make.left.equalTo(self.view).offset((self.view.frame.width / 2) - 50)
        }
        
        name.snp.makeConstraints { (make) -> Void in
            make.top.equalTo(self.userImage.snp.bottom).offset(UIElement().topOffset)
            make.left.equalTo(self.view).offset(UIElement().leftOffset)
            make.right.equalTo(self.view).offset(UIElement().rightOffset)
        }
    }
    
    @objc func didPressSaveButton(_ sender: UIBarButtonItem) {
        let query = PFQuery(className:"_User")
        query.getObjectInBackground(withId: (PFUser.current()?.objectId)!) {
            (object: PFObject?, error: Error?) -> Void in
            if let object = object {
                if let newUserImage = self.newUserImage {
                    object["userImage"] = newUserImage
                }
                
                object["name"] = self.name.text!
                object.saveInBackground {
                    (success: Bool, error: Error?) -> Void in
                    if (success) {
                        UIElement().setDefaultUserDisplayName(self.name.text!)
                        self.goToNext()
                    }
                }
                
            } else {
                UIElement().showAlert("Oops", message: "There was an issue updating your account info. Check your internet connection and try again.", target: self)
            }
        }
    }
    
    @objc func didPressCancelButton(_ sender: UIBarButtonItem) {
        self.goToNext()
    }
    
    //mark: media
    @objc func didPressEditImagebutton(_ sender: UIButton){
        let imagePicker = UIImagePickerController()
        imagePicker.delegate = self
        imagePicker.allowsEditing = false
        
        let alertController = UIAlertController (title: nil, message: nil, preferredStyle: .actionSheet)
        
        let cameraAction = UIAlertAction(title: "Camera", style: .default) { (_) -> Void in
            imagePicker.sourceType = .camera
            self.present(imagePicker, animated: true, completion: nil)
        }
        alertController.addAction(cameraAction)
        
        let photolibraryAction = UIAlertAction(title: "Photo Library", style: .default) { (_) -> Void in
            imagePicker.sourceType = .photoLibrary
            self.present(imagePicker, animated: true, completion: nil)
        }
        alertController.addAction(photolibraryAction)
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel) { (_) -> Void in
        }
        alertController.addAction(cancelAction)
        
        present(alertController, animated: true, completion: nil)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
// Local variable inserted by Swift 4.2 migrator.
let info = convertFromUIImagePickerControllerInfoKeyDictionary(info)

        
        let image = info[convertFromUIImagePickerControllerInfoKey(UIImagePickerController.InfoKey.originalImage)] as! UIImage
        
        self.userImage.setImage(image, for: .normal)
        let proPic = image.jpegData(compressionQuality: 0.5)
        newUserImage = PFFile(name: "defaultProfile_ios.jpeg", data: proPic!)
        newUserImage?.saveInBackground()
        
        dismiss(animated: true, completion: nil)
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        dismiss(animated: true, completion: nil)
    }
    
    //mark: data
    func getLocalUserData() {
        DispatchQueue(label: "background").async {
            autoreleasepool {
                if let userId = PFUser.current()?.objectId {
                    let realm = try! Realm()
                    var realmUser: Results<RealmUser>
                    realmUser = realm.objects(RealmUser.self).filter("userId == '\(userId)'")
                    if realmUser.count != 0 {
                        let user = realmUser[0]
                        let username = user.username
                        let name = user.name
                        let userImage = user.userImageURL
                        DispatchQueue.main.async {
                            self.username = "@\(username!)"
                            self.name.text = name
                            self.userImage.kf.setImage(with: URL(string: userImage!), for: .normal, placeholder: #imageLiteral(resourceName: "person_black"), options: nil, progressBlock: nil, completionHandler: nil)
                        }
                        
                    } else {
                        DispatchQueue.main.async {
                            self.loadUserInfoFromCloud(userId, row: nil)
                        }
                    }
                }
            }
        }
    }
    
    func loadUserInfoFromCloud(_ userId: String, row: Int?) {
        let query = PFQuery(className:"_User")
        query.getObjectInBackground(withId: userId) {
            (user: PFObject?, error: Error?) -> Void in
            if let error = error {
                print(error)
                
            } else if let user = user {
                var name: String!
                var username: String!
                
                if let nameObject = user["name"] as? String {
                    name = nameObject
                    
                } else {
                    name = ""
                }
                
                if let usernameObject = user["username"] as? String {
                    username = usernameObject
                    
                } else {
                    username = "Username"
                }
                
                self.name.text = name
                self.username = "@\(String(describing: username))"
                if let userImageFile = user["userImage"] as? PFFile {
                    self.userImage.kf.setImage(with: URL(string: userImageFile.url!), for: .normal, placeholder: #imageLiteral(resourceName: "person_black"), options: nil, progressBlock: nil, completionHandler: nil)
                }
            }
        }
    }
    
    func goToNext() {
        switch CNContactStore.authorizationStatus(for: .contacts) {
        case .authorized, .denied:
            self.segueToView("Main", identifier: "main")
            break
            
        case .restricted, .notDetermined:
            self.segueToView("Main", identifier: "addFriends")
            break
        }
    }
    
    func segueToView(_ storyboard: String, identifier: String) {
        let storyboard = UIStoryboard(name: storyboard, bundle: nil)
        let initialViewController = storyboard.instantiateViewController(withIdentifier: identifier)
        self.present(initialViewController, animated: true, completion: nil)
    }
}

// Helper function inserted by Swift 4.2 migrator.
fileprivate func convertFromUIImagePickerControllerInfoKeyDictionary(_ input: [UIImagePickerController.InfoKey: Any]) -> [String: Any] {
	return Dictionary(uniqueKeysWithValues: input.map {key, value in (key.rawValue, value)})
}

// Helper function inserted by Swift 4.2 migrator.
fileprivate func convertFromUIImagePickerControllerInfoKey(_ input: UIImagePickerController.InfoKey) -> String {
	return input.rawValue
}
