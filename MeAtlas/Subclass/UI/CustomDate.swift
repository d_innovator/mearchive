//
//  Date.swift
//  MeAtlas
//
//  Created by Dominic  Smith on 6/27/18.
//  Copyright © 2018 Dominic Smith. All rights reserved.
//

import Foundation
class CustomDate {
    
    func formatDate(date: Date) -> String {
        //format date
        let formatter = DateFormatter()
        // initially set the format based on your datepicker date
        if Calendar.current.isDateInToday(date) {
            formatter.dateFormat = "hh:mm a"
            
        } else {
            formatter.dateFormat = "MM/dd/yy, hh:mm a"
        }
        
        formatter.amSymbol = "am"
        formatter.pmSymbol = "pm"
        let myString = formatter.string(from: date)
        
        return myString
    }
}
