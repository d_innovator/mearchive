//
//  Color.swift
//  MeAtlas
//
//  Created by Dominic Smith on 3/20/18.
//  Copyright © 2018 Dominic Smith. All rights reserved.
//

import UIKit

class Color {
    func pastelBlue() -> UIColor {
        return UIColor(red: 218/255, green: 228/255, blue: 237/255, alpha: 1.0)
    }
    
    func meArchiveYellow() -> UIColor {
        return uicolorFromHex(0xffbb00)
    }
    
    func black() -> UIColor {
        return uicolorFromHex(0x292f33)
    }
    
    func white() -> UIColor {
        return uicolorFromHex(0xf4f4f4)
    }
    
    func red() -> UIColor {
        return uicolorFromHex(0xc0392b)
    }
    
    func blue() -> UIColor {
        return uicolorFromHex(0x004e6a)
    }
    
    func green() -> UIColor {
        return uicolorFromHex(0x2ecc71)
    }
    
    func uicolorFromHex(_ rgbValue:UInt32) -> UIColor {
        let red = CGFloat((rgbValue & 0xFF0000) >> 16)/256.0
        let green = CGFloat((rgbValue & 0xFF00) >> 8)/256.0
        let blue = CGFloat(rgbValue & 0xFF)/256.0
        
        return UIColor(red:red, green:green, blue:blue, alpha:1.0)
    }
}
