//
//  PhoneVerificationViewController.swift
//  MeAtlas
//
//  Created by Dominic  Smith on 7/5/18.
//  Copyright © 2018 Dominic Smith. All rights reserved.
//277 531 is +1 000-000-0000 verification code 

import UIKit
import Parse
import SnapKit
import Firebase
import NVActivityIndicatorView

class PhoneVerificationViewController: UIViewController, NVActivityIndicatorViewable {
    
    var phoneNumberString: String!
    
    let uiElement = UIElement()
    
    lazy var phoneNumberLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont(name: "\(uiElement.mainFont)-Bold", size: 25)
        label.text = "What's the code?"
        label.textAlignment = .center
        label.numberOfLines = 0
        label.textColor = uiElement.currentTheme().textColor
        return label
    }()
    
    lazy var phoneNumberDescriptionLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont(name: uiElement.mainFont, size: 20)
        label.text = "Enter the code sent to \(phoneNumberString!)"
        label.textAlignment = .center
        label.numberOfLines = 0
        label.textColor = uiElement.currentTheme().textColor
        return label
    }()
    
    lazy var verificationCodeText: UITextField = {
        let label = UITextField()
        label.placeholder = "Enter code texted to you"
        label.font = UIFont(name: uiElement.mainFont, size: 17)
        label.backgroundColor = .white
        label.borderStyle = .roundedRect
        label.clearButtonMode = .whileEditing
        label.keyboardType = .phonePad
        return label
    }()
    
    lazy var nextButton: UIButton = {
        let button = UIButton()
        button.titleLabel?.font = UIFont(name: uiElement.mainFont, size: 20)
        button.titleLabel?.textAlignment = .right
        button.setTitle("Next", for: .normal)
        button.backgroundColor = Color().meArchiveYellow()
        button.setTitleColor(Color().black(), for: .normal)
        button.layer.cornerRadius = 3
        button.clipsToBounds = true
        return button
    }()

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.view.backgroundColor = UIElement().currentTheme().backgroundColor
        navigationController?.navigationBar.barTintColor = UIElement().currentTheme().backgroundColor
        navigationController?.navigationBar.tintColor = UIElement().currentTheme().textColor
        navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIElement().currentTheme().textColor]

        self.view.addSubview(phoneNumberLabel)
        self.view.addSubview(phoneNumberDescriptionLabel)
        self.view.addSubview(verificationCodeText)
        self.view.addSubview(nextButton)
        nextButton.addTarget(self, action: #selector(didPressNextButton(_:)), for: .touchUpInside)
        
        phoneNumberLabel.snp.makeConstraints { (make) -> Void in
            make.top.equalTo(self.view).offset(UIElement().uiViewTopOffset(self))
            make.left.equalTo(self.view).offset(uiElement.leftOffset)
            make.right.equalTo(self.view).offset(uiElement.rightOffset)
        }
        
        phoneNumberDescriptionLabel.snp.makeConstraints { (make) -> Void in
            make.top.equalTo(self.phoneNumberLabel.snp.bottom).offset(uiElement.elementOffset)
            make.left.equalTo(self.view).offset(uiElement.leftOffset)
            make.right.equalTo(self.view).offset(uiElement.rightOffset)
        }
        
        verificationCodeText.snp.makeConstraints { (make) -> Void in
            make.top.equalTo(self.phoneNumberDescriptionLabel.snp.bottom).offset(uiElement.elementOffset)
            make.left.equalTo(self.view).offset(uiElement.leftOffset)
            make.right.equalTo(self.view).offset(uiElement.rightOffset)
        }
        
        nextButton.snp.makeConstraints { (make) -> Void in
            make.height.equalTo(uiElement.buttonHeight)
            make.top.equalTo(verificationCodeText.snp.bottom).offset(uiElement.elementOffset)
            make.left.equalTo(self.view).offset(uiElement.leftOffset)
            make.right.equalTo(self.view).offset(uiElement.rightOffset)
        }
        
        self.verificationCodeText.becomeFirstResponder()
    }
    
    @objc func didPressNextButton(_ sender: UIButton) {
        self.startAnimating()
        let verificationID = UserDefaults.standard.string(forKey: "authVerificationID")
        let verificationCode = self.verificationCodeText.text
        
        let credential = PhoneAuthProvider.provider().credential(
            withVerificationID: verificationID!,
            verificationCode: verificationCode!)
        
        Auth.auth().signInAndRetrieveData(with: credential) { (authResult, error) in
            
            if let error = error {
                print(error.localizedDescription)
                UIElement().showAlert("Oops", message: error.localizedDescription, target: self)
                self.stopAnimating()
                return
            }
            self.getUsernameFromPhoneNumber(self.phoneNumberString)
        }
    }
    
    func getUsernameFromPhoneNumber(_ phoneNumber: String) {
        let query = PFQuery(className: "_User")
        query.whereKey("phoneNumber", equalTo: phoneNumber)
        query.getFirstObjectInBackground {
            (object: PFObject?, error: Error?) -> Void in
            if error != nil || object == nil {
                UserDefaults.standard.set(phoneNumber, forKey: "phoneNumber")
                UserDefaults.standard.synchronize()
                self.segueToView("newUsername", storyboard: "Login")
                
            } else {
                if let username = object!["username"] as? String {
                    self.loginUser(username: username, password: self.phoneNumberString)
                }
            }
        }
    }
    
    func loginUser(username: String, password: String) {
        PFUser.logInWithUsername(inBackground: username, password: password) {
            (user: PFUser?, error: Error?) -> Void in
            if user != nil {
                //associate current user with device
                let installation = PFInstallation.current()
                installation?["user"] = PFUser.current()
                installation?["userId"] = PFUser.current()?.objectId
                installation?.saveEventually()
                
                self.segueToView("main", storyboard: "Main")
                
            } else {
                self.stopAnimating()
                UIElement().showAlert("Issue logging in.", message: "Check internet connection and try again.", target: self)
            }
        }
    }
    
    func segueToView(_ viewIdentifier: String, storyboard: String) {
        let storyboard = UIStoryboard(name: storyboard, bundle: nil)
        let initialViewController = storyboard.instantiateViewController(withIdentifier: viewIdentifier)
        self.present(initialViewController, animated: true, completion: nil)
    }
}
