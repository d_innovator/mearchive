//
//  CelebrationViewController.swift
//  MeAtlas
//
//  Created by Dominic  Smith on 5/7/18.
//  Copyright © 2018 Dominic Smith. All rights reserved.
//

import UIKit
import SnapKit
import Parse

class CelebrationViewController: UIViewController, WelcomePageViewControllerDelegate {
    
    var welcomePageViewController: WelcomePageViewController? {
        didSet {
            welcomePageViewController?.pageViewDelegate = self
        }
    }
    
    lazy var mearchiveLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont(name: "HelveticaNeue-Bold", size: 30)
        label.text = "MeArchive"
        label.textAlignment = .center
        label.numberOfLines = 0
        label.textColor = Color().black()
        return label
    }()

    lazy var storeInfoLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont(name: "HelveticaNeue-Bold", size: 25)
        label.text = "Your Archive"
        label.textAlignment = .left
        label.numberOfLines = 0
        label.textColor = Color().black()
        return label
    }()
    
    lazy var filingCabinet: UIImageView = {
        let image = UIImageView()
        image.image = #imageLiteral(resourceName: "filingCabinet")
        return image
    }()
    
    lazy var subTitleLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont(name: "HelveticaNeue", size: 20)
        label.text = "Store important notes, todos, reminders photos, website links, passwords, and more."
        label.textAlignment = .left
        label.numberOfLines = 0
        label.textColor = Color().black()
        return label
    }()
    
    lazy var pageIndicator: UIImageView = {
        let image = UIImageView()
        image.image = #imageLiteral(resourceName: "page")
        image.contentMode = .scaleAspectFill
        return image
    }()
    
    lazy var nextButton: UIButton = {
        let button = UIButton()
        button.setTitle("Get Started", for: .normal)
        button.titleLabel?.font = UIFont(name: UIElement().mainFont, size: 17)
        button.setTitleColor(Color().black(), for: .normal)
        button.backgroundColor = Color().meArchiveYellow()
        button.layer.cornerRadius = 3
        button.clipsToBounds = true
        return button
    }()

    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = .white
        self.view.addSubview(mearchiveLabel)
        self.view.addSubview(storeInfoLabel)
        self.view.addSubview(filingCabinet)
        self.view.addSubview(subTitleLabel)
        self.view.addSubview(nextButton)
        self.nextButton.addTarget(self, action: #selector(didPressGetStartedButton(_:)), for: .touchUpInside)
        self.view.addSubview(pageIndicator)
        
        mearchiveLabel.snp.makeConstraints { (make) -> Void in
            make.top.equalTo(self.view).offset(50)
            make.left.equalTo(self.view).offset(UIElement().leftOffset)
            make.right.equalTo(self.view).offset(UIElement().rightOffset)
        }
        
        filingCabinet.snp.makeConstraints { (make) -> Void in
            make.height.equalTo(self.view.frame.height / 2 - 50)
            make.top.equalTo(self.mearchiveLabel.snp.bottom).offset(UIElement().topOffset)
            make.left.equalTo(self.view).offset(UIElement().leftOffset)
            make.right.equalTo(self.view).offset(UIElement().rightOffset)
        }
        
        storeInfoLabel.snp.makeConstraints { (make) -> Void in
            make.top.equalTo(self.filingCabinet.snp.bottom).offset(UIElement().topOffset)
            make.left.equalTo(self.view).offset(UIElement().leftOffset)
            make.right.equalTo(self.view).offset(UIElement().rightOffset)
        }
        
        subTitleLabel.snp.makeConstraints { (make) -> Void in
            make.top.equalTo(self.storeInfoLabel.snp.bottom).offset(UIElement().topOffset)
            make.left.equalTo(self.view).offset(UIElement().leftOffset)
            make.right.equalTo(self.view).offset(UIElement().rightOffset)
        }
        
        nextButton.snp.makeConstraints { (make) -> Void in
            make.height.equalTo(UIElement().buttonHeight)
            make.top.equalTo(self.subTitleLabel.snp.bottom).offset(UIElement().topOffset)
            make.left.equalTo(self.view).offset(UIElement().leftOffset)
            make.right.equalTo(self.view).offset(UIElement().rightOffset)
        }
        
        pageIndicator.snp.makeConstraints { (make) -> Void in
            make.height.equalTo(12)
            make.width.equalTo(24)
            make.top.equalTo(self.nextButton.snp.bottom).offset(UIElement().topOffset)
            make.left.equalTo(self.view.frame.width / 2 - 12)
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let welcomePageViewController = segue.destination as? WelcomePageViewController {
            self.welcomePageViewController = welcomePageViewController
        }
    }
    
    @objc func didPressGetStartedButton(_ sender: UIButton) {
        let storyboard = UIStoryboard(name: "Login", bundle: nil)
        let initialViewController = storyboard.instantiateViewController(withIdentifier: "welcome")
        self.present(initialViewController, animated: true, completion: nil)
    }
    
    func welcomePageViewController(welcomePageviewController: WelcomePageViewController, didUpdatePageCount count: Int) {
        //pageControl.numberOfPages = count
    }
    
    func welcomePageViewController(welcomePageviewController: WelcomePageViewController, didUpdatePageIndex index: Int) {
        //pageControl.currentPage = index
    }

}
