//
//  SettingsTableViewCell.swift
//  MeAtlas
//
//  Created by Dominic  Smith on 4/23/18.
//  Copyright © 2018 Dominic Smith. All rights reserved.
//

import UIKit
import SnapKit

class SettingsTableViewCell: UITableViewCell {
    
    let uiElement = UIElement()
    let color = Color()
    
    //
    lazy var settingsImage: UIImageView = {
        let image = UIImageView()
        return image
    }()
    
    lazy var settingsLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont(name: UIElement().mainFont, size: 17)
        label.textColor = UIElement().currentTheme().textColor
        return label
    }()
    
    lazy var uiSwitch: UISwitch = {
        let uiSwitch = UISwitch()
        return uiSwitch
    }()
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        if reuseIdentifier == "reuse" {
            self.addSubview(settingsImage)
            self.addSubview(settingsLabel)
            
            settingsImage.snp.makeConstraints { (make) -> Void in
                make.height.width.equalTo(25)
                make.top.equalTo(self).offset(uiElement.topOffset)
                make.left.equalTo(self).offset(uiElement.leftOffset)
                make.bottom.equalTo(self).offset(uiElement.bottomOffset)
            }
            
            settingsLabel.snp.makeConstraints { (make) -> Void in
                make.top.equalTo(self).offset(uiElement.topOffset)
                make.left.equalTo(self.settingsImage.snp.right).offset(5)
                make.right.equalTo(self).offset(uiElement.rightOffset)
                make.bottom.equalTo(self).offset(uiElement.bottomOffset)
            }
            
        } else {
            self.addSubview(uiSwitch)
            self.addSubview(settingsLabel)
            
            uiSwitch.snp.makeConstraints { (make) -> Void in
                make.top.equalTo(self).offset(uiElement.topOffset)
                make.left.equalTo(self).offset(uiElement.leftOffset)
                make.bottom.equalTo(self).offset(uiElement.bottomOffset)
            }
            
            settingsLabel.snp.makeConstraints { (make) -> Void in
                make.top.equalTo(self).offset(uiElement.topOffset + 2)
                make.left.equalTo(self.uiSwitch.snp.right).offset(5)
                make.right.equalTo(self).offset(uiElement.rightOffset)
                make.bottom.equalTo(self).offset(uiElement.bottomOffset)
            }
        }
    }

    ///////////
    // We won’t use this but it’s required for the class to compile
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
}
