//
//  Post.swift
//  MeAtlas
//
//  Created by Dominic Smith on 4/2/18.
//  Copyright © 2018 Dominic Smith. All rights reserved.
//

import UIKit
import UserNotifications
import AVFoundation

class Post {
    var objectId: String?
    var text: String
    var date: Date
    var reminder: UNNotificationRequest?
    var todo: Bool?
    var media: UIImage?
    var audio: Data?
    var userId: String
    var username: String?
    var name: String?
    var userImageURL: String?
    var completed: Array<String>?
    var mentions: Array<String>?
    var commentText: Array<String>?
    var commentUserId: Array<String>?
    var commentObjectId: Array<String>?
    var commentDate: Array<String>?
    
    init(objectId: String?, text: String, date: Date, reminder: UNNotificationRequest?, todo: Bool?, media: UIImage?, userId: String, username: String?, name: String?, userImageURL: String?, completed: Array<String>?, mentions: Array<String>?, commentText: Array<String>?, commentUserId: Array<String>?, commentObjectId: Array<String>?, commentDate: Array<String>?, audio: Data?) {

        self.objectId = objectId
        self.text = text
        self.date = date
        self.reminder = reminder
        self.todo = todo
        self.media = media
        self.userId = userId
        self.username = username
        self.name = name
        self.userImageURL = userImageURL
        self.completed = completed
        self.mentions = mentions
        self.commentText = commentText
        self.commentUserId = commentUserId
        self.commentObjectId = commentObjectId
        self.commentDate = commentDate
        self.audio = audio 
    }
}


