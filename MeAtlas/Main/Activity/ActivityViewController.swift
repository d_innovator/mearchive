//
//  ActivityViewController.swift
//  MeAtlas
//
//  Created by Dominic  Smith on 8/21/18.
//  Copyright © 2018 Dominic Smith. All rights reserved.
//

import UIKit
import Kingfisher
import Parse
import SnapKit

class ActivityViewController: UIViewController , UITableViewDelegate, UITableViewDataSource {
    let uiElement = UIElement()
    
    var pendingFriendRequests = [MEAContacts]()
    
    let refreshControl = UIRefreshControl()
        
    var name: String? 
    
    //isFriendshipConfirmed will as a way to determine whether or not user wants to mention person
    var comments = [Comment]()
    var tableView = UITableView()
    
    let activityReuse = "activityReuse"
    let contactsReuse = "contactsReuse"
    let noResultsReuse = "noResultsReuse"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = "Activity"
        
        setUpTableView()
        setUpTabBar()
        
        if let name = UIElement().getCurrentDefaultUserDisplayName() {
            self.name = name
            
        } else {
            self.loadCurrentUserInfo()
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        loadActivity()
        loadPendingFriendRequests()
        
        let installation = PFInstallation.current()
        installation?.badge = 0
        installation?.saveInBackground()
        
        if let tabItems = self.tabBarController?.tabBar.items as NSArray? {
            let tabItem = tabItems[1] as! UITabBarItem
            tabItem.badgeValue = nil
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let indexPath = tableView.indexPathForSelectedRow{
            if let commentsViewController = segue.destination as? CommentsViewController {
                commentsViewController.noteObjectId = self.comments[indexPath.row].postId
            }
        }
    }
        
    func setUpTableView() {
        tableView.delegate = self
        tableView.dataSource = self
        tableView.register(ActivityTableViewCell.self, forCellReuseIdentifier: activityReuse)
        tableView.register(MainTableViewCell.self, forCellReuseIdentifier: contactsReuse)
        tableView.register(MainTableViewCell.self, forCellReuseIdentifier: noResultsReuse)
        tableView.backgroundColor = UIElement().currentTheme().backgroundColor
        tableView.separatorColor = .darkGray
        tableView.separatorStyle = .none
        tableView.frame = view.bounds
        view.addSubview(tableView)
        
        refreshControl.addTarget(self, action: #selector(didPullToRefresh(_:)), for: UIControl.Event.valueChanged)
        tableView.addSubview(refreshControl)
    }
    
    func setUpTabBar() {
        self.view.backgroundColor = UIElement().currentTheme().backgroundColor
        navigationController?.navigationBar.barTintColor = UIElement().currentTheme().backgroundColor
        navigationController?.navigationBar.tintColor = UIElement().currentTheme().textColor
        navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIElement().currentTheme().textColor]
    }
    
    //mark: tableview
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        if section == 0 {
            if pendingFriendRequests.count != 0 {
                return "Friend Requests"
            }
            
            return ""
        }
        
        return "Comments"
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0 {
            return pendingFriendRequests.count
            
        } else {
            if comments.count != 0 {
                return comments.count
                
            } else {
                return 1
            }
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var cell: UITableViewCell!
        
        if indexPath.section == 0 {
            cell = pendingFriendRequestsCell(indexPath)
            
        } else {
            cell = commentsCell(indexPath)
        }
        
        cell.selectionStyle = .none
        cell.backgroundColor = UIElement().currentTheme().backgroundColor
        
        //different color shows behind cell .. getting superview color to match cell
        cell.superview?.backgroundColor = UIElement().currentTheme().backgroundColor
        tableView.separatorStyle = .none
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.section == 1 {
            self.performSegue(withIdentifier: "showComments", sender: self)
        }
    }
    
    func tableView(_ tableView: UITableView, editActionsForRowAt: IndexPath) -> [UITableViewRowAction]? {
        let row = editActionsForRowAt.row
        if editActionsForRowAt.section == 0 {
            let clearAction = UITableViewRowAction(style: .normal, title: "Remove") { action, index in
                self.removeFriendRequest(fromUserId: self.pendingFriendRequests[row].userId!)
                self.pendingFriendRequests.remove(at: row)
                self.tableView.reloadData()
            }
            clearAction.backgroundColor = .red
            return [clearAction]
        }
        return []
    }
    
    func pendingFriendRequestsCell(_ indexPath: IndexPath) -> MainTableViewCell {
        var cell: MainTableViewCell!
        cell = self.tableView.dequeueReusableCell(withIdentifier: contactsReuse) as? MainTableViewCell
        
        var contact: MEAContacts?
        cell.actionButton.removeTarget(self, action: #selector(didPressAcceptbutton(_:)), for: .touchUpInside)
        
        if self.pendingFriendRequests.count == 0 {
            cell = self.tableView.dequeueReusableCell(withIdentifier: noResultsReuse) as? MainTableViewCell
            cell.bodyLabel.text = "All caught up."
            
        } else {
            contact = self.pendingFriendRequests[indexPath.row]
            cell.actionLabel.text = "Accept"
            cell.actionLabel.textColor = UIElement().currentTheme().backgroundColor
            cell.actionButton.addTarget(self, action: #selector(didPressAcceptbutton(_:)), for: .touchUpInside)
            cell.actionButton.backgroundColor = UIElement().currentTheme().textColor
        }
        
        cell.actionButton.tag = indexPath.row
        
        if let contact = contact {
            let personImage = UIElement().currentTheme().personImage
            if let userImageURL = contact.userImageURL {
                cell.userImage.kf.setImage(with: URL(string: userImageURL ), placeholder: personImage, options: nil, progressBlock: nil, completionHandler: nil)
                
            } else {
                cell.userImage.image = personImage
            }
            
            if let username = contact.username {
                cell.username.text = "@\(username)"
                
            } else {
                cell.username.text = contact.phoneNumber
            }
            
            cell.name.text = contact.displayName
        }
        
        cell.selectionStyle = .none
        cell.backgroundColor = UIElement().currentTheme().backgroundColor
        
        //different color shows behind cell .. getting superview color to match cell
        cell.superview?.backgroundColor = UIElement().currentTheme().backgroundColor
        cell.hashtag.textColor = UIElement().currentTheme().hashTagColor
        
        return cell
    }
    
    func commentsCell(_ indexPath: IndexPath) -> UITableViewCell {
        if comments.count == 0 {
            let cell = self.tableView.dequeueReusableCell(withIdentifier: noResultsReuse) as! MainTableViewCell
            cell.bodyLabel.text = "Comments from friends will show up here."
            return cell
            
        } else {
            let cell = self.tableView.dequeueReusableCell(withIdentifier: activityReuse) as! ActivityTableViewCell
            let currentComment = comments[indexPath.row]
            let personImage = uiElement.currentTheme().personImage
            cell.date.text = "\(currentComment.date!)"
            if let username = comments[indexPath.row].username {
                if let userImageURL = currentComment.userImageURL {
                cell.userImage.kf.setImage(with: URL(string: userImageURL ), placeholder: personImage , options: nil, progressBlock: nil, completionHandler: nil)
                    
                } else {
                    cell.userImage.image = personImage
                }
                
               cell.bodyLabel.text = "\(username): \(comments[indexPath.row].text!)"
                
            } else {
                self.loadUserInfoFromCloud(indexPath.row)
            }
            
            return cell
        }
    }
    
    //mark: button actions
    @objc func didPullToRefresh(_ sender: AnyObject) {
        loadActivity()
        loadPendingFriendRequests()
    }
    
    @objc func didPressAcceptbutton(_ sender: UIButton) {
        let fromUserId = self.pendingFriendRequests[sender.tag].userId
        self.pendingFriendRequests.remove(at: sender.tag)
        self.tableView.reloadData()
        
        let query = PFQuery(className: "Friends")
        query.whereKey("fromUserId", equalTo: fromUserId!)
        query.whereKey("toUserId", equalTo: PFUser.current()!.objectId!)
        query.whereKey("isRemoved", equalTo: false)
        query.whereKey("isConfirmed", equalTo: false)
        query.getFirstObjectInBackground {
            (object: PFObject?, error: Error?) -> Void in
            if error != nil || object == nil {
                print("The getFirstObject request failed.")
                
            } else {
                object!["isConfirmed"] = true
                object?.saveEventually {
                    (success: Bool, error: Error?) -> Void in
                    if (success) {
                        if let name = self.name {
                            UIElement().sendAlert("\(name) accepted your friend request.", toUserId: fromUserId!)
                        }
                    }
                }
            }
        }
    }
    
    //mark: data
    func loadActivity() {
        let userId = PFUser.current()!.objectId!
        
        let currentUserCommentsQuery = PFQuery(className: "Comment")
        currentUserCommentsQuery.whereKey("postUserId", equalTo: userId)
        
        let mentionPostQuery = PFQuery(className: "Comment")
        mentionPostQuery.whereKey("mentions", containedIn: [userId])
        
        let query = PFQuery.orQuery(withSubqueries: [currentUserCommentsQuery, mentionPostQuery])
        query.whereKey("isRemoved", equalTo: false)
        query.whereKey("userId", notEqualTo: userId)
        query.whereKey("objectId", notContainedIn: self.comments.map{$0.objectId!})
        query.addAscendingOrder("createdAt")
        query.findObjectsInBackground {
            (objects: [PFObject]?, error: Error?) -> Void in
            if error == nil {
                if let objects = objects {
                    for object in objects {
                        let text = object["text"] as! String
                        let date = CustomDate().formatDate(date: object.createdAt!)
                        let userId = object["userId"] as! String
                        let postId = object["postId"] as! String
                        
                        let newComment = Comment(objectId: object.objectId, text: text, date: date, userId: userId, username: nil, name: nil, userImageURL: nil, postId: postId)
                        
                        self.comments.insert(newComment, at: 0)
                    }
                    
                    self.tableView.reloadData()
                    self.refreshControl.endRefreshing()
                }
                
            } else {
                print("Error: \(error!)")
            }
        }
    }
    
    func loadUserInfoFromCloud(_ row: Int) {
        let comment = comments[row]
        
        let query = PFQuery(className:"_User")
        query.getObjectInBackground(withId: comment.userId) {
            (user: PFObject?, error: Error?) -> Void in
            if let error = error {
                print(error)
                
            } else if let user = user {
                if let nameObject = user["name"] as? String {
                    comment.name = nameObject
                    
                } else {
                    comment.name = "Name"
                }
                
                if let usernameObject = user["username"] as? String {
                    comment.username = usernameObject
                    
                } else {
                    comment.username = "Username"
                }
                
                if let userImageFile = user["userImage"] as? PFFile {
                    comment.userImageURL = userImageFile.url
                }
                
                self.tableView.reloadData()
            }
        }
    }
    
    func loadCurrentUserInfo() {
        let query = PFQuery(className:"_User")
        query.getObjectInBackground(withId: PFUser.current()!.objectId!) {
            (user: PFObject?, error: Error?) -> Void in
            if let error = error {
                print(error)
                
            } else if let user = user {
                var name = ""
                if let displayName = user["name"] as? String {
                    name = displayName
                    
                } else if let username = user["username"] as? String {
                   name = username
                }
                
                self.name = name 
                UIElement().setDefaultUserDisplayName(name)
            }
        }
    }
    
    func loadPendingFriendRequests() {
        var pendingFriendRequestIds = [String]()
        let query = PFQuery(className: "Friends")
        query.whereKey("toUserId", equalTo: PFUser.current()!.objectId!)
        query.whereKey("fromUserId", notContainedIn: self.pendingFriendRequests.map({$0.userId!}))
        query.whereKey("isConfirmed", equalTo: false)
        query.whereKey("isRemoved", equalTo: false)
        query.findObjectsInBackground {
            (objects: [PFObject]?, error: Error?) -> Void in
            if error == nil {
                if let objects = objects {
                    for object in objects {
                        let pendingFriendRequestId = object["fromUserId"] as! String
                        pendingFriendRequestIds.append(pendingFriendRequestId)
                    }
                }
                if pendingFriendRequestIds.count != 0 {
                    self.loadUsersFromCloud(userIds: pendingFriendRequestIds)
                }
                
            } else {
                print("Error: \(error!)")
            }
        }
    }
    
    func loadUsersFromCloud(userIds: Array<String>) {
        let query = PFQuery(className: "_User")
        query.whereKey("objectId", containedIn: userIds)
        query.findObjectsInBackground {
            (objects: [PFObject]?, error: Error?) -> Void in
            if error == nil {
                if let objects = objects {
                    for object in objects {
                        let userObject = MEAContacts(userId: object.objectId, displayName: nil, username: nil, userImageURL: nil, phoneNumber: nil, isFriendshipConfirmed: false, postObjectId: nil)
                        
                        if let name = object["name"] as? String {
                            userObject.displayName = name
                        }
                        
                        if let username = object["username"] as? String {
                            userObject.username = username
                        }
                        
                        if let userImageFile = object["userImage"] as? PFFile {
                            userObject.userImageURL = userImageFile.url!
                        }
                        
                        self.pendingFriendRequests.append(userObject)
                    }
                }
                self.tableView.reloadData()
                
            } else {
                print("Error: \(error!)")
            }
        }
    }
    
    func removeFriendRequest(fromUserId: String) {
        let query = PFQuery(className: "Friends")
        query.whereKey("fromUserId", equalTo:  fromUserId)
        query.whereKey("toUserId", equalTo: PFUser.current()!.objectId! )
        query.whereKey("isRemoved", equalTo: false)
        query.getFirstObjectInBackground {
            (object: PFObject?, error: Error?) -> Void in
            if error != nil || object == nil {
                print("The getFirstObject request failed.")
                
            } else {
                object!["isRemoved"] = true
                object?.saveEventually()
            }
        }
    }
}
