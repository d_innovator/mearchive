//
//  UIElement.swift
//  MeAtlas
//
//  Created by Dominic Smith on 4/2/18.
//  Copyright © 2018 Dominic Smith. All rights reserved.
//

import UIKit
import Foundation
import LocalAuthentication
import Parse
import Alamofire

class UIElement {
    let topOffset = 15
    let leftOffset = 15
    let rightOffset = -15
    let bottomOffset = -15
    let arrowHeightWidth = 10
    let buttonHeight = 50
    let userImageHeightWidth = 40
    let elementOffset = 5
    
    let mainFont = "HelveticaNeue"
    
    func uiViewTopOffset(_ target: UIViewController ) -> CGFloat {
        return UIApplication.shared.statusBarFrame.size.height +
            (target.navigationController?.navigationBar.frame.height ?? 0.0) + CGFloat(topOffset)
    }
    
    func segueToView(_ storyBoard: String, withIdentifier: String, target: UIViewController) {
        let storyboard = UIStoryboard(name: storyBoard, bundle: nil)
        let controller = storyboard.instantiateViewController(withIdentifier: withIdentifier)
        target.present(controller, animated: true, completion: nil)
    }
        
    func showAlert(_ title: String, message: String, target: UIViewController) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Okay", style: .cancel, handler: nil))
        target.present(alert, animated: true, completion: nil)
    }
    
    func permissionDenied(_ title: String, message: String, target: UIViewController) {
        let alertController = UIAlertController (title: title, message: message, preferredStyle: .alert)
        
        let settingsAction = UIAlertAction(title: "Go to Settings", style: .default) { (_) -> Void in
            guard let settingsUrl = URL(string: UIApplication.openSettingsURLString) else {
                return
            }
            
            if UIApplication.shared.canOpenURL(settingsUrl) {
                UIApplication.shared.open(settingsUrl, completionHandler: nil)
            }
        }
        alertController.addAction(settingsAction)
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
        alertController.addAction(cancelAction)
        
        target.present(alertController, animated: true, completion: nil)
    }
    
    func sendAlert(_ message: String, toUserId: String) {
        Alamofire.request("https://me-archive.herokuapp.com/notifications/alertUser", method: .post, parameters: ["message": message, "userId": toUserId], encoding: JSONEncoding.default).validate().response{response in
        }
    }
    
    func setDefaultUserDisplayName(_ name: String) {
        UserDefaults.standard.set(name, forKey: "currentUserDisplayName")
        UserDefaults.standard.synchronize()
    }
    
    func getCurrentDefaultUserDisplayName() -> String? {
        if let name = UserDefaults.standard.string(forKey: "currentUserDisplayName") {
            return name
        }
        
        return nil
    }
    
    enum Theme: Int {
        
        case lightTheme, darkTheme
        
        var backgroundColor: UIColor {
            switch self {
            case .darkTheme:
                return Color().black()
                
            case .lightTheme:
                //return Color().white()
                return .white
            }
        }
        
        var textColor: UIColor {
            switch self {
            case .darkTheme:
                return UIColor.white
                
            case .lightTheme:
                return Color().uicolorFromHex(0x5C5C64)
            }
        }
        
        var hashTagColor: UIColor {
            switch self {
            case .darkTheme:
                return UIColor.lightGray
                
            case .lightTheme:
                return Color().uicolorFromHex(0x5C7C94)
            }
        }
        
        var exitBoxTint: UIColor {
            switch self {
            case .darkTheme:
                return .white
                
            case .lightTheme:
                return Color().black()
            }
        }
        
        var switchText: String {
            switch self {
            case .darkTheme:
                return "Dark"
                
            case .lightTheme:
                return "Light"
            }
        }
        
        var audioFileImage: UIImage {
            switch self {
            case .darkTheme:
                return #imageLiteral(resourceName: "audioFile_white")
                
            case .lightTheme:
                return #imageLiteral(resourceName: "audioFile_black")
            }
        }
        
        var pauseImage: UIImage {
            switch self {
            case .darkTheme:
                return #imageLiteral(resourceName: "pause_white")
                
            case .lightTheme:
                return #imageLiteral(resourceName: "pause_black")
            }
        }
        
        var playImage: UIImage {
            switch self {
            case .darkTheme:
                return #imageLiteral(resourceName: "play_white")
                
            case .lightTheme:
                return #imageLiteral(resourceName: "play_black")
            }
        }
        
        var microphoneImage: UIImage {
            switch self {
            case .darkTheme:
                return #imageLiteral(resourceName: "microphone_white")
                
            case .lightTheme:
                return #imageLiteral(resourceName: "microphone_black")
            }
        }
        
        var checkBoxImage: UIImage {
            switch self {
            case .darkTheme:
                return #imageLiteral(resourceName: "checkbox_white")
                
            case .lightTheme:
                return #imageLiteral(resourceName: "checkbox_black")
            }
        }
        
        var checkImage: UIImage {
            switch self {
            case .darkTheme:
                return #imageLiteral(resourceName: "check_white")
                
            case .lightTheme:
                return #imageLiteral(resourceName: "check_black")
            }
        }
        
        var menuImage: UIImage {
            switch self {
            case .darkTheme:
                return #imageLiteral(resourceName: "menu-white")
                
            case .lightTheme:
                return #imageLiteral(resourceName: "menu-black")
            }
        }
        
        var settingImage: UIImage {
            switch self {
            case .darkTheme:
                return #imageLiteral(resourceName: "setting-white")
                
            case .lightTheme:
                return #imageLiteral(resourceName: "setting-black")
            }
        }
        
        var shieldImage: UIImage {
            switch self {
            case .darkTheme:
                return #imageLiteral(resourceName: "shield-white")
                
            case .lightTheme:
                return #imageLiteral(resourceName: "shield-black")
            }
        }
        
        var powerImage: UIImage {
            switch self {
            case .darkTheme:
                return #imageLiteral(resourceName: "power-white")
                
            case .lightTheme:
                return #imageLiteral(resourceName: "power-black")
            }
        }
        
        var pictureImage: UIImage {
            switch self {
            case .darkTheme:
                return #imageLiteral(resourceName: "picture_white")
                
            case .lightTheme:
                return #imageLiteral(resourceName: "picture_black")
            }
        }
        
        var targetImage: UIImage {
            switch self {
            case .darkTheme:
                return #imageLiteral(resourceName: "target_white")
                
            case .lightTheme:
                return #imageLiteral(resourceName: "target_black")
            }
        }
        
        var personImage: UIImage {
            switch self {
            case .darkTheme:
                return #imageLiteral(resourceName: "person_white")
                
            case .lightTheme:
                return #imageLiteral(resourceName: "person_black")
            }
        }
        
        var atImage: UIImage {
            switch self {
            case .darkTheme:
                return #imageLiteral(resourceName: "at_white")
                
            case .lightTheme:
                return #imageLiteral(resourceName: "at_black")
            }
        }
        
        var dataImage: UIImage {
            switch self {
            case .darkTheme:
                return #imageLiteral(resourceName: "data_white")
                
            case .lightTheme:
                return #imageLiteral(resourceName: "data_black")
            }
        }
        
        var cameraImage: UIImage {
            switch self {
            case .darkTheme:
                return #imageLiteral(resourceName: "camera_white")
                
            case .lightTheme:
                return #imageLiteral(resourceName: "camera_black")
            }
        }
        
        var speechImage: UIImage {
            switch self {
            case .darkTheme:
                return #imageLiteral(resourceName: "speech_white")
                
            case .lightTheme:
                return #imageLiteral(resourceName: "speech_black")
            }
        }
    }
    
    let SelectedThemeKey = "SelectedTheme"
    
    func currentTheme() -> Theme {
        if let storedTheme = (UserDefaults.standard.value(forKey: SelectedThemeKey) as AnyObject).integerValue {
            return Theme(rawValue: storedTheme)!
            
        } else {
            return .lightTheme
        }
    }
    
    func applyTheme(_ theme: Theme) {
        // First persist the selected theme using NSUserDefaults.
        UserDefaults.standard.setValue(theme.rawValue, forKey: SelectedThemeKey)
        UserDefaults.standard.synchronize()
    }
}
