//
//  CommentsTableViewCell.swift
//  MeAtlas
//
//  Created by Dominic  Smith on 7/23/18.
//  Copyright © 2018 Dominic Smith. All rights reserved.
//

import UIKit
import SnapKit
import ActiveLabel

class CommentsTableViewCell: UITableViewCell {
    
    let uiElement = UIElement()
    let color = Color()
    
    let commentsReuse = "commentsReuse"
    
    lazy var bodyLabel: ActiveLabel = {
        let label = ActiveLabel()
        label.font = UIFont(name: uiElement.mainFont, size: 17)
        label.enabledTypes = [.url, .mention]
        label.textColor = uiElement.currentTheme().textColor
        label.numberOfLines = 0
        label.URLColor = uiElement.currentTheme().hashTagColor
        label.mentionColor = uiElement.currentTheme().hashTagColor
        label.handleURLTap { url in  UIApplication.shared.open(url, options: convertToUIApplicationOpenExternalURLOptionsKeyDictionary([:]), completionHandler: nil) }
        return label
    }()

    lazy var userImage: UIImageView = {
        let image = UIImageView()
        image.image = #imageLiteral(resourceName: "person_black")
        image.layer.cornerRadius = CGFloat(uiElement.userImageHeightWidth / 2)
        image.clipsToBounds = true
        image.contentMode = .scaleAspectFill
        image.image = uiElement.currentTheme().personImage
        return image
    }()
    
    lazy var name: UILabel = {
        let label = UILabel()
        label.font = UIFont(name: uiElement.mainFont, size: 16)
        label.textColor = uiElement.currentTheme().textColor
        label.text = "Name"
        return label
    }()
    
    lazy var username: UILabel = {
        let label = UILabel()
        label.font = UIFont(name: uiElement.mainFont, size: 13)
        label.textColor = UIColor.darkGray
        label.text = "@username"
        return label
    }()
    
    lazy var replyButton: UIButton = {
        let button = UIButton()
        button.titleLabel?.font = UIFont(name: uiElement.mainFont, size: 13)
        button.setTitleColor(UIColor.darkGray, for: .normal)
        button.setTitle("Reply", for: .normal)
        return button
    }()
    
    lazy var date: UILabel = {
        let label = UILabel()
        label.font = UIFont(name: uiElement.mainFont, size: 13)
        label.textColor = UIColor.darkGray
        return label
    }()
    
    lazy var menuButton: UIButton = {
        let button = UIButton()
        button.setImage(uiElement.currentTheme().menuImage, for: .normal)
        return button
    }()
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        if reuseIdentifier == commentsReuse {
            self.addSubview(date)
            self.addSubview(userImage)
            self.addSubview(username)
            self.addSubview(name)
            self.addSubview(bodyLabel)
            self.addSubview(replyButton)
            self.addSubview(menuButton)
            
            menuButton.snp.makeConstraints { (make) -> Void in
                make.height.width.equalTo(25)
                make.top.equalTo(self).offset(uiElement.topOffset)
                make.right.equalTo(self).offset(uiElement.rightOffset)
            }
            
            userImage.snp.makeConstraints { (make) -> Void in
                make.width.height.equalTo(uiElement.userImageHeightWidth)
                make.top.equalTo(self).offset(uiElement.topOffset)
                make.left.equalTo(self).offset(uiElement.leftOffset)
            }
            
            name.snp.makeConstraints { (make) -> Void in
                make.top.equalTo(userImage).offset(uiElement.elementOffset)
                make.left.equalTo(userImage.snp.right).offset(uiElement.elementOffset)
            }
            
            username.snp.makeConstraints { (make) -> Void in
                make.top.equalTo(name).offset(1)
                make.left.equalTo(name.snp.right).offset(1)
            }
            
            date.snp.makeConstraints { (make) -> Void in
                make.top.equalTo(name).offset(2)
                make.left.equalTo(username.snp.right)
                //make.right.equalTo(self).offset(uiElement.rightOffset)
            }
            
            bodyLabel.snp.makeConstraints { (make) -> Void in
                make.top.equalTo(name.snp.bottom).offset(uiElement.elementOffset)
                make.left.equalTo(userImage.snp.right).offset(uiElement.elementOffset)
                make.right.equalTo(self).offset(uiElement.rightOffset)
            }
            
            replyButton.snp.makeConstraints { (make) -> Void in
                make.top.equalTo(bodyLabel.snp.bottom).offset(uiElement.elementOffset)
                make.left.equalTo(bodyLabel)
                make.bottom.equalTo(self).offset(uiElement.bottomOffset)
            }
        }
    }

    ///////////
    // We won’t use this but it’s required for the class to compile
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
}

// Helper function inserted by Swift 4.2 migrator.
fileprivate func convertToUIApplicationOpenExternalURLOptionsKeyDictionary(_ input: [String: Any]) -> [UIApplication.OpenExternalURLOptionsKey: Any] {
	return Dictionary(uniqueKeysWithValues: input.map { key, value in (UIApplication.OpenExternalURLOptionsKey(rawValue: key), value)})
}
