//
//  WelcomePlayerViewController.swift
//  MeAtlas
//
//  Created by Dominic  Smith on 6/19/18.
//  Copyright © 2018 Dominic Smith. All rights reserved.
//mark: video, UI

import UIKit
import AVFoundation
import AVKit
import SnapKit

class WelcomePlayerViewController: AVPlayerViewController {
    
    override func viewDidAppear(_ animated: Bool) {
        setUpUI()
        setUpVideo()
    }
    
    //mark: UI
    func setUpUI() {
        self.view.addSubview(self.appName)
        self.view.addSubview(self.getStartedbutton)
        self.getStartedbutton.addTarget(self, action: #selector(self.didPressGetStartedButton(_:)), for: .touchUpInside)
        
        self.appName.snp.makeConstraints { (make) -> Void in
            make.top.equalTo(self.view).offset(50)
            make.left.equalTo(self.view).offset(UIElement().leftOffset)
            make.right.equalTo(self.view).offset(UIElement().rightOffset)
        }
        
        self.getStartedbutton.snp.makeConstraints { (make) -> Void in
            make.height.equalTo(50)
            make.left.equalTo(self.view).offset(UIElement().leftOffset)
            make.right.equalTo(self.view).offset(UIElement().rightOffset)
            make.bottom.equalTo(self.view).offset(UIElement().bottomOffset)
        }
    }
    
    @objc func didPressGetStartedButton(_ sender: UIButton) {
        self.player?.pause()
        let storyboard = UIStoryboard(name: "Login", bundle: nil)
        let initialViewController = storyboard.instantiateViewController(withIdentifier: "phoneNumberAuth")
        self.present(initialViewController, animated: true, completion: nil)
    }
    
    lazy var appName: UILabel = {
        let label = UILabel()
        label.font = UIFont(name: "\(UIElement().mainFont)-Bold", size: 40)
        label.text = "MeArchive"
        label.textAlignment = .center
        label.numberOfLines = 0
        label.textColor = UIElement().currentTheme().textColor
        return label
    }()
    
    lazy var getStartedbutton: UIButton = {
        let button = UIButton()
        button.setTitle("Get Started", for: .normal)
        button.titleLabel?.font = UIFont(name: UIElement().mainFont, size: 17)
        button.setTitleColor(Color().black(), for: .normal)
        button.backgroundColor = Color().meArchiveYellow()
        button.layer.cornerRadius = 3
        button.clipsToBounds = true
        return button
    }()
    
    //mark: Video
    func setUpVideo() {
        let videoFile = getVideoFile()
        self.player = AVPlayer(url: URL(fileURLWithPath: videoFile))
        self.showsPlaybackControls = false
        
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(playerItemDidReachEnd(_:)),
                                               name: NSNotification.Name.AVPlayerItemDidPlayToEndTime,
                                               object: self.player?.currentItem)
        
        do {
            try AVAudioSession.sharedInstance().setCategory(AVAudioSessionCategoryPlayback)
            self.player?.play()
        }
        catch {
            self.player?.play()
        }        
    }
    
    func getVideoFile() -> String {
        guard let path = Bundle.main.path(forResource: "intro", ofType:"mov") else {
            debugPrint("video.m4v not found")
            return "error: video not found"
        }
        
        return path
    }
    
    @objc func playerItemDidReachEnd(_ notification: NSNotification) {
        self.player?.seek(to: kCMTimeZero)
        self.player?.play()
    }
}
