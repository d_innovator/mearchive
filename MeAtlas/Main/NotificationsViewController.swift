//
//  NotificationsViewController.swift
//  MeAtlas
//
//  Created by Dominic  Smith on 8/27/18.
//  Copyright © 2018 Dominic Smith. All rights reserved.
//

import UIKit
import SnapKit

class NotificationsViewController: UIViewController {
    
    let uiElement = UIElement()
    
    lazy var notificationTitleLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont(name: "\(uiElement.mainFont)-bold", size: 30)
        label.text = "Notifications"
        label.textAlignment = .center
        label.numberOfLines = 0
        label.textColor = uiElement.currentTheme().textColor
        return label
    }()
    
    lazy var notificationDescriptionLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont(name: uiElement.mainFont, size: 17)
        label.text = "Get notified of reminders you set for yourself and mentions from friends."
        label.textAlignment = .left
        label.numberOfLines = 0
        label.textColor = uiElement.currentTheme().textColor
        return label
    }()
    
    lazy var nextButton: UIButton = {
        let button = UIButton()
        button.titleLabel?.font = UIFont(name: uiElement.mainFont, size: 20)
        button.setTitle("Get Notified", for: .normal)
        button.backgroundColor = Color().meArchiveYellow()
        button.setTitleColor(Color().black(), for: .normal)
        button.layer.cornerRadius = 3
        button.clipsToBounds = true
        return button
    }()
    
    lazy var skipButton: UIButton = {
        let button = UIButton()
        button.titleLabel?.font = UIFont(name: uiElement.mainFont, size: 17)
        button.setTitle("Skip", for: .normal)
        button.setTitleColor(uiElement.currentTheme().textColor, for: .normal)
        return button
    }()

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.view.backgroundColor = UIElement().currentTheme().backgroundColor
        navigationController?.navigationBar.barTintColor = UIElement().currentTheme().backgroundColor
        navigationController?.navigationBar.tintColor = UIElement().currentTheme().textColor
        navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIElement().currentTheme().textColor]
        
        self.view.addSubview(notificationTitleLabel)
        self.view.addSubview(notificationDescriptionLabel)
        
        nextButton.addTarget(self, action: #selector(didPressNextButton(_:)), for: .touchUpInside)
        self.view.addSubview(nextButton)
        
        skipButton.addTarget(self, action: #selector(didPressSkipButton(_:)), for: .touchUpInside)
        self.view.addSubview(skipButton)

        notificationTitleLabel.snp.makeConstraints { (make) -> Void in
            make.top.equalTo(self.view).offset(uiElement.uiViewTopOffset(self))
            make.left.equalTo(self.view).offset(uiElement.leftOffset)
            make.right.equalTo(self.view).offset(uiElement.rightOffset)
        }
        
        notificationDescriptionLabel.snp.makeConstraints { (make) -> Void in
            make.top.equalTo(self.notificationTitleLabel.snp.bottom).offset(uiElement.topOffset)
            make.left.equalTo(self.view).offset(uiElement.leftOffset)
            make.right.equalTo(self.view).offset(uiElement.rightOffset)
        }
        
        nextButton.snp.makeConstraints { (make) -> Void in
            make.height.equalTo(uiElement.buttonHeight)
            make.left.equalTo(self.view).offset(uiElement.leftOffset)
            make.right.equalTo(self.view).offset(uiElement.rightOffset)
            make.bottom.equalTo(self.skipButton.snp.top).offset(uiElement.bottomOffset)
        }
        
        skipButton.snp.makeConstraints { (make) -> Void in
            make.height.equalTo(uiElement.buttonHeight)
            make.left.equalTo(self.view).offset(uiElement.leftOffset)
            make.right.equalTo(self.view).offset(uiElement.rightOffset)
            make.bottom.equalTo(self.view).offset(uiElement.bottomOffset)
        }
    }
    
    @objc func didPressNextButton(_ sender: UIButton) {
        Reminder().reminderCenter.requestAuthorization(options: [.alert, .sound, .badge]) {
            (granted, error) in
            DispatchQueue.main.async {
                UIApplication.shared.registerForRemoteNotifications()
                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                let initialViewController = storyboard.instantiateViewController(withIdentifier: "editProfile")
                self.present(initialViewController, animated: true, completion: nil)
            }
        }
    }
    
    @objc func didPressSkipButton(_ sender: UIButton) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let initialViewController = storyboard.instantiateViewController(withIdentifier: "editProfile")
        self.present(initialViewController, animated: true, completion: nil)
    }
}
