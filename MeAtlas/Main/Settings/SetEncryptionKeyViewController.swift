//
//  SetEncryptionKeyViewController.swift
//  MeAtlas
//
//  Created by Dominic  Smith on 5/21/18.
//  Copyright © 2018 Dominic Smith. All rights reserved.
//

import UIKit
import SnapKit

class SetEncryptionKeyViewController: UIViewController {
    lazy var titleLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont(name: "\(UIElement().mainFont)-Bold", size: 20 )
        label.text = "Set Encryption Key"
        label.textColor = UIElement().currentTheme().textColor
        label.numberOfLines = 0
        return label
    }()
    
    lazy var encryptionKeyInput: UITextField = {
        let label = UITextField()
        label.placeholder = "xxx-xxx-xxx-xxx"
        label.font = UIFont(name: UIElement().mainFont, size: 17)
        label.backgroundColor = .white
        label.borderStyle = .roundedRect
        label.clearButtonMode = .whileEditing
        label.keyboardType = UIKeyboardType.alphabet
        return label
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.view.backgroundColor = UIElement().currentTheme().backgroundColor
        navigationController?.navigationBar.barTintColor = UIElement().currentTheme().backgroundColor
        navigationController?.navigationBar.tintColor = UIElement().currentTheme().textColor
        navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIElement().currentTheme().textColor]
        
        let doneButton = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(doneAction(_:)))
        self.navigationItem.rightBarButtonItem = doneButton
        
        self.view.addSubview(titleLabel)
        self.view.addSubview(encryptionKeyInput)
        
        titleLabel.snp.makeConstraints { (make) -> Void in
            make.top.equalTo(self.view).offset(UIElement().uiViewTopOffset(self))
            make.left.equalTo(self.view).offset(UIElement().leftOffset)
            make.right.equalTo(self.view).offset(UIElement().rightOffset)
        }
        
        encryptionKeyInput.snp.makeConstraints { (make) -> Void in
            make.top.equalTo(titleLabel.snp.bottom).offset(10)
            make.left.equalTo(self.view).offset(UIElement().leftOffset)
            make.right.equalTo(self.view).offset(UIElement().rightOffset)
        }
        encryptionKeyInput.becomeFirstResponder()        
    }
    
    @objc func doneAction(_ sender: UIBarButtonItem) {
        let encryptionKey = encryptionKeyInput.text?.trimmingCharacters(in: .whitespaces)
        if validateEncryptionKey(encryptionKey!) && Secure().setKey(encryptionKey!) {
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let initialViewController = storyboard.instantiateViewController(withIdentifier: "main")
            self.present(initialViewController, animated: true, completion: nil)
        }
    }
    
    func validateEncryptionKey(_ key: String) -> Bool {
        var isValidated = true
        
        let keyArray = key.split{$0 == "-"}.map(String.init)
        if encryptionKeyInput.text!.isEmpty {
            encryptionKeyInput.attributedPlaceholder = NSAttributedString(string:"Field required",
                                                                          attributes:[NSAttributedString.Key.foregroundColor: UIColor.red])
            isValidated = false
            
        } else if key.count != 15 || keyArray.count != 4 {
            UIElement().showAlert("Incorrect Format", message: "Encryption Key format must be: \n xxx-xxx-xxx-xxx", target: self)
            isValidated = false
        }
        
        return isValidated
    }
}
