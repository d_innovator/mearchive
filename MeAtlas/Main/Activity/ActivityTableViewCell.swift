//
//  ActivityTableViewCell.swift
//  MeAtlas
//
//  Created by Dominic  Smith on 8/21/18.
//  Copyright © 2018 Dominic Smith. All rights reserved.
//

import UIKit
import SnapKit

class ActivityTableViewCell: UITableViewCell {

    let uiElement = UIElement()
    
    //
    lazy var bodyLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont(name: uiElement.mainFont, size: 17)
        label.textColor = uiElement.currentTheme().textColor
        label.text = "Name"
        label.textAlignment = .left 
        label.numberOfLines = 2
        return label
    }()
    
    lazy var userImage: UIImageView = {
        let image = UIImageView()
        image.image = #imageLiteral(resourceName: "person_black")
        image.layer.cornerRadius = CGFloat(uiElement.userImageHeightWidth / 2)
        image.clipsToBounds = true
        image.contentMode = .scaleAspectFill
        image.image = uiElement.currentTheme().personImage
        return image
    }()
    
    lazy var name: UILabel = {
        let label = UILabel()
        label.font = UIFont(name: "\(uiElement.mainFont)-bold", size: 17)
        label.textColor = uiElement.currentTheme().textColor
        label.text = "Name"
        return label
    }()
    
    lazy var date: UILabel = {
        let label = UILabel()
        label.font = UIFont(name: uiElement.mainFont, size: 13)
        label.textColor = UIColor.darkGray
        return label
    }()
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        self.addSubview(date)
        self.addSubview(userImage)
        self.addSubview(name)
        self.addSubview(bodyLabel)
        
        userImage.snp.makeConstraints { (make) -> Void in
            make.width.height.equalTo(uiElement.userImageHeightWidth)
            make.top.equalTo(self).offset(uiElement.topOffset)
            make.left.equalTo(self).offset(uiElement.leftOffset)
        }
        
        date.snp.makeConstraints { (make) -> Void in
            make.top.equalTo(userImage)
            make.left.equalTo(userImage.snp.right).offset(uiElement.elementOffset)
        }
        
        bodyLabel.snp.makeConstraints { (make) -> Void in
            make.top.equalTo(date.snp.bottom).offset(2)
            make.left.equalTo(date)
            make.right.equalTo(self).offset(uiElement.rightOffset)
            make.bottom.equalTo(self).offset(uiElement.bottomOffset)
        }
    }
    
    ///////////
    // We won’t use this but it’s required for the class to compile
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }

}
