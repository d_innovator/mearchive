//
//  Comment.swift
//  MeAtlas
//
//  Created by Dominic  Smith on 7/23/18.
//  Copyright © 2018 Dominic Smith. All rights reserved.
//

import UIKit

class Comment {
    var objectId: String?
    var userId: String!
    var text: String!
    var date: String!
    var username: String?
    var name: String?
    var userImageURL: String?
    var postId: String!
    
    init(objectId: String?, text: String!, date: String!, userId: String!, username: String?, name: String?, userImageURL: String?, postId: String!) {
        self.objectId = objectId
        self.text = text
        self.date = date
        self.userId = userId
        self.username = username
        self.name = name
        self.userImageURL = userImageURL
        self.postId = postId
    }
}
