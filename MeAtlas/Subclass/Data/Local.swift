//
//  Local.swift
//  MeAtlas
//
//  Created by Dominic  Smith on 6/27/18.
//  Copyright © 2018 Dominic Smith. All rights reserved.
//

import Foundation
import RealmSwift

class Local {
    func update(_ localObjectId: String, newObjectId: String?, post: Post?) {
        DispatchQueue(label: "background").async {
            autoreleasepool {
                let realm = try! Realm()
                var realmPost: Results<RealmPost>
                realmPost = realm.objects(RealmPost.self).filter("objectId == '\(localObjectId)'")
                for object in realmPost {
                    if object.objectId == localObjectId {
                        try! realm.write {
                            if let newObjectId = newObjectId {
                                object.objectId = newObjectId
                                object.isSavedToCloud = true
                                
                            } else {
                                object.text = post?.text
                                
                                if let todo = post?.todo {
                                    object.todo = todo
                                }
                                
                                if let media = post?.media {
                                    let mediaData = media.jpegData(compressionQuality: 0.5)
                                    object.media = mediaData
                                }
                            }
                        }
                    }
                }
            }
        }
    }
    
    func deleteAll() {
        let realm = try! Realm()
        try! realm.write {
            realm.deleteAll()
        }
    }
    
    func deleteObject(_ objectId: String) {
        DispatchQueue(label: "background").async {
            autoreleasepool {
                let realm = try! Realm()
                var realmPost: Results<RealmPost>
                realmPost = realm.objects(RealmPost.self).filter("objectId == '\(objectId)'")
                for object in realmPost {
                    if object.objectId == objectId {
                        try! realm.write {
                            realm.delete(object)
                        }
                    }
                }
            }
        }
    }
    
    
}
