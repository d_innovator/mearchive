//
//  AddFriendsViewController.swift
//  MeAtlas
//
//  Created by Dominic  Smith on 7/30/18.
//  Copyright © 2018 Dominic Smith. All rights reserved.
//mark: tableview, contacts, button actions data, search bar

import UIKit
import Parse
import Kingfisher
import Contacts
import MessageUI
import SnapKit

class AddFriendsViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, MFMessageComposeViewControllerDelegate, UISearchBarDelegate {
    
    var name: String?
    
    let uiElement = UIElement()
    let color = Color()
    
    let noResultsReuse = "noResultsReuse"
    var showContacts = false
    var hasGivenAccessToContacts = false
    
    let countryCodes = ["us":"1", "au": "61", "be":"32", "br":"55", "ca":"1", "cn":"86", "dk":"45", "ec":"593", "eg":"20", "fi":"358","fr":"33", "de":"49", "gh":"233", "gr":"30", "gl":"299", "hk":"852", "hu":"36", "is":"354", "in":"91", "ie":"353", "il":"972", "it":"39", "jp":"81", "ke":"254", "lb":"961", "lu":"352", "mx":"52", "mc":"377", "ma":"212", "nl":"31", "nz":"64", "ni":"505", "ng":"234", "no":"47", "pk":"92", "ps":"970", "py":"595", "pe":"51", "ph":"63", "pl":"48", "pt":"351", "pr":"1", "ro":"40", "ru":"7", "rw":"250", "za":"27", "kr":"82", "es":"34", "se":"46", "ch":"41", "tw":"886", "th":"66", "ae":"971", "ug":"256", "gb":"44", "ua":"380"]
    
    override func viewDidLoad() {
        super.viewDidLoad()
    
        setUpTabBar()
        setUpTableView()
        
        self.checkContactsAuthorization()
        
        if let name = self.uiElement.getCurrentDefaultUserDisplayName() {
            self.name = name
            
        } else {
            self.loadCurrentUserInfo()
        }
    }
    
    func setUpTableView() {
        tableView.delegate = self
        tableView.dataSource = self
        self.tableView.register(MainTableViewCell.self, forCellReuseIdentifier: contactsReuse)
        self.tableView.register(MainTableViewCell.self, forCellReuseIdentifier: noResultsReuse)
        tableView.backgroundColor = UIElement().currentTheme().backgroundColor
        tableView.separatorColor = .darkGray
        tableView.keyboardDismissMode = .onDrag
        self.tableView.separatorStyle = .none
        self.tableView.frame = view.bounds
        self.view.addSubview(tableView)
    }
    
    func setUpTabBar() {
        self.view.backgroundColor = UIElement().currentTheme().backgroundColor
        navigationController?.navigationBar.barTintColor = UIElement().currentTheme().backgroundColor
        navigationController?.navigationBar.tintColor = UIElement().currentTheme().textColor
        navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIElement().currentTheme().textColor]
    }
    
    //mark: tableview
    let tableView = UITableView()
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        if section == 0 {
            return "Contacts on MeArchive"
            
        } else {
            return "Invite Contacts"
        }
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
         if section == 0 && filteredContactsOnMeArchive.count != 0 {
            return self.filteredContactsOnMeArchive.count
            
        } else if section == 1 && filteredContactsOnPhone.count != 0 {
            return self.filteredContactsOnPhone.count
        }
        
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        return contactsCell(indexPath)
    }
    
    //mark: search bar
    lazy var searchBar: UISearchBar = {
        let searchBar = UISearchBar()
        searchBar.frame = CGRect(x: 0, y: 0, width: self.view.frame.width - 100, height: 20)
        if uiElement.currentTheme() == .darkTheme {
            searchBar.barTintColor = uiElement.currentTheme().backgroundColor
            
        } else {
            searchBar.barTintColor = .darkGray
        }
        
        return searchBar
    }()
    
    //mark: searchbar
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        let wordPredicate = NSPredicate(format: "self BEGINSWITH[c] %@", searchText)
        if searchText.count == 0 {
            self.filteredContactsOnMeArchive = self.contactsOnMeArchive
            self.filteredContactsOnPhone = self.contactsOnPhone
            
        } else {
            //filter users on MeArchive that are in current user's phone
            var meArchiveFilteredContacts = [MEAContacts]()
            meArchiveFilteredContacts = self.contactsOnMeArchive.filter {
                wordPredicate.evaluate(with: $0.username) ||
                    wordPredicate.evaluate(with: $0.displayName)
            }
            
            meArchiveFilteredContacts.sort(by: {$0.displayName! < $1.displayName!})
            self.filteredContactsOnMeArchive = meArchiveFilteredContacts
            
            
            //filter non-MeArchive users that are in current user's phone
            var phoneFilteredContacts = [MEAContacts]()
            phoneFilteredContacts = self.contactsOnPhone.filter { wordPredicate.evaluate(with: $0.displayName) }
            phoneFilteredContacts.sort(by: {$0.displayName! < $1.displayName!})
            self.filteredContactsOnPhone = phoneFilteredContacts
        }
        
        self.tableView.reloadData()
    }
    
    //mark: contacts
    let contactsReuse = "contactsReuse"
    
    var contactsOnMeArchive = [MEAContacts]()
    var filteredContactsOnMeArchive = [MEAContacts]()
    
    var contactsOnPhone = [MEAContacts]()
    var filteredContactsOnPhone = [MEAContacts]()
    
    func contactsCell(_ indexPath: IndexPath) -> MainTableViewCell {
        var cell: MainTableViewCell!
        cell = self.tableView.dequeueReusableCell(withIdentifier: contactsReuse) as? MainTableViewCell
        
        var contact: MEAContacts?
        cell.actionButton.removeTarget(self, action: #selector(didPressAddButton(_:)), for: .touchUpInside)
        cell.actionButton.removeTarget(self, action: #selector(didPressInviteButton(_:)), for: .touchUpInside)
        
        if indexPath.section == 0 {
            if self.filteredContactsOnMeArchive.count == 0 {
                cell = self.tableView.dequeueReusableCell(withIdentifier: noResultsReuse) as? MainTableViewCell
                cell.bodyLabel.text = "Nothing here yet, invite friends."
                
            } else {
                contact = self.filteredContactsOnMeArchive[indexPath.row]
                if let isConfirmed = self.filteredContactsOnMeArchive[indexPath.row].isFriendshipConfirmed {
                    if isConfirmed {
                        cell.actionLabel.text = "Friends"
                        cell.actionButton.backgroundColor = color.green()
                        
                    } else {
                        cell.actionLabel.text = "Pending"
                        cell.actionButton.backgroundColor = UIColor.darkGray
                    }
                    
                } else {
                    cell.actionLabel.text = "Add"
                    cell.actionButton.backgroundColor = color.blue()
                }
                
                cell.actionButton.addTarget(self, action: #selector(didPressAddButton(_:)), for: .touchUpInside)
            }
            
        } else if indexPath.section == 1 {
            if self.filteredContactsOnPhone.count == 0 {
                cell = self.tableView.dequeueReusableCell(withIdentifier: noResultsReuse) as? MainTableViewCell
                var bodyText = "You denied access to your contacts. Head to the settings app on your phone and grant MeArchive access. This will help MeArchive find friends who are already on MeArchive."
                if hasGivenAccessToContacts {
                    bodyText = "Nothing here yet."
                }
                cell.bodyLabel.text = bodyText
                
            } else {
                contact = self.filteredContactsOnPhone[indexPath.row]
                cell.actionLabel.text = "Invite"
                cell.actionButton.addTarget(self, action: #selector(didPressInviteButton(_:)), for: .touchUpInside)
                cell.actionButton.backgroundColor = UIColor.darkGray
            }
        }
        
        cell.actionButton.tag = indexPath.row
        
        if indexPath.section == 0 && self.filteredContactsOnMeArchive.count != 0 || indexPath.section == 1 && self.filteredContactsOnPhone.count != 0 {
            if let contact = contact {
                let personImage = UIElement().currentTheme().personImage
                if let userImageURL = contact.userImageURL {
                    cell.userImage.kf.setImage(with: URL(string: userImageURL ), placeholder: personImage, options: nil, progressBlock: nil, completionHandler: nil)
                    
                } else {
                    cell.userImage.image = personImage
                }
                
                if let username = contact.username {
                    cell.username.text = "@\(username)"
                    
                } else {
                    cell.username.text = contact.phoneNumber
                }
                
                if let name = contact.displayName {
                    cell.name.text = name
                    
                } else {
                    cell.name.text = ""
                }
            }
        }
        
        cell.selectionStyle = .none
        cell.backgroundColor = UIElement().currentTheme().backgroundColor
        
        //different color shows behind cell .. getting superview color to match cell
        cell.superview?.backgroundColor = UIElement().currentTheme().backgroundColor
        cell.hashtag.textColor = UIElement().currentTheme().hashTagColor
        
        return cell
    }
    
    func loadContactsFromPhone() {
        let toFetch = [CNContactGivenNameKey, CNContactFamilyNameKey, CNContactPhoneNumbersKey]
        let request = CNContactFetchRequest(keysToFetch: toFetch as [CNKeyDescriptor])
        let store = CNContactStore()
        
        var unSortedNames = [String]()
        var unSortedNumbers = [String]()
        
        do {
            try store.enumerateContacts(with: request) {
                contact, stop in
                
                if contact.phoneNumbers.count != 0 {
                    unSortedNames.append(contact.givenName + " " + contact.familyName)
                    let phoneString = (contact.phoneNumbers.first?.value)?.value(forKey: "digits") as! String
                    
                    let countryJaunt = (contact.phoneNumbers.first?.value)?.value(forKey: "countryCode") as! String
                    let charset = CharacterSet(charactersIn: "+")
                    
                    //determine if "phoneNumber" has country code prefix
                    if phoneString.rangeOfCharacter(from: charset) != nil{
                        //number already has country code, append to array
                        unSortedNumbers.append(phoneString)
                        
                    } else {
                        //loop through country codes to find one that matches returned country code from current user's contact book
                        for (country, countryCode) in self.countryCodes{
                            //create new contact with country prefix
                            if countryJaunt == country{
                                unSortedNumbers.append("+\(countryCode)\(phoneString)")
                            }
                        }
                    }
                }
            }
            
            //combine the two for sorting the names alphebetically
            let sortInfo = zip(unSortedNames, unSortedNumbers).sorted(by: {$0.0 < $1.0})
            
            //seperate the sorted data into their respective arrays
            let name = sortInfo.map{$0.0}
            let phoneNumber = sortInfo.map{$0.1}
            for i in 0..<name.count {
                let meaContacts = MEAContacts(userId: nil, displayName: name[i] , username: nil, userImageURL: nil, phoneNumber: phoneNumber[i], isFriendshipConfirmed: nil, postObjectId: nil)
                self.contactsOnPhone.append(meaContacts)
            }
            
            self.loadFriends()
            
        } catch let err{
            print(err)
        }
    }
    
    func checkContactsAuthorization() {
        switch CNContactStore.authorizationStatus(for: .contacts) {
        case .authorized:            
            self.hasGivenAccessToContacts = true
            self.loadContactsFromPhone()
            
            self.addDoneButton()
            
        case .denied:
            self.uiElement.permissionDenied("Oops", message: "You denied permission to your contacts", target: self)
            
            self.addDoneButton()
            break
            
        case .restricted, .notDetermined:
            self.setUpRequestAccessToContactsView()
        }
    }
    
    func messageComposeViewController(_ controller: MFMessageComposeViewController, didFinishWith result: MessageComposeResult) {
        controller.dismiss(animated: true, completion: nil)
    }
    
    lazy var accessToContactsLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont(name: "\(uiElement.mainFont)-Bold", size: 30)
        label.text = "Find Friends on MeArchive"
        label.textAlignment = .center
        label.numberOfLines = 0
        label.textColor = uiElement.currentTheme().textColor
        return label
    }()
    
    lazy var accessToContactsDescriptionLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont(name: uiElement.mainFont, size: 17)
        label.text = "With friends, you can help each other remember anything. Just @mention them, and your post will show up on their archive. They can set reminders on your post and mark it as complete when it's done."
        label.textAlignment = .left
        label.numberOfLines = 0
        label.textColor = uiElement.currentTheme().textColor
        return label
    }()
    
    lazy var requestAccessButton: UIButton = {
        let button = UIButton()
        button.titleLabel?.font = UIFont(name: uiElement.mainFont, size: 20)
        button.titleLabel?.textAlignment = .right
        button.setTitle("Find Friends", for: .normal)
        button.backgroundColor = Color().meArchiveYellow()
        button.setTitleColor(Color().black(), for: .normal)
        button.layer.cornerRadius = 3
        button.clipsToBounds = true
        return button
    }()
    
    lazy var skipButton: UIButton = {
        let button = UIButton()
        button.titleLabel?.font = UIFont(name: uiElement.mainFont, size: 17)
        button.setTitle("Skip", for: .normal)
        button.setTitleColor(uiElement.currentTheme().textColor, for: .normal)
        return button
    }()
    
    func setUpRequestAccessToContactsView() {
        self.view.addSubview(accessToContactsLabel)
        self.view.addSubview(accessToContactsDescriptionLabel)
        
        skipButton.addTarget(self, action: #selector(didPressSkipButton(_:)), for: .touchUpInside)
        self.view.addSubview(skipButton)
        
        requestAccessButton.addTarget(self, action: #selector(didPressRequestAccessButton(_:)), for: .touchUpInside)
        self.view.addSubview(requestAccessButton)
        
        accessToContactsLabel.snp.makeConstraints { (make) -> Void in
            make.top.equalTo(self.view).offset(UIElement().uiViewTopOffset(self))
            make.left.equalTo(self.view).offset(uiElement.leftOffset)
            make.right.equalTo(self.view).offset(uiElement.rightOffset)
        }
        
        accessToContactsDescriptionLabel.snp.makeConstraints { (make) -> Void in
            make.top.equalTo(self.accessToContactsLabel.snp.bottom).offset(uiElement.topOffset)
            make.left.equalTo(self.view).offset(uiElement.leftOffset)
            make.right.equalTo(self.view).offset(uiElement.rightOffset)
        }
        
        requestAccessButton.snp.makeConstraints { (make) -> Void in
            make.height.equalTo(uiElement.buttonHeight)
            make.left.equalTo(self.view).offset(uiElement.leftOffset)
            make.right.equalTo(self.view).offset(uiElement.rightOffset)
            make.bottom.equalTo(self.skipButton.snp.top).offset(uiElement.bottomOffset)
        }
        
        skipButton.snp.makeConstraints { (make) -> Void in
            make.height.equalTo(uiElement.buttonHeight)
            make.left.equalTo(self.view).offset(uiElement.leftOffset)
            make.right.equalTo(self.view).offset(uiElement.rightOffset)
            make.bottom.equalTo(self.view).offset(uiElement.bottomOffset)
        }
        
        self.tableView.isHidden = true
    }
    
    func removeRequestAccessViews() {
        self.skipButton.removeFromSuperview()
        self.requestAccessButton.removeFromSuperview()
        self.accessToContactsLabel.removeFromSuperview()
        self.accessToContactsLabel.removeFromSuperview()
        self.accessToContactsDescriptionLabel.removeFromSuperview()
    }
    
    //mark: button actions
    @objc func didPressSkipButton(_ sender: UIButton) {
        goToNext()
    }
    
    @objc func didPressRequestAccessButton(_ sender: UIButton) {
        CNContactStore().requestAccess(for: .contacts) { granted, error in
            if granted {
                DispatchQueue.main.async {
                    self.removeRequestAccessViews()
                    self.hasGivenAccessToContacts = true
                    self.loadContactsFromPhone()
                    self.tableView.isHidden = false
                    
                    self.addDoneButton()
                }
                
            } else {
                self.goToNext()
            }
        }
    }
    
    @objc func didPressDoneButton(_ sender: UIBarButtonItem) {
        goToNext()
    }
    
    @objc func didPressAddButton(_ sender: UIButton) {
        if let isConfirmed = self.filteredContactsOnMeArchive[sender.tag].isFriendshipConfirmed {
            if !isConfirmed {
                self.filteredContactsOnMeArchive[sender.tag].isFriendshipConfirmed = nil
                self.tableView.reloadData()
                
                let query = PFQuery(className: "Friends")
                query.whereKey("fromUserId", equalTo:  PFUser.current()!.objectId!)
                query.whereKey("toUserId", equalTo: self.filteredContactsOnMeArchive[sender.tag].userId! )
                query.whereKey("isRemoved", equalTo: false)
                query.getFirstObjectInBackground {
                    (object: PFObject?, error: Error?) -> Void in
                    if error != nil || object == nil {
                        print("The getFirstObject request failed.")
                        
                    } else {
                        object!["isRemoved"] = true
                        object?.saveEventually()
                    }
                }
            }
            
        } else {
            sender.backgroundColor = .darkGray
            self.filteredContactsOnMeArchive[sender.tag].isFriendshipConfirmed = false
            self.tableView.reloadData()
            let newFriend = PFObject(className: "Friends")
            newFriend["fromUserId"] = PFUser.current()!.objectId!
            newFriend["toUserId"] = self.filteredContactsOnMeArchive[sender.tag].userId!
            newFriend["isConfirmed"] = false
            newFriend["isRemoved"] = false
            newFriend.saveEventually {
                (success: Bool, error: Error?) -> Void in
                if (success) {
                    if let name = self.name {
                        UIElement().sendAlert("\(name) sent you a friend request.", toUserId: self.filteredContactsOnMeArchive[sender.tag].userId!)
                    }
                }
            }
        }
    }
    
    @objc func didPressInviteButton(_ sender: UIButton) {
        let contact = self.filteredContactsOnPhone[sender.tag]
        if MFMessageComposeViewController.canSendText() {
            let controller = MFMessageComposeViewController()
            controller.body = "Hey \(contact.displayName!), check this app MeArchive out. MeArchive helps you get stuff done with your friends. Download MeArchive, so we can get stuff done together! https://www.mearchive.app/ios"
            if let phoneNumber = contact.phoneNumber {
                controller.recipients = [phoneNumber]
            }
            
            controller.messageComposeDelegate = self
            self.present(controller, animated: true, completion: nil)
        }
    }
    
    //mark: data
    func loadFriends() {
        var userIds = [String]()
        
        let fromCurrentUserQuery = PFQuery(className: "Friends")
        fromCurrentUserQuery.whereKey("fromUserId", equalTo:  PFUser.current()!.objectId!)
        
        let toCurrentUserQuery = PFQuery(className: "Friends")
        toCurrentUserQuery.whereKey("toUserId", equalTo:  PFUser.current()!.objectId!)
        
        let query = PFQuery.orQuery(withSubqueries: [fromCurrentUserQuery, toCurrentUserQuery])
        query.whereKey("isRemoved", equalTo: false)
        query.findObjectsInBackground {
            (objects: [PFObject]?, error: Error?) -> Void in
            if error == nil {
                if let objects = objects {
                    for object in objects {
                        let fromUserId = object["fromUserId"] as! String
                        let toUserId = object["toUserId"] as! String
                        
                        if fromUserId == PFUser.current()!.objectId! {
                            userIds.append(toUserId)
                            
                        } else {
                            userIds.append(fromUserId)
                        }
                    }
                }
                
                self.loadContactsFromCloud(userIdsToNotInclude: userIds)
            } else {
                print("Error: \(error!)")
            }
        }
    }
    
    func loadContactsFromCloud(userIdsToNotInclude: Array<String>) {
        let phoneNumbers = self.contactsOnPhone.map{$0.phoneNumber}
        let query = PFQuery(className: "_User")
        query.whereKey("phoneNumber", containedIn: phoneNumbers as [Any] )
        query.whereKey("objectId", notContainedIn: userIdsToNotInclude)
        query.findObjectsInBackground {
            (objects: [PFObject]?, error: Error?) -> Void in
            if error == nil {
                if let objects = objects {
                    for object in objects {
                        for i in 0..<self.contactsOnPhone.count {
                            //username is person's phone number
                            if self.contactsOnPhone[i].phoneNumber == object["phoneNumber"] as? String {
                                self.contactsOnPhone[i].userId = object.objectId
                                
                                if let username = object["username"] as? String {
                                    self.contactsOnPhone[i].username = "\(username)"
                                }
                                
                                if let userImageFile = object["userImage"] as? PFFile {
                                    self.contactsOnPhone[i].userImageURL = userImageFile.url
                                }
                                
                                self.contactsOnMeArchive.append(self.contactsOnPhone[i])
                                self.contactsOnPhone.remove(at: i)
                                break
                            }
                        }
                    }
                }
                
                self.filteredContactsOnMeArchive = self.contactsOnMeArchive
                self.filteredContactsOnPhone = self.contactsOnPhone
                self.tableView.reloadData()
                
            } else {
                print("Error: \(error!)")
            }
        }
    }
    
    func loadCurrentUserInfo() {
        let query = PFQuery(className:"_User")
        query.getObjectInBackground(withId: PFUser.current()!.objectId!) {
            (user: PFObject?, error: Error?) -> Void in
            if let error = error {
                print(error)
                
            } else if let user = user {
                var name = ""
                if let displayName = user["name"] as? String {
                    name = displayName
                    
                } else if let username = user["username"] as? String {
                    name = username
                }
                
                self.name = name
                self.uiElement.setDefaultUserDisplayName(name)
            }
        }
    }
    
    func segueToView(_ storyboard: String, identifier: String) {
        let storyboard = UIStoryboard(name: storyboard, bundle: nil)
        let controller = storyboard.instantiateViewController(withIdentifier: identifier)
        self.present(controller, animated: true, completion: nil)
    }
    
    func goToNext() {
        self.segueToView("Main", identifier: "main")
    }
    
    func addDoneButton() {
        let doneButton = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(self.didPressDoneButton(_:)))
        searchBar.delegate = self
        let searchBarNavItem = UIBarButtonItem(customView: searchBar)
        self.navigationItem.rightBarButtonItems = [doneButton, searchBarNavItem]
    }
}
