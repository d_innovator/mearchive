//
//  CompletedViewController.swift
//  MeAtlas
//
//  Created by Dominic  Smith on 8/6/18.
//  Copyright © 2018 Dominic Smith. All rights reserved.
//

import UIKit
import Parse
import Kingfisher

class CompletedViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    var people = [MEAContacts]()
    var noteObjectId: String? 
    var tableView = UITableView()
    var isShowingCompleted = false
    
    var noResultsReuse = "noResultsReuse"
    var contactsReuse = "contactsReuse"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        getPeopleWhoCompleted()
        setUpViews()
        setUpTabBar()
    }
    
    func setUpViews() {
        tableView.delegate = self
        tableView.dataSource = self
        tableView.register(MainTableViewCell.self, forCellReuseIdentifier: contactsReuse)
        tableView.register(MainTableViewCell.self, forCellReuseIdentifier: noResultsReuse)
        tableView.backgroundColor = UIElement().currentTheme().backgroundColor
        tableView.separatorColor = .darkGray
        tableView.keyboardDismissMode = .onDrag
        tableView.separatorStyle = .none
        tableView.frame = view.bounds
        view.addSubview(tableView)
    }
    
    func setUpTabBar() {
        self.view.backgroundColor = UIElement().currentTheme().backgroundColor
        navigationController?.navigationBar.barTintColor = UIElement().currentTheme().backgroundColor
        navigationController?.navigationBar.tintColor = UIElement().currentTheme().textColor
        navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIElement().currentTheme().textColor]
    }
    
    //mark: tableview
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        if isShowingCompleted {
            return "People who Completed"
        }
        
        return "People mentioned"
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if self.people.count != 0 {
            return people.count
        }
        
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var cell: MainTableViewCell
        
        if self.people.count != 0 {
            cell = self.tableView.dequeueReusableCell(withIdentifier: contactsReuse) as! MainTableViewCell
            
            cell.actionButton.isHidden = true
            
            let people = self.people[indexPath.row]
            if let isConfirmed = people.isFriendshipConfirmed {
                if isConfirmed {
                    cell.actionButton.layer.borderWidth = 1
                    cell.actionButton.layer.borderColor = UIElement().currentTheme().textColor.cgColor
                    cell.actionButton.backgroundColor = .clear
                    cell.actionLabel.text = "Friends"
                    
                } else {
                    cell.actionButton.layer.borderWidth = 0
                    cell.actionButton.backgroundColor = .darkGray
                    cell.actionLabel.text = "Pending"
                }
                
            } else {
                cell.actionButton.layer.borderWidth = 0
                cell.actionButton.backgroundColor = Color().blue()
                cell.actionLabel.text = "Add"
            }
            
            let personImage = UIElement().currentTheme().personImage
            if let userImageURL = people.userImageURL {
                cell.userImage.kf.setImage(with: URL(string: userImageURL ), placeholder: personImage, options: nil, progressBlock: nil, completionHandler: nil)
                
            } else {
                cell.userImage.image = personImage
            }
            
            if let username = people.username {
                cell.username.text = "@\(username)"
                
            } else {
                cell.username.text = people.phoneNumber
            }
            
            if let name = people.displayName {
                cell.name.text = name
                
            } else {
                cell.name.text = ""
            }

        } else {
            cell = self.tableView.dequeueReusableCell(withIdentifier: noResultsReuse) as! MainTableViewCell
            if isShowingCompleted {
                cell.bodyLabel.text = "No completes yet. People who marked this post as complete will show up here."
                
            } else {
                cell.bodyLabel.text = "No one @mentioned in this post."
            }
        }
        
        cell.selectionStyle = .none
        cell.backgroundColor = UIElement().currentTheme().backgroundColor
        
        //different color shows behind cell .. getting superview color to match cell
        cell.superview?.backgroundColor = UIElement().currentTheme().backgroundColor
        cell.hashtag.textColor = UIElement().currentTheme().hashTagColor
        
        return cell
    }
    
    func getPeopleWhoCompleted() {
        if let objectId = noteObjectId {
            let query = PFQuery(className:"Post")
            query.getObjectInBackground(withId: objectId) {
                (object: PFObject?, error: Error?) -> Void in
                if error != nil {
                    //print(error)
                    
                } else if let object = object {
                    var objectToGet = "mentions"
                    
                    if self.isShowingCompleted {
                        objectToGet = "completed"
                    }
                    
                    if let userIds = object[objectToGet] as? Array<String> {
                        self.loadPeopleInfo(userIds)
                    }
                }
            }
        }
    }
    
    
    func loadPeopleInfo(_ peopleUserIds: Array<String>) {
        let query = PFQuery(className: "_User")
        query.whereKey("objectId", containedIn: peopleUserIds)
        query.findObjectsInBackground {
            (objects: [PFObject]?, error: Error?) -> Void in
            if error == nil {
                if let objects = objects {
                    for object in objects {
                        let userObject = MEAContacts(userId: object.objectId, displayName: nil, username: nil, userImageURL: nil, phoneNumber: nil, isFriendshipConfirmed: true, postObjectId: nil)
                        
                        if let name = object["name"] as? String {
                            userObject.displayName = name
                        }
                        
                        if let username = object["username"] as? String {
                            userObject.username = username
                        }
                        
                        if let userImageFile = object["userImage"] as? PFFile {
                            userObject.userImageURL = userImageFile.url!
                        }
                        
                        self.people.append(userObject)
                    }
                }
                self.tableView.reloadData()
                
            } else {
                print("Error: \(error!)")
            }
        }
    }
}
