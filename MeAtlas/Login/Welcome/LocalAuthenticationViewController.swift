//
//  LocalAuthenticationViewController.swift
//  MeAtlas
//
//  Created by Dominic  Smith on 5/28/18.
//  Copyright © 2018 Dominic Smith. All rights reserved.
//

import UIKit
import LocalAuthentication
import Parse

class LocalAuthenticationViewController: UIViewController {
    
    lazy var loadingSpinner: UIActivityIndicatorView = {
        let spinner  = UIActivityIndicatorView()
        spinner.color = UIElement().currentTheme().textColor
        return spinner
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = UIElement().currentTheme().backgroundColor
        
        self.view.addSubview(loadingSpinner)
        loadingSpinner.snp.makeConstraints { (make) -> Void in
            make.height.width.equalTo(100)
            make.top.equalTo(self.view).offset(self.view.frame.height / 2 - 100)
            make.left.equalTo(self.view).offset(self.view.frame.width / 2 - 35)
        }
        loadingSpinner.startAnimating()
        
        let myContext = LAContext()
        let myLocalizedReasonString = "Help Keep your Archive safe"
        
        var authError: NSError?
        if myContext.canEvaluatePolicy(.deviceOwnerAuthenticationWithBiometrics, error: &authError) {
            myContext.evaluatePolicy(.deviceOwnerAuthenticationWithBiometrics, localizedReason: myLocalizedReasonString) { success, evaluateError in
                if success {
                    DispatchQueue.main.async {
                        let storyboard = UIStoryboard(name: "Main", bundle: nil)
                        let controller = storyboard.instantiateViewController(withIdentifier: "main")
                        self.present(controller, animated: true, completion: nil)
                    }

                } else {
                    DispatchQueue.main.async {
                        if PFUser.current() != nil {
                            PFUser.logOut()
                        }
                        let storyboard = UIStoryboard(name: "Login", bundle: nil)
                        let controller = storyboard.instantiateViewController(withIdentifier: "welcome")
                        let appDelegate = UIApplication.shared.delegate as! AppDelegate
                        //show window
                        appDelegate.window?.rootViewController = controller
                    }
                }
            }
            
        } else {
            if PFUser.current() != nil {
                PFUser.logOut()
            }
            let storyboard = UIStoryboard(name: "Login", bundle: nil)
            let controller = storyboard.instantiateViewController(withIdentifier: "welcome")
            let appDelegate = UIApplication.shared.delegate as! AppDelegate
            //show window
            appDelegate.window?.rootViewController = controller
        }
    }
}
