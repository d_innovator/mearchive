//
//  RealmUser.swift
//  MeAtlas
//
//  Created by Dominic  Smith on 7/11/18.
//  Copyright © 2018 Dominic Smith. All rights reserved.
//

import RealmSwift
import UserNotifications

class RealmUser: Object {
    @objc dynamic var userId: String! = ""
    @objc dynamic var userImageURL: String? = nil
    @objc dynamic var username: String? = nil
    @objc dynamic var name: String? = nil
}
